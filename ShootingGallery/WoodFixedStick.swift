//
//  WoodFixedStick.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/13/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit
class WoodFixedStick : TargetBase {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "stick_woodFixed_outline");
    }
    
    override func configProperties(){
        self.name = "WoodFixedStick";
        self.zPosition = LayerDepth.ROW_ONE_TARGET_BASE.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0); // Bottom-middle
        
        self.hitBoxCenter = CGPoint(x:0, y:0.6);  // In the middle
        self.hitBoxWdith = 0.6;
    }
    
    override func getAfterHitTexture() ->  SKTexture {
        return SKTexture(imageNamed: "stick_wood_outline_broken");
    }
    
    override func getHitScoreValue() -> Int {
        return 500;
    }
}