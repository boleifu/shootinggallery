//
//  PlusTimeTextNode.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/17/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class PlusTimeTextNode : EnvironmentObject {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "plus_5s");
    }
    
    override func configProperties() {
        self.name = "PlusTimeText";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: -0.5, y: 0); // Left-bottom
    }
    
    override func getAction() -> SKAction {
        return GameConstants.FADE_AWAY;
    }
}