//
//  AddScoreTextNode.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/14/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class AddScoreTextNode : GameEntity {
    
    let score:Int;
    
    // Constructor
    init(position:CGPoint, face:CGFloat, targetWidth:CGFloat?, targetHeight:CGFloat?, scoreNum:Int) {
        self.score = scoreNum;
        super.init(position:position, face:face, targetWidth:targetWidth, targetHeight:targetHeight);
        
        // Run the pre-set action
        self.runAction(getAction());
        //print("Adding \(scoreNum), width = \(self.size.width)");
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func generateTexture() -> SKTexture? {
        return TextureFactory.getNumberTexture(score, true, SpecialChar.PLUS);
    }
    
    override func configProperties() {
        self.name = "PlusScoreText";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: -0.5, y: 0); // Left-bottom
    }
    
    func getAction() -> SKAction {
        return GameConstants.FADE_AWAY;
    }
}