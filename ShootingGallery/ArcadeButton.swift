//
//  ArcadeButton.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/5/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

//
// The controller button
// Can be pressed
//
class ArcadeButton:SKSpriteNode {
    
    private var buttonKey:String = "";
    
    private var isPressed:Bool = false;
    private var commandId:Int = 0;   // A unique id, changed for each press
    
    // Constructor
    init(position:CGPoint, width:CGFloat, buttonKey:String) {
        super.init(texture: nil, color: SKColor.whiteColor(), size: CGSize.zero);
        
        // Defaultly in idle state
        releaseButton();
        self.buttonKey = buttonKey;
        self.anchorPoint = CGPointMake(0.5, 0.5);
        self.size = CGSizeMake(width, width);
        self.alpha = 0.6;
        self.position = position;
        self.zPosition = LayerDepth.CONTROL_BUTTON.getDepth();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented");
    }
    
    func pressButton() {
        self.texture = SKTexture(imageNamed: GameConstants.CONTROLLER_BUTTON_PRESSED);
        isPressed = true;
        commandId += 1;
    }
    
    func releaseButton() {
        self.texture = SKTexture(imageNamed: GameConstants.CONTROLLER_BUTTON_IDLE);
        isPressed = false;
    }
    
    // Check whether the given screen position is in this button area
    func isInButton(pos:CGPoint) -> Bool {
        return abs(pos.x) <= self.size.width/2 && abs(pos.y) <= self.size.height/2;
    }
    
    // Output the current user input
    func isButtonPressed() -> Bool {
        return self.isPressed;
    }
    
    func getCommandId() -> Int {
        return self.commandId;
    }
}
