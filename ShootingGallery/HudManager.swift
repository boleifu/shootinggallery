//
//  HudManager.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/10/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

/**
 * This class manage the lifecycle of the game hud
 */
class HudManager {
    
    weak var hudLayer:SKNode?;
    
    // Hud Config
    let hudXMargin:CGFloat;
    let hudYMargin:CGFloat;
    
    let hudLeftX:CGFloat;
    let hudRightX:CGFloat;
    let hudTopY:CGFloat;
    let hudBottomY:CGFloat;
    
    let messageTextHeight:CGFloat;
    
    // Meters
    var timer:DigitNumberNode? = nil;
    var scoreMeter:SmallDigitNumberNode? = nil;
    var ammoMeter:AmmoMeter? = nil;
    var multiplierMeter:MultiplierMeter? = nil;
    
    // Message Board
    let messageBoard:SKNode = SKNode();
    
    // Runtime status
    let lastUpdatedScore:Int = 0;
    var hurryUpNotified:Bool = false;
    
    
    init(hudLayer:SKNode) {
        self.hudLayer = hudLayer;
        self.hudXMargin = GameContext.screenSize.width/40;
        self.hudYMargin = GameContext.screenSize.height/20;
        
        self.hudLeftX = self.hudXMargin;
        self.hudRightX = GameContext.screenSize.width - self.hudXMargin;
        self.hudBottomY = self.hudYMargin;
        self.hudTopY = GameContext.screenSize.height - self.hudYMargin;
        
        self.messageTextHeight = GameContext.screenSize.height/10;
    }
    
    deinit {
        print("HudManager released.")
    }
    
    func initHud() {
        // Add time count down
        timer = DigitNumberNode(position: CGPoint(x: GameContext.screenSize.width/2, y: hudTopY), face:0, targetWidth: nil, targetHeight:GameContext.screenSize.height/10);
        timer?.anchorPoint = CGPoint(x: 0.5, y: 1); // Top-middle
        hudLayer!.addChild(timer!);
        //print("Timer added.");
        
        // Add Score meter icon
        let scoreMeterIcon = SmallScoreIcon(position: CGPoint(x: hudLeftX, y: hudTopY), face:0, targetWidth: nil, targetHeight:GameContext.screenSize.height/15);
        hudLayer!.addChild(scoreMeterIcon);
        
        // Add Score meter
        let scoreMeterX = scoreMeterIcon.position.x + scoreMeterIcon.size.width + hudXMargin;
        scoreMeter = SmallDigitNumberNode(position: CGPoint(x: scoreMeterX, y: hudTopY), face:0, targetWidth: nil, targetHeight:GameContext.screenSize.height/15);
        scoreMeter?.anchorPoint = CGPoint(x: 0, y: 1); // Top-left
        hudLayer!.addChild(scoreMeter!);
        
        // Add MessageBoard
        messageBoard.position = CGPoint(x: GameContext.screenSize.width/2, y: GameContext.screenSize.height/2);
        hudLayer!.addChild(messageBoard);
        
        // Add AmmoMeter
        ammoMeter = AmmoMeter(position: CGPoint(x: hudRightX, y: hudTopY), height: GameContext.screenSize.height/10);
        hudLayer!.addChild(ammoMeter!);
        
        // Add score multiplier meter
        multiplierMeter = MultiplierMeter(position: CGPoint(x: hudLeftX, y: hudTopY - scoreMeterIcon.size.height), height: GameContext.screenSize.height/10);
        hudLayer!.addChild(multiplierMeter!);
        
        // Reset hud
        //resetHud();
        hudLayer?.hidden = true;
    }
    
    func resetHud() {
        timer?.refreshTexture(GameRuntimeContext.gameDuration);
        scoreMeter?.refreshTexture(0);
        messageBoard.removeAllChildren();
        ammoMeter?.resetAmmoMeter(GameRuntimeContext.maxAmmo);
        multiplierMeter?.resetMultiplierMeter();
        
        // Start count down and then start the game
        let countDownNode = TimeCountDownNode(position: CGPoint.zero, face:0, targetWidth: nil, targetHeight:messageTextHeight);
        messageBoard.addChild(countDownNode);
        
        hudLayer?.hidden = false;
    }
    
    func showTimeupText() {
        // Show timeup text
        let timeupText = TimeupTextNode(position: CGPoint.zero, face:0, targetWidth: nil, targetHeight:messageTextHeight);
        messageBoard.addChild(timeupText);
        AudioUtils.playTimeOverSound();
    }
    
    func refresh(currentTime: CFTimeInterval, crossHairPosOnHud:CGPoint) {
        if (!GameRuntimeContext.gameStarted) {
            return;
        }
        
        // Update timer
        let secRemains = Int(GameRuntimeContext.gameEndTime - currentTime);
        if (secRemains != timer?.numberValue) {
            if (secRemains > timer?.numberValue) {
                // Time added
                let timeAddedNum:Int = secRemains - timer!.numberValue;
                dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                    let timeAddedText = AddScoreTextNode(position: CGPoint.zero, face:0, targetWidth: nil, targetHeight:GameContext.screenSize.height/12, scoreNum: timeAddedNum);
                    timeAddedText.anchorPoint = CGPoint(x:0, y:0.5);
                    // Always add map in main thread to avoid racing condition
                    dispatch_async(dispatch_get_main_queue()) {
                        let timeAddingTextPos = CGPoint(x: self.timer!.position.x + self.timer!.size.width/2, y: self.timer!.position.y - self.timer!.size.height/2);
                        timeAddedText.position = timeAddingTextPos;
                        self.hudLayer?.addChild(timeAddedText);
                    }
                }
            }
            
            // Play hurry up voice
            if (secRemains > 10 && hurryUpNotified) {
                self.hurryUpNotified = false;
            } else if (secRemains <= 10 && !hurryUpNotified) {
                // Only play hurry up sound for non-speed mode
                if (GameRuntimeContext.gameMode != GameMode.SPEED_MODE) {
                    AudioUtils.playHurryUpSound();
                }
                self.hurryUpNotified = true;
            }
            
            timer?.refreshTexture(secRemains);
        }
        
        // Update score meter
        let currentScore = GameRuntimeContext.currentScore;
        if (scoreMeter?.numberValue != currentScore) {
            // Show score adding text
            let scoreAdded = currentScore - scoreMeter!.numberValue;
            //print("Adding \(scoreAdded) points...");
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                let scoreAddingText = AddScoreTextNode(position: crossHairPosOnHud, face:0, targetWidth: nil, targetHeight:GameContext.screenSize.height/15, scoreNum: scoreAdded);
                // Always add map in main thread to avoid racing condition
                dispatch_async(dispatch_get_main_queue()) {
                    self.hudLayer?.addChild(scoreAddingText);
                }
            }

            // Update score mether
            scoreMeter?.refreshTexture(currentScore);
        }
        
        // Update multiplier meter
        multiplierMeter!.refreshMultiplierMeter();
    }
}
