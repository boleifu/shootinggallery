//
//  PlayText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/18/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class ArcadeModeText : SelectableNode {
    
    override func getText() -> String? {
        return NSLocalizedString("ARCADE_MODE", comment: "ArcadeModeText");
        //return GameTextConstants.ARCADE_MODE;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Arcade Mode Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Center;
    }
    
    override func select() {
        // Move to the arcade mode, and disable the menu
        GameRuntimeContext.menuStatus = MenuLifyCycle.ENTERING_GAME;
        GameRuntimeContext.targetGameMode = GameMode.ARCADE_MODE;
        unHilight();
    }
}