//
//  SpeedModeDuckTargetSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/28/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SpeedModeDuckTargetSpawner:SingleObjectSpawner {
    
    private var objCapacity:Int = 0;
    var candidatePosX:[CGFloat] = [];
    
    override func getTarget() -> ShootingTarget {
        return EntityFactory.createDuckTarget();
    }
    
    override func getTargetAction(startPos:CGPoint) -> SKAction {
        return GameConstants.VERTICAL_POP_UP;
    }
    
    override func configSpawner() {
        objCapacity = GameContext.speedModeTargetSlotNum;
        
        // Setup candidate x pos
        let gapX:CGFloat = GameContext.gameContentSize.width / CGFloat(objCapacity + 1);
        for i in 1...objCapacity {
            candidatePosX.append(gapX * CGFloat(i));
        }
    }
    
    override func spawnObjectByTime(currentTime: CFTimeInterval) {
        // Do Nothing
    }
    
    override func fillTheScreen() {
        // Do Nothing
    }
    
    override func getTargetInitPos() -> CGPoint {
        return CGPoint(x: self.initPos.x - GameContext.duckWidth/2, y: self.initPos.y);
    }
    
    override func spawnObjectByNumber(targetNum: Int) -> Int {
        if (gameContentLayer == nil) {
            print("Error: gameContentLayer is nil.");
            return 0;
        }
        
        let selectedPosX = getRandXPostions(targetNum);
        for posX in selectedPosX {
            let target = getTarget();
            let targetPos = CGPoint(x:posX, y:self.initPos.y)
            target.position = targetPos;
            gameContentLayer!.addChild(target);
            target.runAction(getTargetAction(targetPos));
        }
        
        return selectedPosX.count;
    }
    
    // ==== Helper functions ====
    
    private func getRandXPostions(targetNum:Int) -> [CGFloat] {
        if (targetNum >= candidatePosX.count) {
            return candidatePosX;
        }
        
        let shuffledPosX:[CGFloat] = candidatePosX.shuffle();
        return Array(shuffledPosX[0..<targetNum]);
    }
}