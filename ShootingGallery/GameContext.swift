//
//  GameContext.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/6/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GameContext {
    
    static var screenSize:CGSize = CGSize.zero;
    static var gameContentSize:CGSize = CGSize.zero;
    
    static var instructionPointerHeight:CGFloat = 0;
    
    // Menu
    static var titleSize:CGFloat = 0;
    static var subTitleSize:CGFloat = 0;
    static var pageTitileSize:CGFloat = 0;
    static var toolTextSize:CGFloat = 0;
    static var menuOptionSize:CGFloat = 0;
    static var gameReportSize:CGFloat = 0;
    static var actionTextSize:CGFloat = 0;
    static var gameModeDescTextSize:CGFloat = 0;
    static var commingSoonTextSize:CGFloat = 0;
    static var licenseDetailTextSize:CGFloat = 0;
    
    // Welcome page
    static var titlePos:CGPoint = CGPoint.zero;
    static var subTitlePos:CGPoint = CGPoint.zero;
    static var startTextPos:CGPoint = CGPoint.zero;
    static var versionTextPos:CGPoint = CGPoint.zero;
    
    // Game prep page
    static var pageTitlePos:CGPoint = CGPoint.zero;
    static var gameModePosX:CGFloat = 0;
    static var settingTextPos:CGPoint = CGPoint.zero;
    static var panelSize:CGSize = CGSize.zero;
    static var panelPos:CGPoint = CGPoint.zero;
    static var duckTargetSignHeight:CGFloat = 0;
    static var duckTargetSignPos:CGPoint = CGPoint.zero;
    static var gameModeDescPosX:CGFloat = 0;
    static var gameModeDescStartY:CGFloat = 0;
    static var gameModeDetailStartY:CGFloat = 0;
    
    // Game report page
    static var gameReportCenterX:CGFloat = 0;
    
    // Game setting page
    static var gameSettingCenterX:CGFloat = 0;
    static var gameSettingMinValue:Int = 0;
    static var gameSettingMaxValue:Int = 5;
    static var gameSettingValueX:CGFloat = 0;
    
    // Game credits page
    static var developerInfoPosX:CGFloat = 0;
    static var licenseInfoPosX:CGFloat = 0;
    
    static var menuContentStartY:CGFloat = 0;
    static var menuContentEndY:CGFloat = 0;
    
    static var actionOneTextPos:CGPoint = CGPoint.zero;
    static var actionTwoTextPos:CGPoint = CGPoint.zero;
    
    // Game on screen controller
    static var crossHairWidth:CGFloat = 0;
    static var arcadeButtonWidth:CGFloat = 0;
    private static var arcadeButtonLeftX:CGFloat = 0;
    private static var arcadeButtonRightX:CGFloat = 0;
    static var arcadeButtonPos:CGPoint = CGPoint.zero;
    
    // Game config
    static var arcadeModeGameTime:Int = 60;
    static var arcadeModeAmmoNum:Int = 5;
    static var speedModeGameTime:Int = 10;
    static var speedModeAmmoNum:Int = 5;
    static var speedModeMaxLevel:Int = 6;
    static var speedModeTargetSlotNum:Int = 6;
    static var minSensitivity:CGFloat = 1;
    static var maxSensitivity:CGFloat = 4;
    static var crossHairSensitivity:CGFloat = 0;
    
    // Game content
    static var duckWidth:CGFloat = 0;
    static var aimingTargetWidth:CGFloat = 0;
    static var stickWidth:CGFloat = 0;
    static var bulletHoleWidth:CGFloat = 0;
    static var waterWidth:CGFloat = 0;
    static var waterHeight:CGFloat = 0;
    static var grassWidth:CGFloat = 0;
    static var grassHeigth:CGFloat = 0;
    static var cloudWidth:CGFloat = 0;
    static var treeWidth:CGFloat = 0;
    
    // Spawner location
    static var frontWaveSpawnerPos:CGPoint = CGPoint.zero;
    static var frontTargetSpawnerPos:CGPoint = CGPoint.zero;
    static var rowTwoWaveSpanerPos:CGPoint = CGPoint.zero;
    static var frontGrassSpawnerPos:CGPoint = CGPoint.zero;
    static var rowTwoTargetSpawnerPos:CGPoint = CGPoint.zero;
    static var rowTwoGrassSpawnerPos:CGPoint = CGPoint.zero;
    static var cloudSpawnerPos:CGPoint = CGPoint.zero;
    
    static func initContext(screenSize:CGSize, _ gameContentSize:CGSize) {
        GameContext.screenSize = screenSize;
        GameContext.gameContentSize = gameContentSize;
        GameContext.updateSensitivity();
        
        // Menu
        GameContext.titleSize = screenSize.height/3;
        GameContext.subTitleSize = screenSize.height/6;
        GameContext.pageTitileSize = screenSize.height/8;
        GameContext.toolTextSize = screenSize.height/25;
        GameContext.menuOptionSize = screenSize.height/10;
        GameContext.gameReportSize = screenSize.height/15;
        GameContext.actionTextSize = screenSize.height/10;
        GameContext.gameModeDescTextSize = screenSize.height/20;
        GameContext.commingSoonTextSize = screenSize.height/10;
        GameContext.licenseDetailTextSize = screenSize.height/25;
        GameContext.instructionPointerHeight = screenSize.height/5;
        
        GameContext.titlePos = CGPoint(x: screenSize.width/2, y: screenSize.height/3*2);
        GameContext.subTitlePos = CGPoint(x: screenSize.width/5*4, y: screenSize.height/2);
        GameContext.startTextPos = CGPoint(x: screenSize.width/2, y: screenSize.height/3);
        GameContext.versionTextPos = CGPoint(x: screenSize.width/20*19, y: screenSize.height/20);

        GameContext.pageTitlePos = CGPoint(x: screenSize.width/2, y: screenSize.height/6*5);
        GameContext.gameModePosX = screenSize.width/5*2;
        GameContext.settingTextPos = CGPoint(x: GameContext.screenSize.width/2, y: screenSize.height - gameContentSize.height);
        
        GameContext.gameReportCenterX = screenSize.width/3;
        
        GameContext.developerInfoPosX = screenSize.width/3;
        GameContext.licenseInfoPosX = screenSize.width/3*2;
        
        GameContext.gameSettingCenterX = screenSize.width/2;
        GameContext.gameSettingValueX = screenSize.width/2;
        
        GameContext.menuContentStartY = screenSize.height/6*4;
        GameContext.menuContentEndY = screenSize.height/4;
        
        GameContext.panelSize = CGSize(width: screenSize.width/2, height: GameContext.menuContentStartY - GameContext.menuContentEndY);
        GameContext.panelPos = CGPoint(x: GameContext.gameModePosX + screenSize.width/30, y: screenSize.height/36*19);
        GameContext.duckTargetSignHeight = GameContext.menuContentStartY - GameContext.menuContentEndY;
        GameContext.duckTargetSignPos = CGPoint(x: GameContext.panelSize.width, y: 0);
        GameContext.gameModeDescPosX = panelSize.width/30;
        GameContext.gameModeDescStartY = panelSize.height/20*7;
        GameContext.gameModeDetailStartY = 0;
        
        GameContext.actionOneTextPos = CGPoint(x: screenSize.width/20*19, y: screenSize.height - gameContentSize.height);
        GameContext.actionTwoTextPos = CGPoint(x: screenSize.width/20*19, y: screenSize.height - gameContentSize.height + menuOptionSize);
        
        // Game On-scree Controllers
        GameContext.crossHairWidth = gameContentSize.height/10;
        GameContext.arcadeButtonWidth = screenSize.height - gameContentSize.height;
        GameContext.arcadeButtonLeftX = screenSize.width/5;
        GameContext.arcadeButtonRightX = screenSize.width/5*4;
        GameContext.updateArcadeButtonPos();
        
        // Game Content
        GameContext.duckWidth = gameContentSize.width/10;
        GameContext.aimingTargetWidth = gameContentSize.width/10;
        GameContext.stickWidth = GameContext.duckWidth/114*34;
        GameContext.bulletHoleWidth = gameContentSize.width/40;
        GameContext.waterWidth = gameContentSize.width;
        GameContext.waterHeight = GameContext.waterWidth/1320*223;
        GameContext.grassWidth = GameContext.waterWidth;
        GameContext.grassHeigth = GameContext.grassWidth/1320*216;
        GameContext.cloudWidth = gameContentSize.width/6;
        GameContext.treeWidth = gameContentSize.width/10;
        
        GameContext.frontWaveSpawnerPos = CGPoint(x:0, y:-GameContext.waterHeight/2);
        GameContext.frontTargetSpawnerPos = CGPoint.zero;
        GameContext.rowTwoWaveSpanerPos = CGPoint.zero;
        
        GameContext.frontGrassSpawnerPos = CGPoint(x:0, y:GameContext.waterHeight/4);
        GameContext.rowTwoTargetSpawnerPos = CGPoint(x:0, y:frontGrassSpawnerPos.y + GameContext.grassHeigth/2)
        GameContext.rowTwoGrassSpawnerPos = CGPoint(x:0, y:frontGrassSpawnerPos.y + GameContext.grassHeigth/2);
        
        GameContext.cloudSpawnerPos = CGPoint(x:0, y:GameContext.gameContentSize.height/5*4);
    }
    
    static func updateSensitivity() {
        let updatedSensitivity = (GameContext.maxSensitivity - GameContext.minSensitivity) / CGFloat(GameContext.gameSettingMaxValue - GameContext.gameSettingMinValue) * CGFloat(GameConfigUtils.getCrosshairSensitivity()) + GameContext.minSensitivity;
        GameContext.crossHairSensitivity = updatedSensitivity;
        print("Sensitivity updated to \(updatedSensitivity)");
    }
    
    static func updateArcadeButtonPos() {
        if (GameConfigUtils.isArcadeButtonOnLeft()) {
            GameContext.arcadeButtonPos = CGPoint(x: GameContext.arcadeButtonLeftX, y:GameContext.arcadeButtonWidth/2);
        } else {
            GameContext.arcadeButtonPos = CGPoint(x: GameContext.arcadeButtonRightX, y:GameContext.arcadeButtonWidth/2);
        }
        print("Arcade Button Pos updated to \(GameContext.arcadeButtonPos)");
    }
    
    
}