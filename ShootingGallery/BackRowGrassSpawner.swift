//
//  BackRowGrassSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/13/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class BackRowGrassSpawner:FrontRowGrassSpawner {
    
    override func getTarget() -> SKSpriteNode {
        let grass = super.getTarget();
        grass.zPosition = LayerDepth.ROW_FOUR_BLOCKER.getDepth();
        return grass;
    }
    
    override func configSpawner() {
        self.objectLiftTime = self.objectLiftTime/4*3;
        super.configSpawner();
        //self.initPos.x -= self.objectWidth/2;
    }
}