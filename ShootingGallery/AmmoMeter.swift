//
//  AmmoMeter.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/12/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class AmmoMeter : SKNode {
    
    // Configuration
    let ammoIconHeight:CGFloat;
    
    // Runtime state
    var currentAmmoCount = 0;
    var maxAmmoCount = 0;
    var progressBarWidth:CGFloat = 0;
    var reloadDuration:CFTimeInterval = 0;
    
    var progressBar:ProgressBar? = nil;
    var ammoIcons:[AmmoIcon] = [];
    //var outline:SKShapeNode? = nil;
    
    init(position:CGPoint, height:CGFloat) {
        self.ammoIconHeight = height;
        super.init();
        self.position = position;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented");
    }
    
    deinit {
        print("Ammo Meter released");
    }
    
    func resetAmmoMeter(ammoNum:Int) {
        ammoIcons.removeAll();
        self.removeAllChildren();
        maxAmmoCount = ammoNum;
        currentAmmoCount = maxAmmoCount;
        progressBarWidth = 0;
        
        if (GameRuntimeContext.gameMode == GameMode.SPEED_MODE) {
            reloadDuration = GameRuntimeContext.speedModeReloadDuration;
        } else {
            reloadDuration = GameRuntimeContext.reloadDuration;
        }
        
        // Add ammo icon
        var currentX:CGFloat = 0;
        for _ in 1...ammoNum {
            let ammoIcon = GoldAmmoIcon(position: CGPoint(x: currentX, y: 0), face:0, targetWidth: nil, targetHeight:ammoIconHeight);
            currentX -= ammoIcon.size.width;
            self.addChild(ammoIcon);
            ammoIcons.append(ammoIcon);
            
            progressBarWidth += ammoIcon.size.width;
        }
        
        // Add reloading progress bar
        progressBar = ProgressBar(position: CGPoint(x: 0, y: -ammoIconHeight), face:0, targetWidth: progressBarWidth, targetHeight:nil);
        self.addChild(progressBar!);
        progressBar?.hidden = true;
        
        /*
        // Add outline square
        outline = SKShapeNode(rectOfSize: CGSize(width: currentX, height: ammoIconHeight));
        outline!.position = CGPoint(x: currentX/2, y: -ammoIconHeight/2);
        outline!.zPosition = LayerDepth.DEBUG_OBJECT.getDepth();
        self.addChild(outline!);
        */
    }
    
    func removeAmmo() -> Bool {
        if (currentAmmoCount > 0) {
            ammoIcons[currentAmmoCount-1].empty();
            currentAmmoCount -= 1;
            showLoadingProgress(GameRuntimeContext.fireCoolDownTime);
            return true;
        }
        return false;
    }
    
    func refillAmmo() {
        if (GameRuntimeContext.isReloading) {
            return;
        }
        
        GameRuntimeContext.isReloading = true;
        self.runAction(SKAction.waitForDuration(reloadDuration), completion: {
            for ammoIcon in self.ammoIcons {
                if (ammoIcon.isEmtpy) {
                    ammoIcon.refill();
                }
            }
            self.currentAmmoCount = self.maxAmmoCount;
            GameRuntimeContext.isReloading = false;
        });
        
        showLoadingProgress(reloadDuration);
    }
    
    func showLoadingProgress(duration:NSTimeInterval) {
        if (duration <= 0) {
            return;
        }
        progressBar!.removeAllActions();
        
        
        progressBar!.hidden = false;
        progressBar!.runAction(ActionFactory.getLoadingAction(duration), completion: {
            self.progressBar!.hidden = true;
        });
    }
    
    /*
    func isInMeter(hitPosOnMeter:CGPoint) -> Bool {
        if (outline == nil) {
            return false;
        }
        
        let hitPosOnOutline = self.convertPoint(hitPosOnMeter, toNode: outline!);
        if (CGPathContainsPoint(outline!.path, nil, hitPosOnOutline, false)) {
            return true;
        } else {
            return false;
        }
    }
    */
}
