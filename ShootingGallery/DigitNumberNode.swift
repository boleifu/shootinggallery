//
//  DigitNumber.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/10/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class DigitNumberNode : GameEntity {
    
    var numberValue:Int = 0;
    var digitCount:Int = 1;
    
    override func generateTexture() -> SKTexture? {
        return TextureFactory.getNumberTexture(0);
    }
    
    override func configProperties(){
        self.name = "DigitNumber";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5); // Middle
    }
    
    func refreshTexture(number:Int) {
        let newTexture = TextureFactory.getNumberTexture(number);
        let newDigitCount = Utils.getDigitCount(number);
        
        let currentHeight = self.size.height;
        
        numberValue = number;
        self.texture = newTexture;
        
        // Resize if digit number changed
        if (digitCount != newDigitCount) {
            digitCount = newDigitCount;
            self.resize(nil, currentHeight);
        }
    }
    
    
}