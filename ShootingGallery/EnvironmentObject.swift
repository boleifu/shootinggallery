//
//  EnvironmentObject.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/5/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

//
// This class is the base class of all environment object in the game.
// EnvironmentObject is the objects that does not affect the game function directly, and only have some visual impacts.
// These objects has an embadded action, and will run once it is constructed.
// Note: EnvironmentObject defaultly inherit the pre-set behaviors of GameEntity.
//
class EnvironmentObject : GameEntity {
    
    // Constructor
    override init(position:CGPoint, face:CGFloat, targetWidth:CGFloat?, targetHeight:CGFloat?) {
        super.init(position:position, face:face, targetWidth:targetWidth, targetHeight:targetHeight);
        
        // Run the pre-set action
        self.runAction(getAction(), completion: {
            self.afterAction();
        });
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     * Get the embadded action of this object.
     * Must be override
     */
    func getAction() -> SKAction {
        // Not implemented
        fatalError("getAction() has not been implemented");
    }
    
    func afterAction() {
        // Default do nothing
    }
}

