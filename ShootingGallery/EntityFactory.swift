//
//  EntityFactory.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/6/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

/**
 * This is the factory class for generating some common game entities.
 */
class EntityFactory {
     
    // Game Contents
    
    static func createInstaructionPointer() -> InstructionPointer {
        return InstructionPointer(position: CGPoint.zero, face:0, targetWidth: nil, targetHeight:GameContext.instructionPointerHeight);
    }
    
    static func createInstructionText() -> InstructionText {
        return InstructionText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.GOLD_COLOR);
    }
    
    static func createDuckTarget() -> ShootingTarget {
        let stick = WoodFixedStick(position: CGPoint.zero, face:0, targetWidth: GameContext.stickWidth, targetHeight:nil);
        let duck = Duck(position: CGPoint.zero, face:0, targetWidth: GameContext.duckWidth, targetHeight:nil);
        return ShootingTarget(base: stick, addOn: duck);
    }
    
    static func createAimingTarget() -> ShootingTarget {
        let stick = MetalStick(position: CGPoint.zero, face:0, targetWidth: GameContext.stickWidth, targetHeight:nil);
        let aimingTarget = AimingTarget(position: CGPoint.zero, face:0, targetWidth: GameContext.aimingTargetWidth, targetHeight:nil);
        return ShootingTarget(base: stick, addOn: aimingTarget);
    }
    
    static func createBulletHole() -> BulletHole {
        return BulletHole(position: CGPoint.zero, face:0, targetWidth: GameContext.bulletHoleWidth, targetHeight:nil);
    }
    
    static func createWater() -> Water {
        return Water(position: CGPoint.zero, face:0, targetWidth: GameContext.waterWidth, targetHeight:nil);
    }
    
    static func createGrass() -> Grass {
        return Grass(position: CGPoint.zero, face:0, targetWidth: GameContext.grassWidth, targetHeight:nil);
    }
    
    static func createCloud() -> Cloud {
        return Cloud(position: CGPoint.zero, face:0, targetWidth: GameContext.cloudWidth, targetHeight:nil);
    }
    
    static func createTree() -> Tree {
        return Tree(position: CGPoint.zero, face:0, targetWidth: GameContext.treeWidth, targetHeight:nil);
    }
}
