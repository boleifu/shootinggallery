//
//  MultiplierMeter.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/14/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class MultiplierMeter : SKNode {
    
    let digitHeight:CGFloat;
    
    var multiplierDigit:MultiplierDigitNode? = nil;
    
    init(position:CGPoint, height:CGFloat) {
        self.digitHeight = height;
        super.init();
        self.position = position;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("MultiplierMeter released");
    }
    
    func resetMultiplierMeter() {
        self.removeAllChildren();
        
        // Add multiplier digit node
        multiplierDigit = MultiplierDigitNode(position: CGPoint(x: 0, y: 0), face:0, targetWidth: nil, targetHeight:digitHeight);
        self.addChild(multiplierDigit!);
        
        multiplierDigit?.hidden = true;
    }
    
    func refreshMultiplierMeter() {
        let currentMultiplierNumber = GameRuntimeContext.currentScoreMultiplier;
        if (currentMultiplierNumber != multiplierDigit!.numberValue) {
            multiplierDigit!.removeAllActions();
            multiplierDigit?.alpha = 1;
            
            // Refresh multiplier digit if number changed
            multiplierDigit!.refreshTexture(currentMultiplierNumber);
            
            // Run action if not multiple by 1
            if (currentMultiplierNumber == 1) {
                multiplierDigit?.hidden = true;
            } else {
                multiplierDigit?.hidden = false;
                var remainingTime = GameRuntimeContext.multiplierRemainingTime;
                if (GameRuntimeContext.gameMode == GameMode.SPEED_MODE) {
                    remainingTime = CFTimeInterval(GameRuntimeContext.targetLevel + 1);
                }
                
                // Run fade away action
                multiplierDigit!.runAction(ActionFactory.getFadeAwayAction(remainingTime), completion: {
                    GameRuntimeContext.currentScoreMultiplier = 1;
                });
            }
        }
    }
}