//
//  CommingSoonText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/26/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class CommingSoonText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("COMMING_SOON_TEXT", comment: "CommingSoonText");
        //return GameTextConstants.COMMING_SOON_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.SUB_TITLE_FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Comming Soon Text";
        self.zPosition = LayerDepth.MENU_TEXT_ADD_ON.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
    }
}