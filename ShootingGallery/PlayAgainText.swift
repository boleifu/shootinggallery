//
//  PlayAgainText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/20/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class PlayAgainText : SelectableNode {
    
    override func getText() -> String? {
        return NSLocalizedString("PLAY_AGAIN_TEXT", comment: "PlayAgainText");
        //return GameTextConstants.PLAY_AGAIN_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Play Again Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Bottom;
    }
    
    override func select() {
        // Enter the game again
        GameRuntimeContext.menuStatus = MenuLifyCycle.ENTERING_GAME;
        GameRuntimeContext.targetGameMode = GameRuntimeContext.prevGameMode;
        unHilight();
    }
}