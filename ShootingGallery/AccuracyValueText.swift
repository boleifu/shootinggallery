//
//  AccuracyValueText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class AccuracyValueText : UpdatableText {
    
    override func getFontName() -> String? {
        return GameTextConstants.SUB_TITLE_FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Accuracy Value Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Left;
        self.verticalAlignmentMode = .Center;
    }
    
    override func update() {
        var accuracyPercentage:Float = 0;
        if (GameRuntimeContext.totalShotNumber != 0) {
            accuracyPercentage = (Float(GameRuntimeContext.totalShotHit)/Float(GameRuntimeContext.totalShotNumber))*100;
        }
        let outputString = String(format: "%d/%d (%.2f%%)",
                                  GameRuntimeContext.totalShotHit,
                                  GameRuntimeContext.totalShotNumber,
                                  accuracyPercentage);
        self.text = outputString;
    }
}