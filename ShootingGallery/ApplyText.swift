//
//  ApplyText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/22/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class ApplyText : SelectableNode {
    
    override func getText() -> String? {
        return NSLocalizedString("APPLY_TEXT", comment: "ApplyText");
        //return GameTextConstants.APPLY_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Apply Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Bottom;
    }
    
    override func select() {
        // Persist the configuration
        GameConfigUtils.persistGameConfig();
        
        // Back to game prep page
        GameRuntimeContext.menuPage = MenuPage.GAME_PREP_PAGE;
        unHilight();
    }
}