//
//  BullseyeText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class BullseyeText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("BULLSEYE_TEXT", comment: "BullseyeText");
        //return GameTextConstants.BULLSEYE_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Bullseye Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Center;
    }
}