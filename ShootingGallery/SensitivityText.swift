//
//  SensitivityText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/22/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SensitivityText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("SENSITIVITY_TEXT", comment: "SensitivityText");
        //return GameTextConstants.SENSITIVITY_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Sensitivity Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Center;
    }
}