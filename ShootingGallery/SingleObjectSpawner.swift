//
//  SingleObjectSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/13/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SingleObjectSpawner:AutoObjectSpawner {
    
    var minInitObjectNum:UInt32 = 3;
    var maxInitObjectNum:UInt32 = 5;
    
    override func getTarget() -> SKNode {
        // Not implemented
        fatalError("getTarget() has not been implemented");
    }
    
    override func getTargetAction(startPos:CGPoint) -> SKAction {
        // Not implemented
        fatalError("getTarget() has not been implemented");
    }
    
    /**
     * Fill the screen with a random number of objects
     */
    override func fillTheScreen() {
        let initObjectNum = Int(Utils.getRandomInt(minInitObjectNum, maxInitObjectNum));
        let objectGapX:CGFloat = GameContext.screenSize.width / CGFloat(initObjectNum + 1);
        for index in 1...initObjectNum {
            let objectPosX:CGFloat = objectGapX * CGFloat(index);
            let objectTarget = getTarget();
            objectTarget.position = CGPoint(x: objectPosX, y: self.initPos.y);
            self.gameContentLayer?.addChild(objectTarget);
            objectTarget.runAction(getTargetAction(objectTarget.position));
        }
    }
    
    override func spawnObjectByNumber(targetNum: Int) -> Int {
        // No default behavior
        return 0;
    }
}