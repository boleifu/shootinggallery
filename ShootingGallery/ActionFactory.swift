//
//  ActionFactory.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/12/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class ActionFactory {
    
    static func getWaveMoveAction(duration:NSTimeInterval, initPoint:CGPoint, xSpeed:CGFloat, yDisplacement:CGFloat, initAngle:CGFloat) -> SKAction {
        let moveAction = SKAction.customActionWithDuration(duration, actionBlock: {
            (node: SKNode!, elapsedTime: CGFloat) -> Void in
            let displacement = yDisplacement * sin(initAngle + CGFloat(M_PI) * elapsedTime);
            node.position.y = initPoint.y + displacement;
            node.position.x = initPoint.x + xSpeed * elapsedTime;
        });
        return moveAction;
    }
    
    static func getSneakAction(appearTime appearTime: NSTimeInterval, hideTime: NSTimeInterval) -> SKAction {
        let hideAction = SKAction.scaleYTo(0.1, duration: 0.5);
        let keepHideAction = SKAction.waitForDuration(hideTime);
        let appearAction = SKAction.scaleYTo(1, duration: 0.5);
        let keepAppearAction = SKAction.waitForDuration(appearTime);
        return SKAction.repeatActionForever(SKAction.sequence([keepAppearAction, hideAction, keepHideAction, appearAction]));
    }
    
    static func getLoadingAction(duration:NSTimeInterval) -> SKAction {
        return SKAction.sequence([SKAction.scaleXTo(0, duration: 0), SKAction.scaleXTo(1, duration: duration)]);
    }
    
    static func getFadeAwayAction(duration:NSTimeInterval) -> SKAction {
        return SKAction.fadeAlphaTo(0, duration: duration);
    }
}