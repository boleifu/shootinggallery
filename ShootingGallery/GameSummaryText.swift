//
//  GameSummaryText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/20/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GameSummaryText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("GAME_SUMMARY_TEXT", comment: "Game over page title");
        //GameTextConstants.GAME_SUMMARY_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Game Summary Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
    }
}