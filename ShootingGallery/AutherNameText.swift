//
//  AutherNameText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/29/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class AutherNameText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("AUTHER_NAME", comment: "AutherNameText");
        //return GameTextConstants.AUTHER_NAME;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Auther Name Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
    }
}