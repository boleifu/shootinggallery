//
//  SelectableNode.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/20/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SelectableNode : MenuText, Selectable {
    
    var isHilighted:Bool = false;
    
    func hilight() {
        isHilighted = true;
        self.fontColor = ColorConstants.RED_COLOR;
    }
    
    func unHilight() {
        isHilighted = false;
        self.fontColor = ColorConstants.BLACK_COLOR;
    }
    
    func select() {
        fatalError("select() has not been implemented");
    }
}