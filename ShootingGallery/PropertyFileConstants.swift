//
//  PropertyFileConstants.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import Foundation

class PropertyFileConstants {
    
    // User data
    static let USER_DATA_FILE_NAME = "UserGameData";
    static let USER_DATA_FILE_TYPE = "plist";
    static let USER_DATA_FILE_FULL_NAME = "UserGameData.plist";
    
    static let ARCADE_MODE_HIGHEST_SCORE = "ArcadeModeHighestScore";
    static let ARCADE_MODE_LONGEST_GAME_TIME = "ArcadeModeLongestGameTime";
    static let ARCADE_MODE_MAX_MULTIPLIER = "ArcadeModeMaxMultiplier";
    
    static let SPEED_MODE_HIGHEST_SCORE = "SpeedModeHighestScore";
    static let SPEED_MODE_LONGEST_GAME_TIME = "SpeedModeLongestGameTime";
    static let SPEED_MODE_MAX_MULTIPLIER = "SpeedModeMaxMultiplier";
    
    // Game Config
    static let GAME_CONFIG_FILE_NAME = "GameConfig";
    static let GAME_CONFIG_FILE_TYPE = "plist";
    static let GAME_CONFIG_FILE_FULL_NAME = "GameConfig.plist";
    
    static let CROSSHAIR_SENSITIVITY = "CrosshairSensitivity";
    static let BGM_VOLUMN = "BgmVolumn";
    static let SE_VOLUMN = "SeVolumn";
    static let ARCADE_BUTTON_ON_LEFT = "ArcadeButtonOnLeft";
}