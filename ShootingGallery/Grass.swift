//
//  Grass.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/13/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class Grass : GameEntity, Damagable {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "grass_long");
    }
    
    override func configProperties(){
        self.name = "Grass";
        self.zPosition = LayerDepth.ROW_THREE_BLOKER.getDepth();
        self.anchorPoint = CGPoint(x: 1, y: 0);  // Right-bottom corner
    }
    
    func isHit(hitPosOnTarget:CGPoint) -> Bool {
        return false;
    }
    
    func hit(hitPosOnNode:CGPoint) -> Bool {
        return false;
    }
    
}