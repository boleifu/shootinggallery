//
//  BackRowWaveSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/13/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class BackRowWaveSpawner:BlockerSpawner {
    
    override func getTarget() -> Water {
        let water = EntityFactory.createWater();
        water.zPosition = LayerDepth.ROW_TWO_BLOKER.getDepth();
        return water;
    }
    
    override func configSpawner() {
        self.objectLiftTime = self.objectLiftTime/4*3;
        super.configSpawner();
        //self.initPos.x -= self.objectWidth/2;
    }
}