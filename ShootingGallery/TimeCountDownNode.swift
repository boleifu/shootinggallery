//
//  TimeCountDownNode.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/11/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class TimeCountDownNode : EnvironmentObject {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: GameConstants.READY_TEXT_IMAGE);
    }
    
    override func configProperties(){
        self.name = "TimeCountDown";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5); // Middle
    }
    
    override func getAction() -> SKAction {
        let textAction = GameConstants.READY_GO;
        let audioAction = GameConstants.READY_GO_AUDIO;
        return SKAction.group([textAction, audioAction]);
    }
    
    override func afterAction() {
        // Power on the game
        GameRuntimeContext.gamePoweredOn = true;
    }
}