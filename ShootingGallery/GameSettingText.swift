//
//  GameSettingText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/22/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GameSettingText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("GAME_SETTING_TEXT", comment: "GameSettingText");
        //return GameTextConstants.GAME_SETTING_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Game Setting Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
    }
}