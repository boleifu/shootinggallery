//
//  InstructionPointer.swift
//  Duck Hunt
//
//  Created by Bolei Fu on 8/31/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class InstructionPointer : EnvironmentObject {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "pointer");
    }
    
    override func configProperties(){
        self.name = "InstructionPointer";
        self.zPosition = LayerDepth.DEBUG_OBJECT.getDepth();
        self.anchorPoint = CGPoint(x: 0.2, y: 0.8); // Left-middle
        //self.alpha = 0.8;
    }
    
    override func getAction() -> SKAction {
        let startTutroicalAction = SKAction.customActionWithDuration(0, actionBlock: {
            (node:SKNode!, elapsed:CGFloat) -> Void in
            GameRuntimeContext.isPlayingTutorial = true;
        });
        
        let moveAction = SKAction.sequence([SKAction.moveBy(CGVector(dx: GameContext.screenSize.width/6, dy:GameContext.screenSize.height/6), duration: 1.5),
            SKAction.moveBy(CGVector(dx: -GameContext.screenSize.width/6, dy:-GameContext.screenSize.height/6), duration: 1.5)]);
        let moveToButton = SKAction.moveTo(GameContext.arcadeButtonPos, duration: 0);
        return SKAction.sequence([startTutroicalAction, moveAction, moveToButton, SKAction.waitForDuration(2)]);
    }
    
    override func afterAction() {
        GameRuntimeContext.isPlayingTutorial = false;
        self.removeFromParent();
    }
}