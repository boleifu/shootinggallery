//
//  GameTextConstants.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/18/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import Foundation

class GameTextConstants {
    static let UNKNON_TEXT = "Unknown";
    /*
    static let GAME_TITLE = "Duck Hunt";
    static let GAME_SUB_TITLE = "Carnival";
    static let PLAY_TEXT = "Play";
    static let START_TEXT = "Start";
    static let CREDITS_TEXT = "Credits";
    
    static let SELECT_GAME_MODE = "Select Game Mode";
    static let ARCADE_MODE = "Arcade Mode";
    static let SPEED_MODE = "Speed Mode"
    static let ONLINE_BATTLE = "Online Battle";
    
    static let ARCADE_MODE_DESC_1 = "Show me your skill!!!";
    static let ARCADE_MODE_DESC_2 = "Targets keep spawning."
    static let ARCADE_MODE_DETAIL_1 = "Hit target: Score +10";
    static let ARCADE_MODE_DETAIL_2 = "Hit bullseye: Score +100";
    static let ARCADE_MODE_DETAIL_3 = "Hit bullseye: Multiplier +1";
    static let ARCADE_MODE_DETAIL_4 = "Destory stick: Time +5s"
    
    static let SPEED_MODE_DESC_1 = "Bullseye hit only!!!"
    static let SPEED_MODE_DESC_2 = "Reload speed increased."
    static let SPEED_MODE_DETAIL_1 = "Hit bullseye: Score +100";
    static let SPEED_MODE_DETAIL_2 = "Clean up targets: +Time";
    static let SPEED_MODE_DETAIL_3 = "Clean up targets: Multiplier +1";
    static let SPEED_MODE_DETAIL_4 = "Destory stick: No bonus"
    
    static let COMMING_SOON_TEXT = "Comming Soon!!!"
    
    static let GAME_SUMMARY_TEXT = "Game Summary";
    static let SCORE_TEXT = "Score:";
    static let HIGHEST_SCORE_TEXT = "Highest Score:";
    static let NEW_HIGHTEST_SCORE = "New Highest Score!!!";
    static let GAME_TIME_TEXT = "Game Time:";
    static let NEW_LONGEST_GAME_TIME = "New Longest Game Time!!!";
    static let MAX_MULTIPLIER_TEXT = "Max Multiplier:";
    static let NEW_MAX_MULTIPLIER = "New Highest Multiplier!!!";
    static let ACCURACY_TEXT = "Accuracy:";
    static let BULLSEYE_TEXT = "Bullseye:";
    static let HIGHEST_TEXT = "Highest:";
    static let SECOND_TEXT = "sec";
    
    static let GAME_SETTING_TEXT = "Game Setting";
    static let BGM_TEXT = "Music Volumn:";
    static let SE_TEXT = "Sound Effect:";
    static let SENSITIVITY_TEXT = "Sensitivity:";
    static let ARCADE_BUTTON_POS_TEXT = "Button Position:"
    static let LEFT_TEXT = "Left";
    static let RIGHT_TEXT = "Right";
    
    static let GAME_FULL_NAME_TEXT = "Duck Hunt - Carnival";
    static let EMAIL_ME_TEXT = "Email Me";
    static let DEVELOPED_BY_TEXT = "Developed By";
    static let AUTHER_NAME = "Bolei Fu";
    static let AUTHOR_EMAIL = "bolei.game@gmail.com";
    static let LICENSE_TEXT = "License";
    */
    
    static let LICENSE_DETAILS_LINE_1:[String] = ["Shooting Gallery pack", "Voiceover Pack No.1", "Metal Ping.aif", "Colonel Bogey March (Piano Arrangement)", "Font \"SOME TIME LATER\"", "Other game assets"];
    static let LICENSE_DETAILS_LINE_2:[String] = ["Auther: Kenney Vleugels", "Auther: Kenney Vleugels", "Auther: timgormly", "Author: Kenneth J. Alford", "Auther: Fredrick R Brennan", "License: Public Domain"];
    static let LICENSE_DETAILS_LINE_3:[String] = ["License: CC0", "License: CC0", "https://www.freesound.org", "Performer: Jean-Francois Noel", "License: OFL-1.1", ""];
    static let LICENSE_DETAILS_LINE_4:[String] = ["www.kenney.nl", "www.kenney.nl", "", "License: CC BY 3.0", "", ""];
    static let LICENSE_DETAILS_LINE_5:[String] = ["", "", "", "https://musopen.org", "", ""];
    
    /*
    static let SETTING_TEXT = "Setting";
    static let PLAY_AGAIN_TEXT = "Play Again";
    static let BACK_TEXT = "Back";
    static let APPLY_TEXT = "Apply";
    */
    
    
    static let FONT_NAME = "Some Time Later";
    static let SUB_TITLE_FONT_NAME = "Lionelofparis";
}
