//
//  VersionText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/28/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class VersionText : MenuText {
    
    override func getText() -> String? {
        return Utils.version();
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Version Text";
        self.zPosition = LayerDepth.MENU_SUB_TITLE.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Bottom;
    }
}