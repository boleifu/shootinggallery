//
//  RowTwoDuckTargetSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/14/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class RowTwoDuckTargetSpawner:DuckTargetSpawner {
    
    override func getTarget() -> ShootingTarget {
        let target = EntityFactory.createDuckTarget();
        target.addOn!.zPosition = LayerDepth.ROW_TWO_TARGET_ADDON.getDepth();
        target.base.zPosition = LayerDepth.ROW_TWO_TARGET_BASE.getDepth();
        return target;
    }
    
    override func configSpawner() {
        super.configSpawner();
        self.spawnCoolDownTime = 2.5;
    }
}