//
//  SmallDigitNumberNode.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/11/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SmallDigitNumberNode : GameEntity {
    
    var numberValue:Int = 0;
    var digitCount:Int = 1;
    
    override func generateTexture() -> SKTexture? {
        return TextureFactory.getNumberTexture(0, true);
    }
    
    override func configProperties(){
        self.name = "SmallDigitNumber";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5); // Middle
    }
    
    func refreshTexture(number:Int) {
        let newTexture = TextureFactory.getNumberTexture(number, true);
        let newDigitCount = Utils.getDigitCount(number);
        
        let currentHeight = self.size.height;
        
        numberValue = number;
        self.texture = newTexture;
        
        // Resize if digit number changed
        if (digitCount != newDigitCount) {
            digitCount = newDigitCount;
            self.resize(nil, currentHeight);
        }
    }
    
    
}