//
//  SmallScoreIcon.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/11/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SmallScoreIcon : GameEntity {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "text_score_small");
    }
    
    override func configProperties(){
        self.name = "SmallScoreIcon";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: 0, y: 1);  // Top-left
    }
}