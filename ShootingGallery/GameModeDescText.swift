//
//  ArcadeModeDescText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/26/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GameModeDescText : MenuText {
    
    // Constructor
    init(fontSize:CGFloat, fontColor:UIColor, text:String) {
        super.init(fontSize:fontSize, fontColor:fontColor);
        self.text = text;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented");
    }
    
    override func getText() -> String? {
        return GameTextConstants.UNKNON_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.SUB_TITLE_FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Game Mode Desc Text";
        self.zPosition = LayerDepth.MENU_TEXT_ADD_ON.getDepth();
        self.horizontalAlignmentMode = .Left;
        self.verticalAlignmentMode = .Center;
    }
}