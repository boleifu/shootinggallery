//
//  GameRuntimeContext.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/10/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

enum GameMode {
    case MENU;
    case ARCADE_MODE;
    case SPEED_MODE;
}

enum MenuLifyCycle {
    case ENABLED;
    case DISABLED;
    case ENTERING_GAME;
    case LEAVING_GAME;
}

enum MenuPage {
    case UNKNOWN;
    case WELCOME_PAGE;
    case CREDIT_PAGE;
    case GAME_PREP_PAGE;
    case SETTING_PAGE;
    case GAME_OVER_PAGE;
    case GAME_PAGE;
}

class GameRuntimeContext {
    // Game Timer
    static var currentGameTime:CFTimeInterval = 100; // Start from 100 since some timer treat 0 as a unset time
    static var lastUpdatedRealTime:CFTimeInterval = 0;
    
    // Game config
    static var gameDuration:Int = 0;
    static var maxAmmo:Int = 1;
    static var fireCoolDownTime:CFTimeInterval = 0.5;
    static var reloadDuration:CFTimeInterval = 1;
    static var speedModeReloadDuration:CFTimeInterval = 0.5;
    static var multiplierRemainingTime:CFTimeInterval = 2;
    
    // Game lift cycle state
    static var gameMode:GameMode = GameMode.MENU;
    static var menuStatus:MenuLifyCycle = MenuLifyCycle.ENABLED;
    static var menuPage:MenuPage = MenuPage.WELCOME_PAGE;
    static var targetGameMode:GameMode = GameMode.MENU; // The mode that game is going to enter
    static var prevGameMode:GameMode = GameMode.MENU;   // The mode that game just finished
    
    static var gamePoweredOn = false;
    static var powerOnInProgress = false;
    static var gameStarted = false;
    
    // Runtime stats
    static var isPlayingTutorial = false;
    
    static var gameStartTime:CFTimeInterval = 0;
    static var gameEndTime:CFTimeInterval = 0;
    
    static var currentScore:Int = 0;
    static var currentScoreMultiplier:Int = 1;
    
    static var isReloading = false;
    static var breakTheRecord:Bool = false;
    
    // Speed mode execlude
    static var targetLevel:Int = 0;
    static var targetRemains:Int = 0;
    
    // Game Summary
    static var totalGameTime:Int = 0;
    static var maxScoreMultiplier:Int = 1;
    static var totalShotNumber:Int = 0;
    static var totalShotHit:Int = 0;
    static var bullseyeHit:Int = 0;
    
    
    static func resetMenuModeRuntimeContext() {
        GameRuntimeContext.gameMode = GameMode.MENU;
    }
    
    static func resetGameRuntimeContext(gameTime:Int, _ maxAmmo:Int) {
        GameRuntimeContext.gameDuration = gameTime;
        GameRuntimeContext.maxAmmo = maxAmmo;
        GameRuntimeContext.prevGameMode = GameRuntimeContext.gameMode;

        // Init runtime state
        GameRuntimeContext.gameStarted = false;
        GameRuntimeContext.currentScore = 0;
        GameRuntimeContext.currentScoreMultiplier = 1;
        GameRuntimeContext.isReloading = false;
        GameRuntimeContext.breakTheRecord = false;
        
        // Speed mode execlusive
        GameRuntimeContext.targetLevel = 0;
        GameRuntimeContext.targetRemains = 0;
        
        // Reset game statistics
        resetGameSummary();
    }
    
    static func addScore(score:Int) {
        GameRuntimeContext.currentScore += score;
    }
    
    static func setGameStarted(gameStartTime gameStartTime:CFTimeInterval) {
        GameRuntimeContext.gameStartTime = gameStartTime;
        GameRuntimeContext.gameEndTime = gameStartTime + CFTimeInterval(gameDuration);
        GameRuntimeContext.gameStarted = true;
    }
    
    static func resetGameSummary() {
        GameRuntimeContext.totalGameTime = 0;
        GameRuntimeContext.maxScoreMultiplier = 1;
        GameRuntimeContext.totalShotNumber = 0;
        GameRuntimeContext.totalShotHit = 0;
        GameRuntimeContext.bullseyeHit = 0;
    }
    
    static func getGameSummaryReport() -> String {
        var report:String = "";
        report += "Game Score: \(currentScore)\n";
        report += "Game Time: \(Int(gameEndTime - gameStartTime))\n";
        report += "Max Multiplier: X\(maxScoreMultiplier)\n";
        report += "Total shot: \(totalShotNumber)\n";
        report += "Target hit: \(totalShotHit)\n";
        report += "Bullseye hit: \(bullseyeHit)\n";
        return report;
    }
}
