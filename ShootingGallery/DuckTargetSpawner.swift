//
//  DuckTargetSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/6/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class DuckTargetSpawner:SingleObjectSpawner {
    
    let minLifeTime:UInt32 = 5;
    let maxLifeTime:UInt32 = 10;
    
    let minYDisplacementRatio:UInt32 = 2;
    let maxYDisplacementRatio:UInt32 = 5;
    
    
    override func getTarget() -> ShootingTarget {
        return EntityFactory.createDuckTarget();
    }
    
    override func getTargetAction(startPos:CGPoint) -> SKAction {
        let targetLifeTime = NSTimeInterval(Utils.getRandomInt(minLifeTime, maxLifeTime));
        let initAngle = CGFloat(M_PI * Utils.getRandomPercent());
        let yDisplacement = GameContext.duckWidth / CGFloat(Utils.getRandomInt(minYDisplacementRatio, maxYDisplacementRatio));
        let xSpeed = (GameContext.gameContentSize.width + GameContext.duckWidth/2) / CGFloat(targetLifeTime);
        
        let moveAction = ActionFactory.getWaveMoveAction(targetLifeTime, initPoint: startPos, xSpeed: xSpeed, yDisplacement: yDisplacement, initAngle: initAngle);
        
        //let moveAction = SKAction.moveBy(CGVector(dx: GameContext.gameContentSize.width, dy: 0), duration: lifeTime);
        return SKAction.sequence([moveAction, SKAction.removeFromParent()]);
    }
    
    override func configSpawner() {
        self.spawnCoolDownTime = 1.5;
    }
    
    override func fillTheScreen() {
        // Do Nothing
    }
    
    override func getTargetInitPos() -> CGPoint {
        return CGPoint(x: self.initPos.x - GameContext.duckWidth/2, y: self.initPos.y);
    }
}