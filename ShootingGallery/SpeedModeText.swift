//
//  SpeedModeText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/25/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SpeedModeText : SelectableNode {
    
    override func getText() -> String? {
        return NSLocalizedString("SPEED_MODE", comment: "SpeedModeText");
        //return GameTextConstants.SPEED_MODE;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Speed Mode Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Center;
    }
    
    override func select() {
        // Move to the speed mode, and disable the menu
        GameRuntimeContext.menuStatus = MenuLifyCycle.ENTERING_GAME;
        GameRuntimeContext.targetGameMode = GameMode.SPEED_MODE;
        unHilight();
    }
}