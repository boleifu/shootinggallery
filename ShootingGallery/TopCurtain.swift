//
//  TopCurtain.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/5/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class TopCurtain : GameEntity {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "curtain_straight");
    }
    
    override func configProperties(){
        self.name = "TopCurtain";
        self.zPosition = LayerDepth.TOP_CURTAIN.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 1);  // Top-middle
    }
}