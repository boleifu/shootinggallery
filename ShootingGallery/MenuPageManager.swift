//
//  MenuPageManager.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/20/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

/**
 * This class manage the lifecycle of one page
 */
class MenuPageManager {

    weak var menuLayer:SKNode?;
    let pageLayer = SKNode();
    
    var pageType:MenuPage = MenuPage.UNKNOWN;
    var menuTextNodes:[MenuText] = [];
    var selectableNodes:[Selectable] = [];
    
    init(menuLayer:SKNode) {
        self.menuLayer = menuLayer;
        menuLayer.addChild(pageLayer);
        
        setupPage();
        
        // Disabled by default
        pageLayer.hidden = true;
    }
    
    func setupPage() {
        fatalError("setupPage() has not been implemented");
    }
    
    func enablePage() {
        pageLayer.hidden = false;
    }
    
    func disablePage() {
        pageLayer.hidden = true;
    }
    
    func afterUpdate() {
        // No default behavior
    }
}
