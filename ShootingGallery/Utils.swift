//
//  Utils.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/4/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class Utils {
    
    static func fitInBoundary(targetPoint:CGPoint, boundary:CGSize) -> CGPoint {
        let x:CGFloat = min(max(targetPoint.x, 0), boundary.width);
        let y:CGFloat = min(max(targetPoint.y, 0), boundary.height);
        return CGPoint(x: x, y: y);
    }
    
    // Get the digit count of a given number
    // Example: 10 -> 2, -5 -> 2
    static func getDigitCount(number:Int) -> Int {
        if (number == 0) {
            return 1;
        }
        
        var digitCount:Int = (Int)(log10(Double(abs(number))) + 1);
        if (number < 0) {
            digitCount += 1;
        }
        return digitCount;
    }
    
    // Get a random integer with in the range [start, end)
    static func getRandomInt(start:UInt32, _ end:UInt32) -> UInt32 {
        let minNum = min(start, end);
        let maxNum = max(start, end);
        let diff = maxNum - minNum;
        return arc4random_uniform(diff) + start;
    }
    
    static func getRandomPercent() -> Double {
        return Double(arc4random_uniform(101) / 100);
    }
    
    // Get the version of the software
    static func version() -> String {
        let dictionary = NSBundle.mainBundle().infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"]as! String
        return "V \(version).\(build)"
    }
}
