//
//  MaxMultiplierText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class MaxMultiplierText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("MAX_MULTIPLIER_TEXT", comment: "MaxMultiplierText");
        //return GameTextConstants.MAX_MULTIPLIER_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Max Multiplier Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Center;
    }
}