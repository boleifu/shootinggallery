//
//  CreditPageManager.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/29/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GameCreditsPageManager : MenuPageManager {
    
    var updatabelTexts:[UpdatableText] = [];
    
    override func setupPage() {
        self.pageType = MenuPage.CREDIT_PAGE;
        
        // Add Title
        let titleText = MenuObjectFactory.createGameFullNameText();
        titleText.position = GameContext.pageTitlePos;
        menuTextNodes.append(titleText);
        pageLayer.addChild(titleText);
        
        // Add developer info
        let developerInfoGapY = (GameContext.menuContentStartY - GameContext.menuContentEndY)/4;
        var currentY = GameContext.menuContentStartY;
        
        let developedBy = MenuObjectFactory.createDevelopedByText();
        developedBy.position = CGPoint(x: GameContext.developerInfoPosX, y: currentY);
        menuTextNodes.append(developedBy);
        pageLayer.addChild(developedBy);
        
        currentY -= developerInfoGapY;
        
        let autherName = MenuObjectFactory.createAutherNameText();
        autherName.position = CGPoint(x: GameContext.developerInfoPosX, y: currentY);
        menuTextNodes.append(autherName);
        pageLayer.addChild(autherName);
        
        currentY -= developerInfoGapY;
        
        let autherEmail = MenuObjectFactory.createAutherEmailText();
        autherEmail.position = CGPoint(x: GameContext.developerInfoPosX, y: currentY);
        menuTextNodes.append(autherEmail);
        pageLayer.addChild(autherEmail);
        
        // Add license info
        let licenseInfoGapY = (GameContext.menuContentStartY - GameContext.menuContentEndY)/6;
        currentY = GameContext.menuContentStartY;
        
        let licenseText = MenuObjectFactory.createLicenseText();
        licenseText.position = CGPoint(x: GameContext.licenseInfoPosX, y: GameContext.menuContentStartY);
        menuTextNodes.append(licenseText);
        pageLayer.addChild(licenseText);
        
        currentY -= licenseInfoGapY;
        
        // Add license details
        let licenseDetailText1 = MenuObjectFactory.createLicenseDetailsText(GameTextConstants.LICENSE_DETAILS_LINE_1);
        licenseDetailText1.position = CGPoint(x: GameContext.licenseInfoPosX, y: currentY);
        updatabelTexts.append(licenseDetailText1);
        pageLayer.addChild(licenseDetailText1)
        
        currentY -= licenseInfoGapY;
        
        let licenseDetailText2 = MenuObjectFactory.createLicenseDetailsText(GameTextConstants.LICENSE_DETAILS_LINE_2);
        licenseDetailText2.position = CGPoint(x: GameContext.licenseInfoPosX, y: currentY);
        updatabelTexts.append(licenseDetailText2);
        pageLayer.addChild(licenseDetailText2)
        
        currentY -= licenseInfoGapY;
        
        let licenseDetailText3 = MenuObjectFactory.createLicenseDetailsText(GameTextConstants.LICENSE_DETAILS_LINE_3);
        licenseDetailText3.position = CGPoint(x: GameContext.licenseInfoPosX, y: currentY);
        updatabelTexts.append(licenseDetailText3);
        pageLayer.addChild(licenseDetailText3)
        
        currentY -= licenseInfoGapY;
        
        let licenseDetailText4 = MenuObjectFactory.createLicenseDetailsText(GameTextConstants.LICENSE_DETAILS_LINE_4);
        licenseDetailText4.position = CGPoint(x: GameContext.licenseInfoPosX, y: currentY);
        updatabelTexts.append(licenseDetailText4);
        pageLayer.addChild(licenseDetailText4)
        
        currentY -= licenseInfoGapY;
        
        let licenseDetailText5 = MenuObjectFactory.createLicenseDetailsText(GameTextConstants.LICENSE_DETAILS_LINE_5);
        licenseDetailText5.position = CGPoint(x: GameContext.licenseInfoPosX, y: currentY);
        updatabelTexts.append(licenseDetailText5);
        pageLayer.addChild(licenseDetailText5)
        
        currentY -= licenseInfoGapY;
        
        
        // Add email me text
        /*
        let semailMeText = EntityFactory.createEmailMeText();
        semailMeText.position = GameContext.settingTextPos;
        selectableNodes.append(semailMeText);
        pageLayer.addChild(semailMeText);
         */
        
        // Add Back text
        let backText = MenuObjectFactory.createBackText(MenuPage.WELCOME_PAGE);
        backText.position = GameContext.actionOneTextPos;
        selectableNodes.append(backText);
        pageLayer.addChild(backText);
    }
    
    override func enablePage() {
        super.enablePage();
        
        let licenseUpdateAction = SKAction.customActionWithDuration(0.0, actionBlock:
            { (node:SKNode!, elapsed:CGFloat) -> Void in
                for updatableText in self.updatabelTexts {
                    updatableText.update();
                }
        });
        let licenseSliderAction = SKAction.repeatActionForever(SKAction.sequence([SKAction.waitForDuration(5), licenseUpdateAction]));
        
        pageLayer.runAction(licenseSliderAction);
    }
    
    override func disablePage() {
        super.disablePage();
        pageLayer.removeAllActions();
    }
}