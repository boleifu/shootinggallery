//
//  MultiplierDigitNode.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/14/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class MultiplierDigitNode : GameEntity {
    
    var numberValue:Int = 1;
    var digitCount:Int = 1;
    
    override func generateTexture() -> SKTexture? {
        return TextureFactory.getNumberTexture(1, false, SpecialChar.CROSS);
    }
    
    override func configProperties(){
        self.name = "MultiplierDigitNumber";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: 0, y: 1); // Top-left
    }
    
    func refreshTexture(number:Int) {
        let newTexture = TextureFactory.getNumberTexture(number, false, SpecialChar.CROSS);
        let newDigitCount = Utils.getDigitCount(number);
        
        let currentHeight = self.size.height;
        
        numberValue = number;
        self.texture = newTexture;
        
        // Resize if digit number changed
        if (digitCount != newDigitCount) {
            digitCount = newDigitCount;
            self.resize(nil, currentHeight);
        }
    }
    
    
}