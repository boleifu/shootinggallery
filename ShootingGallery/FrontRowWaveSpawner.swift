//
//  FrontRowWaveSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/13/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class FrontRowWaveSpawner:BlockerSpawner {
    
    override func getTarget() -> Water {
        return EntityFactory.createWater();
    }
}