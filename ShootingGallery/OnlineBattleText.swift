//
//  OnlineBattleText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/25/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class OnlineBattleText : SelectableNode {
    
    override func getText() -> String? {
        return NSLocalizedString("ONLINE_BATTLE", comment: "OnlineBattleText");
        //return GameTextConstants.ONLINE_BATTLE;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Online Battle Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Center;
    }
    
    override func select() {
        // TODO: Enter online battle
        //GameRuntimeContext.menuStatus = MenuLifyCycle.ENTERING_GAME;
        unHilight();
    }
}