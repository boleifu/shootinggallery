//
//  ButtonPosValueText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/23/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class ButtonPosValueText : SelectableNode {
    
    override func getText() -> String? {
        return getValueString();
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Button Pos Value Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Left;
        self.verticalAlignmentMode = .Center;
    }
    
    override func select() {
        // Revert the boolean
        GameConfigUtils.setArcadeButtonOnLeft(!GameConfigUtils.isArcadeButtonOnLeft());
        
        // Update the button position
        self.text = getValueString();
        GameContext.updateArcadeButtonPos();
        
        unHilight();
    }
    
    private func getValueString() -> String {
        if (GameConfigUtils.isArcadeButtonOnLeft()) {
            return NSLocalizedString("LEFT_TEXT", comment: "ButtonPosValueText");
            //return GameTextConstants.LEFT_TEXT;
        } else {
            return NSLocalizedString("RIGHT_TEXT", comment: "ButtonPosValueText");
            //return GameTextConstants.RIGHT_TEXT;
        }
    }
}