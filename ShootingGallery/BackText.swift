//
//  BackText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/20/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class BackText : SelectableNode {
    
    let destPage:MenuPage;
    
    // Constructor
    init(fontSize:CGFloat, fontColor:UIColor, destPage:MenuPage) {
        self.destPage = destPage;
        super.init(fontSize:fontSize, fontColor:fontColor);
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func getText() -> String? {
        return NSLocalizedString("BACK_TEXT", comment: "BackText");
        //return GameTextConstants.BACK_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Back Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Bottom;
    }
    
    override func select() {
        // Move back to the dest page
        GameRuntimeContext.menuPage = destPage;
        unHilight();
    }
}