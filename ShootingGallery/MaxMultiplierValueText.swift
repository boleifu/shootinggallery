//
//  MaxMultiplierValueText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class MaxMultiplierValueText : UpdatableText {
    
    override func getFontName() -> String? {
        return GameTextConstants.SUB_TITLE_FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Max Multiplier Value Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Left;
        self.verticalAlignmentMode = .Center;
    }
    
    override func update() {
        let highestText:String = NSLocalizedString("HIGHEST_TEXT", comment: "highestText");
        let highestMultiplierText:String = NSLocalizedString("NEW_MAX_MULTIPLIER", comment: "MaxMultiplierValueText");
        
        var maxMutiplier:Int = 0;
        if (GameRuntimeContext.prevGameMode == GameMode.ARCADE_MODE) {
            maxMutiplier = UserDataUtils.getArcadeModeMaxMultiplier();
        } else if (GameRuntimeContext.prevGameMode == GameMode.SPEED_MODE) {
            maxMutiplier = UserDataUtils.getSpeedModeMaxMultiplier();
        }

        let currentMultiplier = GameRuntimeContext.maxScoreMultiplier;
        
        if (currentMultiplier > maxMutiplier) {
            GameRuntimeContext.breakTheRecord = true;
            
            if (GameRuntimeContext.prevGameMode == GameMode.ARCADE_MODE) {
                UserDataUtils.setArcadeModeMaxMultiplier(currentMultiplier);
            } else if (GameRuntimeContext.prevGameMode == GameMode.SPEED_MODE) {
                UserDataUtils.setSpeedModeMaxMultiplier(currentMultiplier);
            }
            
            UserDataUtils.setArcadeModeMaxMultiplier(currentMultiplier);
            self.text = String("X\(currentMultiplier) (\(highestMultiplierText))");
            //self.text = String("X\(currentMultiplier) (\(GameTextConstants.NEW_MAX_MULTIPLIER))");
        } else {
            self.text = String("X\(currentMultiplier) (\(highestText) X\(maxMutiplier))");
            //self.text = String("X\(currentMultiplier) (\(GameTextConstants.HIGHEST_TEXT) X\(maxMutiplier))");
        }
    }
}