//
//  GoldAmmoIcon.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/12/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GoldAmmoIcon : AmmoIcon {
    
    override func getFilledTexture() -> SKTexture {
        return SKTexture(imageNamed: "icon_bullet_gold_long");
    }
    
    override func getEmptyTexture() -> SKTexture {
        return SKTexture(imageNamed: "icon_bullet_empty_long");
    }
}