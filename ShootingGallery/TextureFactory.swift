//
//  TextureFactory.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/12/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

enum SpecialChar {
    case NONE;
    case PLUS;
    case CROSS;
}

/**
 * This is the factory class for generating some common textures of game entities.
 */
class TextureFactory {
    
    static func getPlusTexture(isSmallSize:Bool = false) -> SKTexture {
        if (isSmallSize) {
            return SKTexture(imageNamed: "text_plus_small");
        } else {
            return SKTexture(imageNamed: "text_plus");
        }
    }
    
    static func getCrossTexture(isSmallSize:Bool = false) -> SKTexture {
        if (isSmallSize) {
            return SKTexture(imageNamed: "text_cross_small");
        } else {
            return SKTexture(imageNamed: "text_cross");
        }
    }
    
    static func getSingleDigitTexture(number:Int, _ isSmallSize:Bool = false) -> SKTexture {
        var inputNumber = number;
        if (inputNumber > 9 || inputNumber < 0) {
            print("Error: Number(\(inputNumber)) not in range.");
            inputNumber = 0;
        }
        
        if (!isSmallSize) {
            return SKTexture(imageNamed: GameConstants.NUMBER_IMAGES[inputNumber]);
        } else {
            return SKTexture(imageNamed: GameConstants.SMALL_NUMBER_IMAGES[inputNumber]);
        }
    }
    
    static func getNumberTexture(number:Int, _ isSmallSize:Bool = false, _ specChar:SpecialChar = SpecialChar.NONE) -> SKTexture {
        let inputNumber = max(number, 0);
        
        if (inputNumber < 10 && specChar == SpecialChar.NONE) {
            return getSingleDigitTexture(inputNumber, isSmallSize);
        }
        
        // Generate texture
        let baseNode = SKNode();
        var current_x:CGFloat = 0;
        var tempValue:Int = inputNumber;
        while (tempValue > 0) {
            let currentDigit:Int = tempValue % 10;
            tempValue = tempValue / 10;
            
            var digitNode:SKSpriteNode = SKSpriteNode(texture: getSingleDigitTexture(currentDigit, isSmallSize));
            if (digitNode.size.width <= 0) {
                // Failed to fetch the texture, try again
                print("Failed to create the texture node for \(currentDigit). Try again.");
                digitNode = SKSpriteNode(texture: getSingleDigitTexture(currentDigit, isSmallSize));
            }
            digitNode.anchorPoint = CGPoint(x: 1, y: 0);  // Right-bottom corner
            digitNode.position = CGPoint(x: current_x, y: 0);
            baseNode.addChild(digitNode);
            
            current_x -= digitNode.size.width;
            //if (addPlus) {
            //    print("currentX = \(current_x). digitNodeWidth = \(digitNode.size.width)");
            //}
        }
        
        if (specChar != SpecialChar.NONE) {
            if (specChar == SpecialChar.PLUS) {
                let plusNode = SKSpriteNode(texture: getPlusTexture(isSmallSize));
                plusNode.anchorPoint = CGPoint(x: 1, y: 0);  // Right-bottom corner
                plusNode.position = CGPoint(x: current_x, y: 0);
                baseNode.addChild(plusNode);
                current_x -= plusNode.size.width;
                //print("Base has \(baseNode.children.count) children. CurrentX = \(current_x)");
            } else if (specChar == SpecialChar.CROSS) {
                let crossNode = SKSpriteNode(texture: getCrossTexture(isSmallSize));
                crossNode.anchorPoint = CGPoint(x: 1, y: 0);  // Right-bottom corner
                crossNode.position = CGPoint(x: current_x, y: 0);
                baseNode.addChild(crossNode);
                current_x -= crossNode.size.width;
            }
        }
        
        // Convert sprite to texture
        let textureView:SKView = SKView();
        let texture:SKTexture = textureView.textureFromNode(baseNode)!;
        texture.filteringMode = SKTextureFilteringMode.Nearest;
        return texture;
    }
}