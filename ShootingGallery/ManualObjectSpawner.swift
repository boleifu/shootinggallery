//
//  ManualObjectSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/13/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class ManualObjectSpawner {
    
    weak var gameContentLayer:SKNode?;  // Where the target add to, have to be out of screen
    var initPos:CGPoint = CGPoint.zero;
    
    var lastUpdatedTime:NSTimeInterval = 0;
    var objectVelocity:CGVector = CGVector.zero;
    var spawnedObjects:LinkedList<SKSpriteNode> = LinkedList();
    
    init(gameContentLayer:SKNode?, initPos:CGPoint) {
        self.gameContentLayer = gameContentLayer;
        self.initPos = initPos;
        
        configSpawner();
    }
    
    deinit {
        print("ManualObjectSpawner released.")
    }
    
    func update(currentTime: CFTimeInterval) {
        updateSpawnedObjects(currentTime);
        spawnNewObject();
        removeExpiredObject();

        lastUpdatedTime = currentTime;
    }
    
    func getTarget() -> SKSpriteNode {
        // Not implemented
        fatalError("getTarget() has not been implemented");
    }
    
    func configSpawner() {
        // No default behavior
    }
    
    func fillTheScreen() {
        let sampleObject = getTarget();
        let objectWidth = sampleObject.size.width;
        let initObjectNum = Int(GameContext.screenSize.width / objectWidth);
        for index:Int in 0...initObjectNum {
            let newObject = getTarget();
            newObject.position.x = initPos.x + objectWidth * CGFloat(index);
            newObject.position.y = initPos.y;
            gameContentLayer?.addChild(newObject);
            spawnedObjects.insert(newObject, atIndex: 0);
        }
        print("Spawned \(index) objects to fill the screen");
    }
    
    // Mannual object actions
    
    func updateSpawnedObjects(currentTime: CFTimeInterval) {
        let deltaTime = CGFloat(currentTime - lastUpdatedTime);
        
        // Move all objects
        for spawnedObject in spawnedObjects.toArray() {
            spawnedObject.position.x = spawnedObject.position.x + objectVelocity.dx * deltaTime;
            spawnedObject.position.y = spawnedObject.position.y + objectVelocity.dy * deltaTime;
        }
    }
    
    
    func spawnNewObject() {
        // Spawn new object if the last spawned one has appears on the screen
        var needSpawn = true;
        if (!spawnedObjects.isEmpty) {
            let latestObject:SKSpriteNode = spawnedObjects.first!.value;
            needSpawn = latestObject.position.x >= initPos.x;
        }
        
        if (needSpawn) {
            let newObject = getTarget();
            newObject.position.x = initPos.x - newObject.size.width;
            newObject.position.y = initPos.y;
            gameContentLayer?.addChild(newObject);
            spawnedObjects.insert(newObject, atIndex: 0);
        }
    }
    
    func removeExpiredObject() {
        // Remove the oldest object if out of screen already
        if (spawnedObjects.isEmpty) {
            return;
        }
        
        let oldestObject:SKSpriteNode = spawnedObjects.last!.value;
        if (oldestObject.position.x > GameContext.screenSize.width) {
            spawnedObjects.removeLast();
            oldestObject.removeFromParent();
        }
    }
}
