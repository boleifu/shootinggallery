//
//  NumberValueText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/22/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SettingValueText : SelectableNode {
    
    let filledSign:String = "■";
    let emptySign:String = "□"
    let optionKey:String;
    
    var currentNum:Int = 0;
    
    let minNum:Int;
    let maxNum:Int;
    
    // Constructor
    init(fontSize:CGFloat, fontColor:UIColor, optionKey:String, minNum:Int, maxNum:Int) {
        self.optionKey = optionKey;
        self.minNum = minNum;
        self.maxNum = maxNum;
        super.init(fontSize:fontSize, fontColor:fontColor);
        
        self.currentNum = getCurrentOptionValue();
        self.text = getValueString();
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func getText() -> String? {
        return getValueString();
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Number value Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Left;
        self.verticalAlignmentMode = .Center;
    }
    
    override func select() {
        // Loop from min to max
        currentNum += 1;
        if (currentNum > maxNum) {
            currentNum = minNum;
        }
        
        self.text =  getValueString();
        
        // Update the game config
        updateOptionValue();
        
        unHilight();
    }
    
    private func getCurrentOptionValue() -> Int {
        if (optionKey == PropertyFileConstants.CROSSHAIR_SENSITIVITY) {
            return GameConfigUtils.getCrosshairSensitivity();
        } else if (optionKey == PropertyFileConstants.BGM_VOLUMN) {
            return GameConfigUtils.getBgmVolumn();
        } else if (optionKey == PropertyFileConstants.SE_VOLUMN) {
            return GameConfigUtils.getSeVolumn();
        } else {
            print("Error: Invalid option key [\(optionKey)]");
            return 0;
        }
    }
    
    private func updateOptionValue() {
        if (optionKey == PropertyFileConstants.CROSSHAIR_SENSITIVITY) {
            GameConfigUtils.setCrosshairSensitivity(currentNum);
            GameContext.updateSensitivity();
            return;
        } else if (optionKey == PropertyFileConstants.BGM_VOLUMN) {
            GameConfigUtils.setBgmVolumn(currentNum);
            AudioUtils.updateBgmVolumn();
            return;
        } else if (optionKey == PropertyFileConstants.SE_VOLUMN) {
            GameConfigUtils.setSeVolumn(currentNum);
            AudioUtils.updateSeVolumn();
            return;
        } else {
            print("Error: Invalid option key [\(optionKey)]");
            return;
        }
    }
    
    private func getValueString() -> String {
        var output:String = "";
        
        for _ in 0..<currentNum {
            output += filledSign;
        }

        for _ in 0..<(maxNum-currentNum) {
            output += emptySign;
        }
        
        return output;
    }
}