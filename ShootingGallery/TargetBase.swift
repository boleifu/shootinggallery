//
//  TargetBase.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/14/16.
//  Copyright © 2016 boleifu. All rights reserved.
//


import SpriteKit
class TargetBase : TargetObject {
    
    // Config, all normalized
    var hitBoxCenter:CGPoint = CGPoint.zero;  // In the middle
    var hitBoxWdith:CGFloat = 0;
    
    let stickWidth:CGFloat = 18/34;
    
    override func generateTexture() -> SKTexture? {
        fatalError("generateTexture() has not been implemented");
    }
    
    func getAfterHitTexture() ->  SKTexture {
        fatalError("getAfterHitTexture() has not been implemented");
    }
    
    override func configProperties(){
        // No config
    }
    
    override func setHitBox() {
        hitBox = SKShapeNode(rectOfSize: CGSize(width: self.size.width * hitBoxWdith, height: self.size.width * hitBoxWdith));
        hitBox?.position = CGPoint(x: 0, y: self.size.height * hitBoxCenter.y);
        //hitBox?.zPosition = LayerDepth.DEBUG_OBJECT.getDepth();
        hitBox?.hidden = true;
        self.addChild(hitBox!);
    }
    
    override func getAction() -> SKAction {
        return SKAction.sequence([]);
    }
    
    func getHitScoreValue() -> Int {
        fatalError("getHitScoreValue() has not been implemented");
    }
    
    override func hit(hitPosOnNode:CGPoint) -> Bool {
        
        GameRuntimeContext.totalShotHit += 1;
        
        if (!self.isHit(hitPosOnNode)) {
            print("Miss Stick!!!")
            return false;
        }
        
        print("Hit Stick!!!");
        self.texture = getAfterHitTexture();
        resize(self.size.width*stickWidth, nil);
        self.removeAllChildren();
        hitBox = nil;
        
        if (GameRuntimeContext.gameMode != GameMode.SPEED_MODE) {
            // Add time
            GameRuntimeContext.gameEndTime += 5;
            
            // Increase the multiplier
            //GameRuntimeContext.currentScoreMultiplier += 3;
        } else {
            GameRuntimeContext.targetRemains -= 1;
        }
        
        // Add bullseye hit
        GameRuntimeContext.bullseyeHit += 1;

        return true;
    }
}