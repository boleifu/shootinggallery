//
//  BulletHole.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/5/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class BulletHole : EnvironmentObject {
    
    let lifeTime:NSTimeInterval = 1;
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "shot_blue_large");
    }
    
    override func configProperties(){
        self.name = "BulletHole";
        self.zPosition = LayerDepth.BULLET_HOLE.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5); // Middle
    }
    
    override func getAction() -> SKAction {
        return SKAction.sequence([SKAction.waitForDuration(lifeTime), SKAction.removeFromParent()]);
    }
    
}