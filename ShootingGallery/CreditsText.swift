//
//  CreditsText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/29/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class CreditsText : SelectableNode {
    
    override func getText() -> String? {
        return NSLocalizedString("CREDITS_TEXT", comment: "CreditsText");
        //return GameTextConstants.CREDITS_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Credits Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Center;
    }
    
    override func select() {
        // Move to the game credit page
        GameRuntimeContext.menuPage = MenuPage.CREDIT_PAGE;
        unHilight();
    }
}