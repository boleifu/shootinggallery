//
//  GameFullNameText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/29/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GameFullNameText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("GAME_FULL_NAME_TEXT", comment: "GameFullNameText");
        //return GameTextConstants.GAME_FULL_NAME_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Game Full Name Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
    }
}