//
//  LayerDepthEnum.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/4/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit
//
// This defines the depth of each object in the game scene
//
enum LayerDepth : Double {
    // Layer Node: 1-9
    //case GAME_STAGE_LAYER = 1;
    //case HUD_LAYER = 2;
    //case CONTENT_LAYER = 3;
    
    // Environment Object
    case BULLET_HOLE = 1;
    
    // Game Content
    case BACKGROUND = 100;
    
    case ROW_SIX_BLOCKER = 150;
    
    case ROW_FIVE_BLOCKER = 200;
    
    case ROW_FOUR_BLOCKER = 300
    
    case ROW_TWO_TARGET_BASE = 350;
    case ROW_TWO_TARGET_ADDON = 21;
    
    case ROW_THREE_BLOKER = 400;
    
    case ROW_TWO_BLOKER = 450;
    
    case ROW_ONE_TARGET_BASE = 500;
    case ROW_ONE_TARGET_ADDON = 20;
    
    case ROW_ONE_BLOCKER = 599;
    
    // Game Stage
    case TABLE = 700;
    case SIDE_CURTAIN = 710;
    case TOP_CURTAIN = 720;
    
    // Menu
    case MENU_BACKGROUND = 800;
    case MENU_BACKGROUND_ADD_ON = 805;
    case MENU_TITLE = 810;
    case MENU_SUB_TITLE = 820;
    case MENU_OBJECT = 850;
    case MENU_IMAGE_ADD_ON = 22;
    case MENU_TEXT_ADD_ON = 23;
    
    // On Screen controller
    case RIFLE = 900;
    case CROSSHAIR = 910;
    
    // HUD
    case CONTROL_BUTTON = 1000;
    case HUD_CONTENT = 1010;
    
    // DEBUG
    case DEBUG_OBJECT = 1999;
    
    // Get the zPosition value of this LayerDepth object
    func getDepth() -> CGFloat {
        return CGFloat(self.rawValue)
    }
}

