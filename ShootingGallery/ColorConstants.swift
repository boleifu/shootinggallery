//
//  ColorConstants.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/19/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class ColorConstants {
    
    static var BLACK_COLOR = UIColor(red:0.12, green:0.12, blue:0.13, alpha:1.0); // #1F1F21
    static var GOLD_COLOR = UIColor(red:1.00, green:0.80, blue:0.00, alpha:1.0);  // #FFCC00
    static var RED_COLOR = UIColor(red:1.00, green:0.07, blue:0.00, alpha:1.0);   // #FF1300
    static var GREEN_COLOR = UIColor(red:0.30, green:0.85, blue:0.39, alpha:1.0); // #4CD964
    static var BLUE_COLOR = UIColor(red:0.00, green:0.48, blue:1.00, alpha:1.0);  //#007AFF
    static var SKYBLUE_COLOR = UIColor(red:0.82, green:0.93, blue:0.99, alpha:1.0); //#D1EEFC
    static var DARKGREY_COLOR = UIColor(red:0.56, green:0.56, blue:0.58, alpha:1.0); //#8E8E93
    static var SILVER_COLOR = UIColor(red:0.89, green:0.87, blue:0.79, alpha:1.0); //#E4DDCA
}