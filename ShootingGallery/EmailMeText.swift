//
//  EmailMeText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/29/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit
import MessageUI

class EmailMeText : SelectableNode,MFMailComposeViewControllerDelegate {
    
    override func getText() -> String? {
        return NSLocalizedString("EMAIL_ME_TEXT", comment: "EmailMeText");
        //return GameTextConstants.EMAIL_ME_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Email Me Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Bottom;
    }
    
    override func select() {
        // TODO: Execute the email pop-up window
        unHilight();
    }
}