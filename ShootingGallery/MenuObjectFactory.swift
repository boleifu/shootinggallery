//
//  MenuObjectFactory.swift
//  Duck Hunt
//
//  Created by Bolei Fu on 8/31/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

/**
 * This is the factory class for generating some common menu entities.
 */
class MenuObjectFactory {

    // Game Menu
    static func createCurtain() -> Curtain {
        return Curtain(position: CGPoint.zero, face:0, targetWidth: GameContext.screenSize.width/2, targetHeight:GameContext.screenSize.height);
    }
    
    // Game welcome page
    static func createTitleText() -> TitleText {
        return TitleText(fontSize:GameContext.titleSize, fontColor:ColorConstants.BLACK_COLOR);
    }
    
    static func createSubTitleText() -> SubTitleText {
        return SubTitleText(fontSize:GameContext.subTitleSize, fontColor:ColorConstants.GOLD_COLOR);
    }
    
    static func createVersionText() -> VersionText {
        return VersionText(fontSize:GameContext.toolTextSize, fontColor:ColorConstants.BLACK_COLOR);
    }
    
    static func createGameStartText() -> StartGameText {
        return StartGameText(fontSize:GameContext.subTitleSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createCreditsText() -> CreditsText {
        return CreditsText(fontSize:GameContext.subTitleSize/2, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    // Game prep page
    static func createGamePrepPageTitle() -> SelectGameModeText {
        return SelectGameModeText(fontSize:GameContext.pageTitileSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createArcadeModeText() -> ArcadeModeText {
        return ArcadeModeText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createSpeedModeText() -> SpeedModeText {
        return SpeedModeText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createOnlineBattleText() -> OnlineBattleText {
        return OnlineBattleText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createPanel() -> Panel {
        return Panel(position: CGPoint.zero, face:0, targetWidth: GameContext.panelSize.width, targetHeight:GameContext.panelSize.height);
    }
    
    static func createDuckTargetSign() -> DuckTargetSign {
        return DuckTargetSign(position: CGPoint.zero, face:0, targetWidth: nil, targetHeight:GameContext.duckTargetSignHeight);
    }
    
    static func createCommingSoonText() -> CommingSoonText {
        return CommingSoonText(fontSize:GameContext.commingSoonTextSize, fontColor:ColorConstants.GOLD_COLOR);
    }
    
    static func createGameModeDescText(text:String) -> GameModeDescText {
        return GameModeDescText(fontSize:GameContext.gameModeDescTextSize, fontColor:ColorConstants.GOLD_COLOR, text:text);
    }
    
    // Game over page
    static func createGameSummaryText() -> GameSummaryText {
        return GameSummaryText(fontSize:GameContext.pageTitileSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createScoreText() -> ScoreText {
        return ScoreText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createGameTimeText() -> GameTimeText {
        return GameTimeText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createMaxMultiplierText() -> MaxMultiplierText {
        return MaxMultiplierText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createAccuracyText() -> AccuracyText {
        return AccuracyText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createBullseyeText() -> BullseyeText {
        return BullseyeText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createScoreValueText() -> ScoreValueText {
        return ScoreValueText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.GOLD_COLOR);
    }
    
    static func createMaxMultiplierValueText() -> MaxMultiplierValueText {
        return MaxMultiplierValueText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.GOLD_COLOR);
    }
    
    static func createGameTimeValueText() -> GameTimeValueText {
        return GameTimeValueText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.GOLD_COLOR);
    }
    
    static func createAccuracyValueText() -> AccuracyValueText {
        return AccuracyValueText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.GOLD_COLOR);
    }
    
    static func createBullseyeValueText() -> BullseyeValueText {
        return BullseyeValueText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.GOLD_COLOR);
    }
    
    // Game setting page
    
    static func createGameSettingText() -> GameSettingText {
        return GameSettingText(fontSize:GameContext.pageTitileSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createSensitivityText() -> SensitivityText {
        return SensitivityText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createBgmText() -> BgmText {
        return BgmText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createSeText() -> SeText {
        return SeText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createArcadeButtonPosText() -> ArcadeButtonPosText {
        return ArcadeButtonPosText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createSensitivityValueText() -> SettingValueText {
        return SettingValueText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR, optionKey: PropertyFileConstants.CROSSHAIR_SENSITIVITY, minNum: GameContext.gameSettingMinValue, maxNum: GameContext.gameSettingMaxValue);
    }
    
    static func createBgmVolumnValueText() -> SettingValueText {
        return SettingValueText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR, optionKey: PropertyFileConstants.BGM_VOLUMN, minNum: GameContext.gameSettingMinValue, maxNum: GameContext.gameSettingMaxValue);
    }
    
    static func createSeVolumnValueText() -> SettingValueText {
        return SettingValueText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR, optionKey: PropertyFileConstants.SE_VOLUMN, minNum: GameContext.gameSettingMinValue, maxNum: GameContext.gameSettingMaxValue);
    }
    
    static func createButtonPosValueText() -> ButtonPosValueText {
        return ButtonPosValueText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    // Game credits page
    
    static func createGameFullNameText() -> GameFullNameText {
        return GameFullNameText(fontSize:GameContext.pageTitileSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createDevelopedByText() -> DevelopedByText {
        return DevelopedByText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createAutherNameText() -> AutherNameText {
        return AutherNameText(fontSize:GameContext.menuOptionSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createAutherEmailText() -> AutherEmailText {
        return AutherEmailText(fontSize:GameContext.gameModeDescTextSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createLicenseText() -> LicenseText {
        return LicenseText(fontSize:GameContext.gameReportSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createLicenseDetailsText(texts:[String]) -> LicenseDetailsText {
        return LicenseDetailsText(fontSize:GameContext.licenseDetailTextSize, fontColor: ColorConstants.BLACK_COLOR, texts: texts);
    }
    
    // Common text
    static func createSettingText() -> SettingText {
        return SettingText(fontSize:GameContext.actionTextSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createPlayAgainText() -> PlayAgainText {
        return PlayAgainText(fontSize:GameContext.actionTextSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createBackText(destPage:MenuPage) -> BackText {
        return BackText(fontSize:GameContext.actionTextSize, fontColor: ColorConstants.BLACK_COLOR, destPage: destPage);
    }
    
    static func createApplyText() -> ApplyText {
        return ApplyText(fontSize:GameContext.actionTextSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
    static func createEmailMeText() -> EmailMeText {
        return EmailMeText(fontSize:GameContext.actionTextSize, fontColor: ColorConstants.BLACK_COLOR);
    }
    
}