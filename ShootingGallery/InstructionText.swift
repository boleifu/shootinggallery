//
//  InstructionText.swift
//  Duck Hunt
//
//  Created by Bolei Fu on 8/31/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class InstructionText : SKLabelNode {
    
    // Constructor
    init(fontSize:CGFloat, fontColor:UIColor) {
        super.init();
        
        self.text = GameTextConstants.UNKNON_TEXT;
        self.fontName = getFontName();
        self.fontSize = fontSize;
        self.fontColor = fontColor;
        self.zPosition = LayerDepth.DEBUG_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
        
        // Run the pre-set action
        self.runAction(getAction(), completion: {
            self.afterAction();
        });
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented");
    }
    
    deinit {
        //print("Game Entity \(self.name) deinit.");
    }
    
    private func getFontName() -> String {
        return GameTextConstants.SUB_TITLE_FONT_NAME;
    }
    
    private func getAction() -> SKAction {
        let instructionText1 = NSLocalizedString("TUTORIAL_TEXT_1", comment: "instructionText1");
        let instructionText2 = NSLocalizedString("TUTORIAL_TEXT_2", comment: "instructionText2");
        
        let showInstruction1 = SKAction.customActionWithDuration(0, actionBlock: {
            (node:SKNode!, elapsed:CGFloat) -> Void in
            self.text = instructionText1;
        });
        
        let showInstruction2 = SKAction.customActionWithDuration(0, actionBlock: {
            (node:SKNode!, elapsed:CGFloat) -> Void in
            self.text = instructionText2;
        });
        
        
        return SKAction.sequence([showInstruction1, SKAction.waitForDuration(3), showInstruction2, SKAction.waitForDuration(2)]);
    }
    
    private func afterAction() {
        self.removeFromParent();
    }
}