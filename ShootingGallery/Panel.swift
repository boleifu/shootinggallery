//
//  Panel.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/25/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class Panel : GameEntity {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "wood_board_tall");
    }
    
    override func configProperties(){
        self.name = "Panel";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.anchorPoint = CGPoint(x: 0, y: 0.5); // Left-middle
        //self.alpha = 0.8;
    }
}