//
//  WelcomePageMenager.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/20/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class WelcomePageManager : MenuPageManager {
    
    override func setupPage() {
        self.pageType = MenuPage.WELCOME_PAGE;
        
        // Add Title
        let titleText = MenuObjectFactory.createTitleText();
        titleText.position = GameContext.titlePos;
        menuTextNodes.append(titleText);
        pageLayer.addChild(titleText);
        
        // Add subTitle
        let subTitleText = MenuObjectFactory.createSubTitleText();
        subTitleText.position = GameContext.subTitlePos;
        subTitleText.zRotation = CGFloat(M_PI_4/4);
        menuTextNodes.append(subTitleText);
        pageLayer.addChild(subTitleText);
        
        // Add version
        let version = MenuObjectFactory.createVersionText();
        version.position = GameContext.versionTextPos;
        menuTextNodes.append(version);
        pageLayer.addChild(version);
        
        // Add start text
        let gameStartText = MenuObjectFactory.createGameStartText();
        gameStartText.position = GameContext.startTextPos;
        selectableNodes.append(gameStartText);
        pageLayer.addChild(gameStartText);
        
        // Add credits text
        let gameCreditsText = MenuObjectFactory.createCreditsText();
        gameCreditsText.position = GameContext.actionOneTextPos;
        selectableNodes.append(gameCreditsText);
        pageLayer.addChild(gameCreditsText);
        
    }
}