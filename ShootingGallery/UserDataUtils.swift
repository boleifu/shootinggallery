//
//  ConfigurationUtils.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import Foundation
import UIKit

class UserDataUtils {
    
    private static var userGameData:NSMutableDictionary? = nil;

    // Game configuration
    // Get the config file path from the app bundle
    private static func getConfigFileBundlePath() -> String? {
        let bundlePath = NSBundle.mainBundle().pathForResource(PropertyFileConstants.USER_DATA_FILE_NAME, ofType:PropertyFileConstants.USER_DATA_FILE_TYPE);
        print("Config file bundle path = \(bundlePath)");
        return bundlePath;
    }
    
    // Get the config file path from the user domain
    private static func getConfigFilePath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as NSArray
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let path = documentsDirectory.stringByAppendingPathComponent(PropertyFileConstants.USER_DATA_FILE_FULL_NAME);
        print("Config file path = \(path)");
        return path;
    }
    
    // Read the config file and get the config details.
    // Plist files in the project bundle are read only.
    // You need to save a copy of the plist in the Documents folder than edit and read that one.
    private static func getUserGameData(forceRefresh forceRefresh:Bool=false) -> NSMutableDictionary? {
        if (UserDataUtils.userGameData == nil || forceRefresh) {
            let path = UserDataUtils.getConfigFilePath();
            let fileManager = NSFileManager.defaultManager();
            if (!fileManager.fileExistsAtPath(path)) {
                // Config file not in the user domain, copy from the bundle
                let bundlePath:String? = UserDataUtils.getConfigFileBundlePath();
                if (bundlePath == nil) {
                    print("Error: Cannot get game configurations from bundle.");
                    return nil;
                }
                print("Config file not in the user domain, copy from the bundle path = \(bundlePath!).")
                do {
                    try fileManager.copyItemAtPath(bundlePath!, toPath: path);
                } catch {
                    print("Copying the config file from bundle to user domain failed. Error:\(error)");
                    return nil;
                }
            }
            
            let existingUserData = NSDictionary(contentsOfFile: path);
            UserDataUtils.userGameData = NSMutableDictionary(dictionary: existingUserData!);
        }
        //print("Current user data: \(UserDataUtils.userGameData)")
        return UserDataUtils.userGameData;
    }
    
    // Persist the config file with the current configuration
    // Plist files in the project bundle are read only.
    // You need to save a copy of the plist in the Documents folder than edit and read that one.
    static func persistUserData() {
        if (UserDataUtils.userGameData == nil) {
            print("Error: Unable to persist the user data since the data is not loaded yet.")
            return;
        }
        
        let path:String? = UserDataUtils.getConfigFilePath();
        if (path == nil) {
            print("Error: Cannot get config file path.");
            return;
        }
        
        let succeed:Bool = UserDataUtils.userGameData!.writeToFile(path!, atomically:false);
        
        //print("UserGameData file updated. Path = \(path!). Succeed = \(succeed)");
        print("UserGameData file persisted. Succeed = \(succeed)");
    }
    
    
    // Get data
    
    // Arcade Mode
    
    static func getArcadeModeHighestScore() -> Int {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            let highestScore:Int? = userGameData![PropertyFileConstants.ARCADE_MODE_HIGHEST_SCORE] as? Int;
            if (highestScore != nil) {
                return highestScore!;
            }
        }
        print("Error: Cannot get ArcadeModeHighestScore.");
        return 0;
    }
    
    static func setArcadeModeHighestScore(highestScore:Int) {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            userGameData!.setObject(highestScore, forKey: PropertyFileConstants.ARCADE_MODE_HIGHEST_SCORE);
        } else {
            print("Error: Cannot set ArcadeModeHighestScore.");
        }
    }
    
    static func getArcadeModeLongestGameTime() -> Int {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            let longestGameTime:Int? = userGameData![PropertyFileConstants.ARCADE_MODE_LONGEST_GAME_TIME] as? Int;
            if (longestGameTime != nil) {
                return longestGameTime!;
            }
        }
        print("Error: Cannot get ArcadeModeLongestGameTime.");
        return 0;
    }
    
    static func setArcadeModeLongestGameTime(longestGameTime:Int) {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            userGameData!.setObject(longestGameTime, forKey: PropertyFileConstants.ARCADE_MODE_LONGEST_GAME_TIME);
        } else {
            print("Error: Cannot set ArcadeModeLongestGameTime.");
        }
    }
    
    static func getArcadeModeMaxMultiplier() -> Int {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            let longestGameTime:Int? = userGameData![PropertyFileConstants.ARCADE_MODE_MAX_MULTIPLIER] as? Int;
            if (longestGameTime != nil) {
                return longestGameTime!;
            }
        }
        print("Error: Cannot get ArcadeModeMaxMultiplier.");
        return 0;
    }
    
    static func setArcadeModeMaxMultiplier(maxMultiplier:Int) {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            userGameData!.setObject(maxMultiplier, forKey: PropertyFileConstants.ARCADE_MODE_MAX_MULTIPLIER);
        } else {
            print("Error: Cannot set ArcadeModeMaxMultiplier.");
        }
    }
    
    // Speed Mode
    
    static func getSpeedModeHighestScore() -> Int {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            let highestScore:Int? = userGameData![PropertyFileConstants.SPEED_MODE_HIGHEST_SCORE] as? Int;
            if (highestScore != nil) {
                return highestScore!;
            }
        }
        print("Error: Cannot get SpeedModeHighestScore.");
        return 0;
    }
    
    static func setSpeedModeHighestScore(highestScore:Int) {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            userGameData!.setObject(highestScore, forKey: PropertyFileConstants.SPEED_MODE_HIGHEST_SCORE);
        } else {
            print("Error: Cannot set SpeedModeHighestScore.");
        }
    }
    
    static func getSpeedModeLongestGameTime() -> Int {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            let longestGameTime:Int? = userGameData![PropertyFileConstants.SPEED_MODE_LONGEST_GAME_TIME] as? Int;
            if (longestGameTime != nil) {
                return longestGameTime!;
            }
        }
        print("Error: Cannot get SpeedModeLongestGameTime.");
        return 0;
    }
    
    static func setSpeedModeLongestGameTime(longestGameTime:Int) {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            userGameData!.setObject(longestGameTime, forKey: PropertyFileConstants.SPEED_MODE_LONGEST_GAME_TIME);
        } else {
            print("Error: Cannot set SpeedModeLongestGameTime.");
        }
    }
    
    static func getSpeedModeMaxMultiplier() -> Int {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            let longestGameTime:Int? = userGameData![PropertyFileConstants.SPEED_MODE_MAX_MULTIPLIER] as? Int;
            if (longestGameTime != nil) {
                return longestGameTime!;
            }
        }
        print("Error: Cannot get SpeedModeMaxMultiplier.");
        return 0;
    }
    
    static func setSpeedModeMaxMultiplier(maxMultiplier:Int) {
        let userGameData: NSMutableDictionary? = UserDataUtils.getUserGameData();
        if (userGameData != nil) {
            userGameData!.setObject(maxMultiplier, forKey: PropertyFileConstants.SPEED_MODE_MAX_MULTIPLIER);
        } else {
            print("Error: Cannot set SpeedModeMaxMultiplier.");
        }
    }
}