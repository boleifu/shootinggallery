//
//  BackBoard.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/5/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class BackBoard : GameEntity, Damagable {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "uncolored_forest");
    }
    
    override func configProperties(){
        self.name = "BackBoard";
        self.zPosition = LayerDepth.BACKGROUND.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5); // Middle
    }
    
    func isHit(hitPosOnTarget: CGPoint) -> Bool {
        return false
    }
    
    func hit(hitPosOnNode: CGPoint) -> Bool {
        return false;
    }
}
