//
//  TargetBody.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/14/16.
//  Copyright © 2016 boleifu. All rights reserved.
//


import SpriteKit
class TargetBody : TargetObject {
    
    // Config, all normalized
    var hitBoxX:CGFloat = 0;
    var hitBoxY:CGFloat = 0;
    var hitBoxRadius:CGFloat = 0;
    
    override func generateTexture() -> SKTexture? {
        fatalError("generateTexture() has not been implemented");
    }
    
    override func configProperties(){
        // No config
    }
    
    override func setHitBox() {
        hitBox = SKShapeNode(circleOfRadius: self.size.width * hitBoxRadius);
        hitBox?.position = CGPoint(x: self.size.height * hitBoxX, y: self.size.height * hitBoxY);
        //hitBox?.zPosition = LayerDepth.DEBUG_OBJECT.getDepth();
        hitBox?.hidden = true;
        self.addChild(hitBox!);
    }
    
    override func getAction() -> SKAction {
        return SKAction.sequence([]);
    }
    
    func getHitScoreValue() -> Int {
       fatalError("getHitScoreValue() has not been implemented");
    }
    
    func getMissScoreValue() -> Int {
        fatalError("getMissScoreValue() has not been implemented");
    }
    
    override func hit(hitPosOnNode:CGPoint) -> Bool {
        
        GameRuntimeContext.totalShotHit += 1;
        
        if (!self.isHit(hitPosOnNode)) {
            print("Miss TargetBody!!!")
            // Add score if not speed mode
            if (GameRuntimeContext.gameMode != GameMode.SPEED_MODE) {
                GameRuntimeContext.addScore(getMissScoreValue() * GameRuntimeContext.currentScoreMultiplier);
            }
            return false;
        }
        
        print("Hit TagetBody!!!");
        //self.texture = SKTexture(imageNamed: "duck_outline_back");
        
        if (GameRuntimeContext.gameMode == GameMode.ARCADE_MODE) {
            self.runAction(GameConstants.ARCADE_DUCK_HIT);
        } else if (GameRuntimeContext.gameMode == GameMode.SPEED_MODE) {
            self.runAction(GameConstants.SPEED_DUCK_HIT);
        } else if (GameRuntimeContext.gameMode == GameMode.MENU) {
            self.runAction(GameConstants.ARCADE_DUCK_HIT);
        } else {
            print("ERROR: Invalide game mode \(GameRuntimeContext.gameMode). Failed to process the bullseye hit.")
        }
        
        hitBox = nil;
        
        // Add score
        GameRuntimeContext.addScore(getHitScoreValue() * GameRuntimeContext.currentScoreMultiplier);
        
        // Add bullseye hit
        GameRuntimeContext.bullseyeHit += 1;
        
        if (GameRuntimeContext.gameMode == GameMode.SPEED_MODE) {
            GameRuntimeContext.targetRemains -= 1;
        } else {
            // Increase the multiplier
            GameRuntimeContext.currentScoreMultiplier += 1;
            
            // Update max multiplier
            GameRuntimeContext.maxScoreMultiplier = max(GameRuntimeContext.currentScoreMultiplier, GameRuntimeContext.maxScoreMultiplier);
        }
        
        return true;
    }
}
