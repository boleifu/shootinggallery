//
//  TitleText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/18/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class TitleText : MenuText {
 
    override func getText() -> String? {
        return NSLocalizedString("GAME_TITLE", comment: "Game Title");
        //GameTextConstants.GAME_TITLE;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Title Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
    }
}