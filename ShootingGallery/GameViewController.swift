//
//  GameViewController.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/4/16.
//  Copyright (c) 2016 boleifu. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    
    var gameView:SKView? = nil;
    var gameScene:GameScene? = nil;
    
    // In game menu
    @IBOutlet weak var inGameMenuView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var resumeButton: UIButton!
    @IBOutlet weak var restartButton: UIButton!
    @IBOutlet weak var quitButton: UIButton!
    
    var menuHidden:Bool = true;
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self);
        print("GameViewController is deinited.");
    }
    
    // ======== Lifecycle control ========

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Pause game if becoming inactive
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GameViewController.willResignActive(_:)), name: UIApplicationWillResignActiveNotification, object: nil);
        
        // Resume game if active again
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GameViewController.becomeActive(_:)), name: UIApplicationDidBecomeActiveNotification, object: nil);
        
        
        // Configure the view.
        gameView = self.view as? SKView;
        //gameView!.showsFPS = true;
        //gameView!.showsNodeCount = true;
        // Note: Need to disable this due to a xcode 7.2 bug.
        //       Otherwise, the program will ended with "Message from debugger: Terminated due to memory issue"
        gameView!.showsFields = false;
        //skView!.frameInterval = 5;
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        gameView!.ignoresSiblingOrder = true
    
        // Config the scene
        gameScene = GameScene(size: gameView!.bounds.size, menuButton:menuButton);
        
        /* Set the scale mode to scale to fit the window */
        gameScene!.scaleMode = .AspectFill
        
        // Setup on screen menu
        menuButton.alpha = 0.8;
        
        // Resume button
        let resetText:String = NSLocalizedString("RESUME_TEXT", comment: "Resume Button");
        resumeButton.setTitle(resetText, forState: .Normal);
        resumeButton.setTitle(resetText, forState: .Highlighted);
        resumeButton.setTitle(resetText, forState: .Selected);
        resumeButton.setTitle(resetText, forState: .Disabled);
        resumeButton.titleLabel?.font = UIFont(name: GameTextConstants.FONT_NAME, size: 30);
        resumeButton.setBackgroundImage(UIImage(named: "yellow_button04"), forState: UIControlState.Normal);
        resumeButton.setBackgroundImage(UIImage(named: "yellow_button05"), forState: UIControlState.Highlighted);
        
        // Restart button
        let restartText:String = NSLocalizedString("RESTART_TEXT", comment: "Restart Button");
        restartButton.setTitle(restartText, forState: .Normal);
        restartButton.setTitle(restartText, forState: .Highlighted);
        restartButton.setTitle(restartText, forState: .Selected);
        restartButton.setTitle(restartText, forState: .Disabled);
        restartButton.titleLabel?.font = UIFont(name: GameTextConstants.FONT_NAME, size: 30);
        restartButton.setBackgroundImage(UIImage(named: "red_button01"), forState: UIControlState.Normal);
        restartButton.setBackgroundImage(UIImage(named: "red_button02"), forState: UIControlState.Highlighted);
        
        // Quit button
        let quitText:String = NSLocalizedString("QUIT_TEXT", comment: "Quit Button");
        quitButton.setTitle(quitText, forState: .Normal);
        quitButton.setTitle(quitText, forState: .Highlighted);
        quitButton.setTitle(quitText, forState: .Selected);
        quitButton.setTitle(quitText, forState: .Disabled);
        quitButton.titleLabel?.font = UIFont(name: GameTextConstants.FONT_NAME, size: 30);
        quitButton.setBackgroundImage(UIImage(named: "blue_button04"), forState: UIControlState.Normal);
        quitButton.setBackgroundImage(UIImage(named: "blue_button05"), forState: UIControlState.Highlighted);
        
        hideMenu();

        
        // Present the scene
        gameView!.presentScene(gameScene!)
    }
    
    @objc private func willResignActive(notification: NSNotification) {
        print("GameViewController will resign active...");
        gameView!.paused = true;
        gameScene!.paused = true;
        gameScene!.touchDisabled = true;
        // Stop the game timer
        GameRuntimeContext.lastUpdatedRealTime = 0;
    }
    
    @objc private func becomeActive(notification: NSNotification) {
        print("GameViewController becomes active...");
        gameView!.paused = false;
        gameScene!.paused = false;
        gameScene!.touchDisabled = false;
    }
    
    // ======== Configuration ========

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // ======= In game menu =======
    
    @IBAction func clickedMenuButton(sender: UIButton) {
        AudioUtils.playButtonClickSound();
        if (menuHidden) {
            showMenu();
        } else {
            hideMenu();
        }
    }
    
    @IBAction func clickedResumeButton(sender: UIButton) {
        AudioUtils.playButtonClickSound();
        hideMenu();
        print("Resume button clicked");
    }
    
    @IBAction func clickedRestartButton(sender: UIButton) {
        AudioUtils.playButtonClickSound();
        
        // Shut down the game
        if (!GameRuntimeContext.powerOnInProgress) {
            GameRuntimeContext.gamePoweredOn = false;
            GameRuntimeContext.gameStarted = false;
            AudioUtils.stopBgmThemeOne();
        } else {
            print("Cannot restart the game since it is still powering on.");
        }

        
        hideMenu();
        print("Restart button clicked");
    }
    
    @IBAction func clickedQuitButton(sender: UIButton) {
        AudioUtils.playButtonClickSound();
        
        if (!GameRuntimeContext.powerOnInProgress) {
            // Shut down the game
            GameRuntimeContext.gamePoweredOn = false;
            GameRuntimeContext.gameStarted = false;
            AudioUtils.stopBgmThemeOne();
            
            // Back to the welcom page
            GameRuntimeContext.gameMode = GameMode.MENU;
            GameRuntimeContext.menuPage = MenuPage.WELCOME_PAGE;
            GameRuntimeContext.menuStatus = MenuLifyCycle.LEAVING_GAME;
        } else {
            print("Cannot quit the game since it is still powering on.");
        }
        
        hideMenu();
        print("Quit button clicked");
    }
    
    private func hideMenu() {
        // Disable menu view
        inGameMenuView.userInteractionEnabled = false;
        inGameMenuView.hidden = true;
        gameView!.paused = false;
        if (gameScene != nil) {
            gameScene!.touchDisabled = false;
            gameScene!.paused = false;
        }
        menuHidden = true;
    }
    
    private func showMenu() {
        // Disable menu view
        inGameMenuView.userInteractionEnabled = true;
        inGameMenuView.hidden = false;
        gameView!.paused = true;
        gameScene!.paused = true;
        gameScene!.touchDisabled = true;
        menuHidden = false;
        // Stop the game timer
        GameRuntimeContext.lastUpdatedRealTime = 0;
    }
}
