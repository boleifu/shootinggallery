//
//  GameScene.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/4/16.
//  Copyright (c) 2016 boleifu. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    weak var inGameMenuButton:UIButton? = nil;
    
    // Screen layers
    let gameStageLayer = SKNode();
    let menuLayer = SKNode();
    let hudLayer = SKNode();
    let gameContentLayer = SKNode();
    let playerLayer = SKNode();
    
    // Game controller
    var gameLifeCycleManager:GameLifeCycleManager? = nil;
    var menuManager: MenuManager? = nil;
    
    // Game scene property
    let screenSize:CGSize;
    var gameContentSize:CGSize = CGSize.zero;
    
    // Game scene object
    var fireButton:ArcadeButton? = nil;
    var crosshair:CrossHair? = nil;
    var rifle:Rifle? = nil;
    
    // Runtime properties
    var touchDisabled:Bool = false;
    weak var fireButtonTouch:UITouch? = nil;
    weak var aimingTouch:UITouch? = nil;
    var aimingDeltaPoint = CGVector.zero;
    var lastProcessedMenuPage:MenuPage = MenuPage.UNKNOWN;
    
    // ======== Constructure ========
    init(size: CGSize, menuButton: UIButton) {
        inGameMenuButton = menuButton;
        screenSize = size;
        super.init(size: size);
        
        // Add game layer
        self.addChild(hudLayer);
        self.addChild(gameStageLayer);
        self.addChild(gameContentLayer);
        self.addChild(playerLayer);
        self.addChild(menuLayer);
        
        // Init game config
        gameContentSize = CGSize(width: screenSize.width, height: screenSize.height/5*4);
        menuLayer.position = CGPoint.zero;
        gameContentLayer.position = CGPoint(x:0, y:screenSize.height - gameContentSize.height);
        playerLayer.position = gameContentLayer.position;
        GameContext.initContext(screenSize, gameContentSize);    // Init game context
        gameLifeCycleManager = GameLifeCycleManager(hudLayer:hudLayer, gameContentLayer:gameContentLayer);
        menuManager = MenuManager(menuLayer:menuLayer);
        
        // Init the game
        // Setup menu
        menuManager?.initMenu();
        
        // Setup stage
        setupStage();
        
        // Setup game on-screen controller
        setupController();
        
        // Setup player
        setupPlayer();
        
        // Setup game
        gameLifeCycleManager!.setupGame();
        
        // Setup audio players
        AudioUtils.setupAllAudioPlayers();
        
        // Setup tutorial
        setupTutorial();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.removeAllActions();
        self.removeAllChildren();
        print("GameScene released");
    }
    
    // ======== Lifecycle control ========
    
    override func didMoveToView(view: SKView) {
        // Do nothing
    }

    override func update(currentTime: CFTimeInterval) {
        
        // Update the game timer
        // We need a separate timer since the input currentTime is not continues
        // if there is a pause or app goes to background
        if (GameRuntimeContext.lastUpdatedRealTime == 0) {
            // Skip the first frame
            GameRuntimeContext.lastUpdatedRealTime = currentTime;
            return;
        } else {
            // Update the game time
            let deltaTime = currentTime - GameRuntimeContext.lastUpdatedRealTime;
            GameRuntimeContext.currentGameTime += deltaTime;
            GameRuntimeContext.lastUpdatedRealTime = currentTime;
        }
        
        //print("Real time = \(currentTime), Game time = \(GameRuntimeContext.currentGameTime).");
        
        // Update button pos
        fireButton?.position = GameContext.arcadeButtonPos;
        
        // Update aiming crosshair
        updatePlayer();
        
        // Process the game based on the game mode
        if (GameRuntimeContext.gameMode == GameMode.MENU) {
            inGameMenuButton?.hidden = true;
            processMenuMode(GameRuntimeContext.currentGameTime);
        } else if (GameRuntimeContext.gameMode == GameMode.ARCADE_MODE) {
            inGameMenuButton?.hidden = false;
            processArcadeMode(GameRuntimeContext.currentGameTime);
        } else if (GameRuntimeContext.gameMode == GameMode.SPEED_MODE) {
            inGameMenuButton?.hidden = false;
            processSpeedMode(GameRuntimeContext.currentGameTime);
        }
        
        // Only process once
        aimingDeltaPoint = CGVector.zero;
    }
    
    private func processMenuMode(currentGameTime: CFTimeInterval) {        
        if (!hudLayer.hidden) {
            // First time move to the menu from the game
            hudLayer.hidden = true;
            //gameLifeCycleManager?.clearGameContents();
        }
        
        
        if (GameRuntimeContext.menuStatus == MenuLifyCycle.DISABLED) {
            return;
        } else if (GameRuntimeContext.menuStatus == MenuLifyCycle.ENABLED) {
            // Update aiming crosshair
            let aimingPosMoved = aimingDeltaPoint != CGVector.zero;
            var selected:Bool = false;
            if (fireButton!.isButtonPressed()) {
                selected = rifle!.fire(currentGameTime, commandId: fireButton!.getCommandId());
            }
            
            if (aimingPosMoved || selected || GameRuntimeContext.menuPage != lastProcessedMenuPage) {
                lastProcessedMenuPage = GameRuntimeContext.menuPage;
                let crossHairPosOnMenu = playerLayer.convertPoint(crosshair!.position, toNode: menuLayer);
                menuManager?.updateMenu(crossHairPosOnMenu, selected: selected);
            }
        } else if (GameRuntimeContext.menuStatus == MenuLifyCycle.ENTERING_GAME) {
            menuManager?.disableMenu();
        } else if (GameRuntimeContext.menuStatus == MenuLifyCycle.LEAVING_GAME) {
            menuManager?.enableMenu();
        }
    }
    
    private func processArcadeMode(currentGameTime: CFTimeInterval) {
        // Power on the game if needed
        if (!GameRuntimeContext.gamePoweredOn) {
            if (!GameRuntimeContext.powerOnInProgress) {
                gameLifeCycleManager!.resetGame(gameTime: GameContext.arcadeModeGameTime, ammoNum: GameContext.arcadeModeAmmoNum);
                gameLifeCycleManager!.fillGameContents();
                GameRuntimeContext.powerOnInProgress = true;
            }
        } else {
            // First update after game powered on
            if (GameRuntimeContext.powerOnInProgress) {
                GameRuntimeContext.powerOnInProgress = false;
                // Start the game
                GameRuntimeContext.setGameStarted(gameStartTime: currentGameTime);
                AudioUtils.playBgmThemeOne();
            }
        }
        
        // Process the game
        
        // Refresh spawner
        gameLifeCycleManager!.spawnArcadeGameContents(currentGameTime);
        
        // Only continue if game is marked as started
        if (!GameRuntimeContext.gameStarted) {
            return;
        }
        
        // Fire rifle if needed
        if (fireButton!.isButtonPressed()) {
            if (rifle!.fire(currentGameTime, commandId: fireButton!.getCommandId())) {
                // Process rifle fire
                gameLifeCycleManager!.processRifleFire(currentGameTime, aimedPosOnGameContent: crosshair!.position);
            }
        }
        
        // Refresh HUD
        let crossHairPosOnHud = gameContentLayer.convertPoint(crosshair!.position, toNode: hudLayer);
        gameLifeCycleManager!.refreshHud(currentGameTime, crossHairPosOnHud: crossHairPosOnHud);
        
        // Check timeup
        gameLifeCycleManager!.checkTimeup(currentGameTime);
    }
    
    private func processSpeedMode(currentGameTime: CFTimeInterval) {
        // Power on the game if needed
        if (!GameRuntimeContext.gamePoweredOn) {
            if (!GameRuntimeContext.powerOnInProgress) {
                gameLifeCycleManager!.resetGame(gameTime: GameContext.speedModeGameTime, ammoNum: GameContext.speedModeAmmoNum);
                gameLifeCycleManager!.fillGameContents();
                GameRuntimeContext.powerOnInProgress = true;
            }
        } else {
            // First update after game powered on
            if (GameRuntimeContext.powerOnInProgress) {
                GameRuntimeContext.powerOnInProgress = false;
                // Start the game
                GameRuntimeContext.setGameStarted(gameStartTime: currentGameTime);
                AudioUtils.playBgmThemeOne();
            }
        }
        
        // Process the game
        
        // Refresh spawner
        gameLifeCycleManager!.spawnSpeedGameContents(currentGameTime);
        
        // Only continue if game is marked as started
        if (!GameRuntimeContext.gameStarted) {
            return;
        }
        
        // Fire rifle if needed
        if (fireButton!.isButtonPressed()) {
            if (rifle!.fire(currentGameTime, commandId: fireButton!.getCommandId())) {
                // Process rifle fire
                gameLifeCycleManager!.processRifleFire(currentGameTime, aimedPosOnGameContent: crosshair!.position);
            }
        }
        
        // Refresh HUD
        let crossHairPosOnHud = gameContentLayer.convertPoint(crosshair!.position, toNode: hudLayer);
        gameLifeCycleManager!.refreshHud(currentGameTime, crossHairPosOnHud: crossHairPosOnHud);
        
        // Check timeup
        gameLifeCycleManager!.checkTimeup(currentGameTime);

    }
    
    // ======== Touch control ========
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if (touchDisabled) {
            print("Touch disabled...");
            return;
        }
        
        for touch in touches {
            // Check if the button is clicked
            if (fireButton!.isInButton(touch.locationInNode(fireButton!))) {
                if (fireButtonTouch == nil) {
                    fireButtonTouch = touch;
                    fireButton?.pressButton();
                }
                continue;
            }
            
            // Not click the button, try to move the crosshair
            if (aimingTouch == nil) {
                aimingTouch = touch;
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if (touchDisabled) {
            print("Touch disabled...");
            return;
        }
        
        for touch in touches {
            // Only process the move of aiming touch
            if (touch == aimingTouch) {
                let currentPoint:CGPoint! = touch.locationInNode(self);
                let prevPoint:CGPoint! = touch.previousLocationInNode(self);
                aimingDeltaPoint = CGVector(dx:(currentPoint.x-prevPoint.x), dy:(currentPoint.y-prevPoint.y));
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            if (touch == fireButtonTouch) {
                fireButton?.releaseButton();
                fireButtonTouch = nil;
            } else if (touch == aimingTouch) {
                aimingTouch = nil;
                aimingDeltaPoint = CGVector.zero;
            }
        }
    }
    
    // ======== Helper functions ========
    
    private func setupStage() {
        // Table
        let table = Table(position: CGPoint(x: 0, y: screenSize.height - gameContentSize.height), face:0, targetWidth: screenSize.width, targetHeight:nil);
        gameStageLayer.addChild(table);
    }
    
    private func setupController() {
        // Add fire button
        fireButton = ArcadeButton(position: GameContext.arcadeButtonPos, width: GameContext.arcadeButtonWidth, buttonKey: GameConstants.FIRE_BUTTON_KEY);
        gameStageLayer.addChild(fireButton!)
    }
        
    private func setupPlayer() {
        // Setup crosshair
        crosshair = CrossHair(position: CGPoint(x: gameContentSize.width/2, y: gameContentSize.height/2), face:0, targetWidth: GameContext.crossHairWidth, targetHeight:nil);
        playerLayer.addChild(crosshair!);
        
        // Setup rifle
        rifle = Rifle(position:CGPoint.zero, face:0, targetWidth: nil, targetHeight:screenSize.height/2);
        rifle?.update(CGPoint(x: gameContentSize.width/2, y: gameContentSize.height/2));
        gameStageLayer.addChild(rifle!);
    }
    
    private func setupTutorial() {
        // Add control tutorial
        let pointer = EntityFactory.createInstaructionPointer();
        pointer.position = CGPoint(x: GameContext.screenSize.width/3, y: GameContext.screenSize.height/2);
        gameStageLayer.addChild(pointer);
        
        // Add tutorial text
        let tutorialText = EntityFactory.createInstructionText();
        tutorialText.position = CGPoint(x: GameContext.screenSize.width/2, y: (screenSize.height - gameContentSize.height)/2);
        gameStageLayer.addChild(tutorialText);
    }
    
    private func updatePlayer() {
        if (aimingDeltaPoint != CGVector.zero) {
            crosshair?.update(aimingDeltaPoint, boundary: gameContentSize, sensitivity: GameContext.crossHairSensitivity);
            let crossHairPosInScreen = playerLayer.convertPoint(crosshair!.position, toNode: gameStageLayer);
            rifle?.update(crossHairPosInScreen);
            //aimingDeltaPoint = CGVector.zero;    // Only process once
        }
    }
}