//
//  GameConstants.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/5/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit
class GameConstants {
    
    // Game Controller Texture
    static let CONTROLLER_JOYSTICK_BASE = "joystick_base";
    static let CONTROLLER_JOYSTICK_BALL = "joystick_ball";
    static let CONTROLLER_BUTTON_IDLE = "arcade_button";
    static let CONTROLLER_BUTTON_PRESSED = "arcade_button_down";
    
    // Button Key
    static let FIRE_BUTTON_KEY = "Fire Button";
    
    
    // Node name
    static let RIFLE_NAME = "Rifle";
    static let CROSSHAIR_NAME = "CrossHair";
    
    // Number image name
    static let NUMBER_IMAGES:[String] = ["text_0", "text_1", "text_2", "text_3", "text_4", "text_5", "text_6", "text_7", "text_8", "text_9"];
    static let SMALL_NUMBER_IMAGES:[String] = ["text_0_small", "text_1_small", "text_2_small", "text_3_small", "text_4_small", "text_5_small", "text_6_small", "text_7_small", "text_8_small", "text_9_small"];
    
    // Text image name
    /*
    static let TEXT_READY_KEY = "ready";
    static let TEXT_GO_KEY = "go";
    static let TEXT_TIMEUP_KEY = "timeup";
    static let TEXT_GAMEOVER_KEY = "gameover";
    static let TEXT_IMAGE_NAME_MAP:[String:String] = [TEXT_READY_KEY:"text_ready",
                                                      TEXT_GO_KEY:"text_go",
                                                      TEXT_TIMEUP_KEY:"text_timeup",
                                                      TEXT_GAMEOVER_KEY:"text_gameover"];
    */
    static let TIMEUP_TEXT_IMAGE = "text_timeup";
    static let READY_TEXT_IMAGE = "text_ready";
    
    
    // Pre-defined action
    static let FADE_AWAY = SKAction.sequence([SKAction.fadeAlphaTo(0, duration: 1), SKAction.removeFromParent()]);
    static let REMOVE_AFTER_1_SEC = SKAction.sequence([SKAction.waitForDuration(1), SKAction.removeFromParent()]);
    static let SHOW_1_SEC = SKAction.sequence([SKAction.unhide(), SKAction.waitForDuration(1), SKAction.hide()]);
    static let BLINK = SKAction.sequence([SKAction.fadeOutWithDuration(0.1), SKAction.fadeInWithDuration(0.1)]);
    static let BLINK_FOR_TIME = SKAction.repeatAction(BLINK, count: 10);
    static let SHAKE = SKAction.sequence([SKAction.rotateByAngle(-CGFloat(M_PI_4), duration: 0.1), SKAction.rotateByAngle(CGFloat(M_PI_2), duration: 0.2), SKAction.rotateByAngle(-CGFloat(M_PI_4), duration: 0.1)]);
    static let SMALL_SHAKE = SKAction.sequence([SKAction.rotateByAngle(-CGFloat(M_PI_4/2), duration: 1), SKAction.rotateByAngle(CGFloat(M_PI_4), duration: 2), SKAction.rotateByAngle(-CGFloat(M_PI_4/2), duration: 1)]);
    static let SCALE_BLINK = SKAction.sequence([SKAction.scaleTo(0.5, duration: 0.5), SKAction.scaleTo(1.5, duration: 0.5)]);
    static let HILIGHT = SKAction.repeatAction(SCALE_BLINK, count: 3);
    static let POP_UP = SKAction.sequence([SKAction.scaleTo(0, duration: 0), SKAction.scaleTo(1, duration: 0.5)]);
    static let VERTICAL_POP_UP = SKAction.sequence([SKAction.scaleYTo(0, duration: 0), SKAction.scaleYTo(1, duration: 0.5)]);
    static let READY_GO = SKAction.sequence([SKAction.animateWithTextures([SKTexture(imageNamed: "text_ready"), SKTexture(imageNamed: "text_ready"), SKTexture(imageNamed: "text_go")], timePerFrame: 0.75), SKAction.removeFromParent()]);
    static let READY_GO_AUDIO = SKAction.sequence([SKAction.playSoundFileNamed("Sounds/ready.wav", waitForCompletion: false), SKAction.waitForDuration(1.5), SKAction.playSoundFileNamed("Sounds/go.wav", waitForCompletion: false)]);
    
    static let ARCADE_DUCK_HIT = SKAction.sequence([SKAction.scaleYTo(0.1, duration: 0.1), SKAction.customActionWithDuration(0.0, actionBlock: { (node:SKNode!, elapsed:CGFloat) -> Void in
        node.removeAllChildren();
    }), SKAction.removeFromParent()]);
    
    static let SPEED_DUCK_HIT = SKAction.sequence([SKAction.scaleXTo(1, duration: 0.1), SKAction.scaleXTo(0.1, duration: 0.1), SKAction.customActionWithDuration(0, actionBlock: {
        (node:SKNode!, elapsed:CGFloat) -> Void in
        let targetNode:SKSpriteNode? = node as? SKSpriteNode;
        if (targetNode != nil) {
            targetNode!.texture = SKTexture(imageNamed: "duck_outline_back");
        }
    }), SKAction.scaleXTo(-0.1, duration: 0.1), SKAction.scaleXTo(-1, duration: 0.1)]);
    
    // Audio file
    static let COMMON_AUDIO_FILE_TYPE = "wav";
    
    // Sound effect
    static let HIT_WOOD_FILE_NAME = "hit-wood";
    static let HIT_METAL_FILE_NAME = "hit-metal";
    static let NO_AMMO_FILE_NAME = "no-ammo";
    static let RELOAD_FILE_NAME = "reload";
    static let STICK_BREAK_FILE_NAME = "stick-break";
    static let BUTTON_CLICK_FILE_NAME = "click1";
    
    // Human voice
    static let READY_FILE_NAME = "ready";
    static let GO_FILE_NAME = "go";
    static let TIME_OVER_FILE_NAME = "time_over";
    static let HURRY_UP_FILE_NAME = "hurry_up";
    static let NEW_HIGHT_SCORE_FILE_NAME = "new_highscore";
    
    // BGM
    static let THEME_ONE_NAME = "Barroom Ballet Short Refined";
    static let MENU_BGM_NAME = "colonel_bogey_march_full";
    static let BGM_TYPE = "mp3";
    
}