//
//  GameConfigUtils.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/22/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import Foundation
import UIKit

class GameConfigUtils {
    
    private static var gameConfigs:NSMutableDictionary? = nil;
    
    // Game configuration
    // Get the config file path from the app bundle
    private static func getConfigFileBundlePath() -> String? {
        let bundlePath = NSBundle.mainBundle().pathForResource(PropertyFileConstants.GAME_CONFIG_FILE_NAME, ofType:PropertyFileConstants.GAME_CONFIG_FILE_TYPE);
        print("Config file bundle path = \(bundlePath)");
        return bundlePath;
    }
    
    // Get the config file path from the user domain
    private static func getConfigFilePath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as NSArray
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let path = documentsDirectory.stringByAppendingPathComponent(PropertyFileConstants.GAME_CONFIG_FILE_FULL_NAME);
        print("Config file path = \(path)");
        return path;
    }
    
    // Read the config file and get the config details.
    // Plist files in the project bundle are read only.
    // You need to save a copy of the plist in the Documents folder than edit and read that one.
    private static func getGameConfig(forceRefresh forceRefresh:Bool=false) -> NSMutableDictionary? {
        if (GameConfigUtils.gameConfigs == nil || forceRefresh) {
            let path = GameConfigUtils.getConfigFilePath();
            let fileManager = NSFileManager.defaultManager();
            if (!fileManager.fileExistsAtPath(path)) {
                // Config file not in the user domain, copy from the bundle
                let bundlePath:String? = GameConfigUtils.getConfigFileBundlePath();
                if (bundlePath == nil) {
                    print("Error: Cannot get game configurations from bundle.");
                    return nil;
                }
                print("Config file not in the user domain, copy from the bundle path = \(bundlePath!).")
                do {
                    try fileManager.copyItemAtPath(bundlePath!, toPath: path);
                } catch {
                    print("Copying the config file from bundle to user domain failed. Error:\(error)");
                    return nil;
                }
            }
            
            let existingGameConfigs = NSDictionary(contentsOfFile: path);
            GameConfigUtils.gameConfigs = NSMutableDictionary(dictionary: existingGameConfigs!);
        }
        //print("Current game config: \(GameConfigUtils.gameConfigs)")
        return GameConfigUtils.gameConfigs;
    }
    
    // Persist the config file with the current configuration
    // Plist files in the project bundle are read only.
    // You need to save a copy of the plist in the Documents folder than edit and read that one.
    static func persistGameConfig() {
        if (GameConfigUtils.gameConfigs == nil) {
            print("Error: Unable to persist the game config since the data is not loaded yet.")
            return;
        }
        
        let path:String? = GameConfigUtils.getConfigFilePath();
        if (path == nil) {
            print("Error: Cannot get config file path.");
            return;
        }
        
        let succeed:Bool = GameConfigUtils.gameConfigs!.writeToFile(path!, atomically:false);
        
        //print("UserGameData file updated. Path = \(path!). Succeed = \(succeed)");
        print("UserGameData file persisted. Succeed = \(succeed)");
    }
    
    
    // Get data
    
    static func getCrosshairSensitivity() -> Int {
        let gameConfig: NSMutableDictionary? = GameConfigUtils.getGameConfig();
        if (gameConfig != nil) {
            let sensitivity:Int? = gameConfig![PropertyFileConstants.CROSSHAIR_SENSITIVITY] as? Int;
            if (sensitivity != nil) {
                return sensitivity!;
            }
        }
        print("Error: Cannot get CrosshairSensitivity.");
        return 0;
    }
    
    static func setCrosshairSensitivity(sensitivity:Int) {
        let gameConfig: NSMutableDictionary? = GameConfigUtils.getGameConfig();
        if (gameConfig != nil) {
            gameConfig!.setObject(sensitivity, forKey: PropertyFileConstants.CROSSHAIR_SENSITIVITY);
        } else {
            print("Error: Cannot set CrosshairSensitivity.");
        }
    }
    
    static func getBgmVolumn() -> Int {
        let gameConfig: NSMutableDictionary? = GameConfigUtils.getGameConfig();
        if (gameConfig != nil) {
            let volumn:Int? = gameConfig![PropertyFileConstants.BGM_VOLUMN] as? Int;
            if (volumn != nil) {
                return volumn!;
            }
        }
        print("Error: Cannot get BgmVolumn.");
        return 0;
    }
    
    static func setBgmVolumn(volumn:Int) {
        let gameConfig: NSMutableDictionary? = GameConfigUtils.getGameConfig();
        if (gameConfig != nil) {
            gameConfig!.setObject(volumn, forKey: PropertyFileConstants.BGM_VOLUMN);
        } else {
            print("Error: Cannot set BgmVolumn.");
        }
    }
    
    static func getSeVolumn() -> Int {
        let gameConfig: NSMutableDictionary? = GameConfigUtils.getGameConfig();
        if (gameConfig != nil) {
            let volumn:Int? = gameConfig![PropertyFileConstants.SE_VOLUMN] as? Int;
            if (volumn != nil) {
                return volumn!;
            }
        }
        print("Error: Cannot get SeVolumn.");
        return 0;
    }
    
    static func setSeVolumn(volumn:Int) {
        let gameConfig: NSMutableDictionary? = GameConfigUtils.getGameConfig();
        if (gameConfig != nil) {
            gameConfig!.setObject(volumn, forKey: PropertyFileConstants.SE_VOLUMN);
        } else {
            print("Error: Cannot set SeVolumn.");
        }
    }
    
    static func isArcadeButtonOnLeft() -> Bool {
        let gameConfig: NSMutableDictionary? = GameConfigUtils.getGameConfig();
        if (gameConfig != nil) {
            let buttonOnLeft:Bool? = gameConfig![PropertyFileConstants.ARCADE_BUTTON_ON_LEFT] as? Bool;
            if (buttonOnLeft != nil) {
                return buttonOnLeft!;
            }
        }
        print("Error: Cannot get ArcadeButtonOnLeft.");
        return true;
    }
    
    static func setArcadeButtonOnLeft(isOnLeft:Bool) {
        let gameConfig: NSMutableDictionary? = GameConfigUtils.getGameConfig();
        if (gameConfig != nil) {
            gameConfig!.setObject(isOnLeft, forKey: PropertyFileConstants.ARCADE_BUTTON_ON_LEFT);
        } else {
            print("Error: Cannot set ArcadeButtonOnLeft.");
        }
    }
    
}
