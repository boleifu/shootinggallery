//
//  Table.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/5/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class Table : GameEntity {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "rounded_table");
    }
    
    override func configProperties(){
        self.name = "Table";
        self.zPosition = LayerDepth.TABLE.getDepth();
        self.anchorPoint = CGPoint(x: 0, y: 1); // Top-left corner
    }
}