//
//  GameOverPage.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/20/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GameOverPageManager : MenuPageManager {
    
    var updatabelTexts:[UpdatableText] = [];
    
    override func setupPage() {
        self.pageType = MenuPage.GAME_OVER_PAGE;
        
        // Add Page Title
        let titleText = MenuObjectFactory.createGameSummaryText();
        titleText.position = GameContext.pageTitlePos;
        menuTextNodes.append(titleText);
        pageLayer.addChild(titleText);
        
        // Add Game Summary
        let gameReportItemGapY = (GameContext.menuContentStartY - GameContext.menuContentEndY)/4;
        var currentY = GameContext.menuContentStartY;
        
        // Add Score text
        let scoreText = MenuObjectFactory.createScoreText();
        scoreText.position = CGPoint(x: GameContext.gameReportCenterX, y: currentY);
        menuTextNodes.append(scoreText);
        pageLayer.addChild(scoreText);
        
        // Add Score value text
        let scoreValueText = MenuObjectFactory.createScoreValueText();
        scoreValueText.position = CGPoint(x: GameContext.gameReportCenterX, y: currentY);
        updatabelTexts.append(scoreValueText);
        pageLayer.addChild(scoreValueText);
        
        currentY -= gameReportItemGapY;
        
        // Add Game time text
        let gameTimeText = MenuObjectFactory.createGameTimeText();
        gameTimeText.position = CGPoint(x: GameContext.gameReportCenterX, y: currentY);
        menuTextNodes.append(gameTimeText);
        pageLayer.addChild(gameTimeText);
        
        // Add Game time value text
        let gameTimeValueText = MenuObjectFactory.createGameTimeValueText();
        gameTimeValueText.position = CGPoint(x: GameContext.gameReportCenterX, y: currentY);
        updatabelTexts.append(gameTimeValueText);
        pageLayer.addChild(gameTimeValueText);
        
        currentY -= gameReportItemGapY;
        
        // Add Max multiplier text
        let maxMultiplierText = MenuObjectFactory.createMaxMultiplierText();
        maxMultiplierText.position = CGPoint(x: GameContext.gameReportCenterX, y: currentY);
        menuTextNodes.append(maxMultiplierText);
        pageLayer.addChild(maxMultiplierText);
        
        // Add Max multiplier value text
        let maxMultiplierValueText = MenuObjectFactory.createMaxMultiplierValueText();
        maxMultiplierValueText.position = CGPoint(x: GameContext.gameReportCenterX, y: currentY);
        updatabelTexts.append(maxMultiplierValueText);
        pageLayer.addChild(maxMultiplierValueText);
        
        currentY -= gameReportItemGapY;
        
        // Add Accuracy text
        let accuracyText = MenuObjectFactory.createAccuracyText();
        accuracyText.position = CGPoint(x: GameContext.gameReportCenterX, y: currentY);
        menuTextNodes.append(accuracyText);
        pageLayer.addChild(accuracyText);
        
        // Add Accuracy value text
        let accuracyValueText = MenuObjectFactory.createAccuracyValueText();
        accuracyValueText.position = CGPoint(x: GameContext.gameReportCenterX, y: currentY);
        updatabelTexts.append(accuracyValueText);
        pageLayer.addChild(accuracyValueText);
        
        currentY -= gameReportItemGapY;
        
        // Add Bullseye text
        let bullseyeText = MenuObjectFactory.createBullseyeText();
        bullseyeText.position = CGPoint(x: GameContext.gameReportCenterX, y: currentY);
        menuTextNodes.append(bullseyeText);
        pageLayer.addChild(bullseyeText);
        
        // Add Bullseye value text
        let bullseyeValueText = MenuObjectFactory.createBullseyeValueText();
        bullseyeValueText.position = CGPoint(x: GameContext.gameReportCenterX, y: currentY);
        updatabelTexts.append(bullseyeValueText);
        pageLayer.addChild(bullseyeValueText);
        
        // Add play again text
        let playAgainText = MenuObjectFactory.createPlayAgainText();
        playAgainText.position = GameContext.actionTwoTextPos;
        selectableNodes.append(playAgainText);
        pageLayer.addChild(playAgainText);
        
        // Add Back text
        let backText = MenuObjectFactory.createBackText(MenuPage.WELCOME_PAGE);
        backText.position = GameContext.actionOneTextPos;
        selectableNodes.append(backText);
        pageLayer.addChild(backText);
    }
    
    override func enablePage() {
        super.enablePage();
        print(GameRuntimeContext.getGameSummaryReport());
        
        // Update all data
        for updatableText in updatabelTexts {
            updatableText.update();
        }
        
        if (GameRuntimeContext.breakTheRecord) {
            AudioUtils.playNewHighScoreSound();
        }
        
        // Persist the user game data
        UserDataUtils.persistUserData();
    }
}