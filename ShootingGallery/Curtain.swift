//
//  Curtain.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/18/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class Curtain : GameEntity {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "curtain");
    }
    
    override func configProperties(){
        self.name = "Curtain";
        self.zPosition = LayerDepth.MENU_BACKGROUND.getDepth();
        self.anchorPoint = CGPoint(x: 0, y: 1); // Left-top
    }
    
    func openCurtain() {
        GameRuntimeContext.gameMode = GameRuntimeContext.targetGameMode;
        GameRuntimeContext.menuStatus = MenuLifyCycle.DISABLED;
        self.runAction(SKAction.scaleXTo(0, duration: 1), completion: {
            self.hidden = true;
        });
    }
    
    func closeCurtain() {
        self.hidden = false;
        self.runAction(SKAction.scaleXTo(1, duration: 1), completion: {
            GameRuntimeContext.menuStatus = MenuLifyCycle.ENABLED;
        });
    }
}