//
//  WaterOne.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/6/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class Water : GameEntity, Damagable {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "water_long");
    }
    
    override func configProperties(){
        self.name = "Water";
        self.zPosition = LayerDepth.ROW_ONE_BLOCKER.getDepth();
        self.anchorPoint = CGPoint(x: 1, y: 0);  // Right-bottom corner
    }
    
    func isHit(hitPosOnTarget:CGPoint) -> Bool {
        return false;
    }
    
    func hit(hitPosOnNode:CGPoint) -> Bool {
        return false;
    }
    
}