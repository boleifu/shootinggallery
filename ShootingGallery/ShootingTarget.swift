//
//  DuckTarget.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/6/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

/**
 * This class defines the shooting target in the game
 * Usually contains two target objects.
 * A base object (required) and a add on object (optional).
 */
class ShootingTarget : SKNode {
    
    let base:TargetObject;
    let addOn:TargetObject?;
    
    init(base: TargetObject, addOn: TargetObject?) {
        self.base = base;
        self.addOn = addOn;
        super.init();
        
        self.name = "Shooting Target";
        self.position = base.position;
        
        base.position = CGPoint.zero;
        self.addChild(base);
        if (addOn != nil) {
            addOn?.position = CGPoint(x:0, y:base.size.height);
            base.addChild(addOn!);
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented");
    }
    
    deinit {
        //print("Game Entity \(self.name) deinit.");
    }
    
    /**
     * Process the bullet hit
     * @param hitPosInNode The hit position in this shooting target
     * @param Whether the bullseye hit
     */
    func hit(hitPosInNode:CGPoint) -> Bool {
        let hitPosInBase = base.convertPoint(hitPosInNode, fromNode: self);
        
        // Process base hit first
        if (base.hit(hitPosInBase)) {
            return true;
        } else {
            // Not hit base, check add on
            if (addOn != nil) {
                let hitPosInAddOn = addOn!.convertPoint(hitPosInNode, fromNode: self);
                return addOn!.hit(hitPosInAddOn);
            }
        }
        return false;
    }
    
    
}