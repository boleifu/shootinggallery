//
//  SettingText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/22/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SettingText : SelectableNode {
    
    override func getText() -> String? {
        return NSLocalizedString("SETTING_TEXT", comment: "SettingText");
        //return GameTextConstants.SETTING_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Setting Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Bottom;

    }
    
    override func select() {
        // Move to the setting page
        GameRuntimeContext.menuPage = MenuPage.SETTING_PAGE;
        unHilight();
    }
}