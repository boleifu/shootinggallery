//
//  ShootingTarget.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/6/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

/**
 * This class defines the target object in the game
 */
class TargetObject : GameEntity, Damagable {
    
    var hitBox:SKShapeNode? = SKShapeNode(); // The bullseye
    
    // Constructor
    override init(position:CGPoint, face:CGFloat, targetWidth:CGFloat?, targetHeight:CGFloat?) {
        super.init(position:position, face:face, targetWidth:targetWidth, targetHeight:targetHeight);
        
        // Init the bullseye
        setHitBox();
        
        // Run the pre-set action
        self.runAction(getAction());
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     * Get the score value of the bullseye hit
     */
    func getScoreValue() -> Int {
        return 0;
    }
    
    /**
     * Initialize the bullseye.
     * Must be override
     */
    func setHitBox() {
        // Not implemented
        fatalError("setHitBox() has not been implemented");
    }
    
    /**
     * Get the embadded action of this object.
     * Must be override
     */
    func getAction() -> SKAction {
        // Not implemented
        fatalError("getAction() has not been implemented");
    }
    
    // Damagable protocal
    
    /**
     * Check whether the bullseye hit
     */
    func isHit(hitPosOnTarget:CGPoint) -> Bool {
        if (hitBox == nil) {
            return false;
        }
        
        let hitPosOnHitBox = self.convertPoint(hitPosOnTarget, toNode: hitBox!);
        if (CGPathContainsPoint(hitBox!.path, nil, hitPosOnHitBox, false)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Process the bullseye hit
     */
    func hit(hitPosOnNode:CGPoint) -> Bool {
        // Not implemented
        fatalError("hit() has not been implemented");
    }
}