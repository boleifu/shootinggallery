//
//  CrossHair.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/4/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class CrossHair : GameEntity {
    
    // ======== Config ========
    override func configProperties() {
        self.name = "CrossHair";
        self.zPosition = LayerDepth.CROSSHAIR.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5);  // Middle
    }
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "crosshair_outline_large");
    }
    
    // Runtime update
    /**
     * Update the crosshair position
     */
    func update(deltaPos: CGVector, boundary:CGSize, sensitivity:CGFloat) {
        let newPos = CGPoint(x: self.position.x + deltaPos.dx * sensitivity, y: self.position.y + deltaPos.dy * sensitivity);
        self.position = Utils.fitInBoundary(newPos, boundary: boundary);
    }
}