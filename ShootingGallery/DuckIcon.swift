//
//  DuckIcon.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/11/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class DuckIcon : GameEntity {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "icon_duck");
    }
    
    override func configProperties(){
        self.name = "DuckIcon";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 1);  // Top-middle
    }
}