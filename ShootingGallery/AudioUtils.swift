//
//  AudioUtils.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/14/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import AVFoundation

class AudioUtils {
    
    static var seVolumn:Float = 0;
    static var bgmVolumn:Float = 0;
    
    static var hitWoodAudioPlayer: AVAudioPlayer? = nil;
    static var hitMetalAudioPlayer: AVAudioPlayer? = nil;
    static var noAmmoAudioPlayer: AVAudioPlayer? = nil;
    static var reloadAudioPlayer: AVAudioPlayer? = nil;
    //static var stickBreakAudioPlayer: AVAudioPlayer? = nil;
    static var buttonClickAudioPlayer: AVAudioPlayer? = nil;
    
    static var readyAudioPlayer: AVAudioPlayer? = nil;
    static var goAudioPlayer: AVAudioPlayer? = nil;
    static var timeOverAudioPlayer: AVAudioPlayer? = nil;
    static var hurryUpAudioPlayer: AVAudioPlayer? = nil;
    static var newHightScoreAudioPlayer:AVAudioPlayer? = nil;
    
    static var menuBgmAudioPlayer: AVAudioPlayer? = nil;
    static var bgmThemeOneAudioPlayer: AVAudioPlayer? = nil;
    
    
    static func createAudioPlayer(audioFileName:String, _ audioFileType:String) -> AVAudioPlayer? {
        // Setup sound player
        let path = NSBundle.mainBundle().pathForResource("Sounds/"+audioFileName, ofType: audioFileType);
        if (path == nil) {
            print("ERROR: Unable to find the audio file \(audioFileName).\(audioFileType)");
            return nil;
        }
        let url = NSURL.fileURLWithPath(path!);
        do {
            return try AVAudioPlayer(contentsOfURL: url);
        } catch let error as NSError {
            print("ERROR: Failed to create the audio player for the file \(audioFileName).\(audioFileType). Error: \(error.localizedDescription)");
            return nil;
        }
    }
    
    static func setupAllAudioPlayers() {
        updateBgmVolumn();
        updateSeVolumn();
        
        // Sound effects
        if (hitWoodAudioPlayer == nil) {
            setupHitWoodAudioPlayer();
        }
        if (hitMetalAudioPlayer == nil) {
            setupHitMetalAudioPlayer();
        }
        if (noAmmoAudioPlayer == nil) {
            setupNoAmmoAudioPlayer();
        }
        if (reloadAudioPlayer == nil) {
            setupReloadAudioPlayer();
        }
        //if (stickBreakAudioPlayer == nil) {
        //    setupStickBreakAudioPlayer();
        //}
        if (buttonClickAudioPlayer == nil) {
            setupButtonClickAudioPlayer();
        }
        
        // Human voices
        if (readyAudioPlayer == nil) {
            setupReadyAudioPlayer();
        }
        if (goAudioPlayer == nil) {
            setupGoAudioPlayer();
        }
        if (timeOverAudioPlayer == nil) {
            setupTimeOverAudioPlayer();
        }
        if (hurryUpAudioPlayer == nil) {
            setupHurryUpAudioPlayer();
        }
        if (newHightScoreAudioPlayer == nil) {
            setupNewHightScoreAudioPlayer();
        }
        
        // BGM
        if (menuBgmAudioPlayer == nil) {
            setupMenuAudioPlayer();
        }
        if (bgmThemeOneAudioPlayer == nil) {
            setupBgmThemeOneAudioPlayer();
        }
    }
    
    // Hit wood sound
    static func setupHitWoodAudioPlayer() {
        hitWoodAudioPlayer = createAudioPlayer(GameConstants.HIT_WOOD_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playHitWoodSound() {
        if (hitWoodAudioPlayer == nil) {
            setupHitWoodAudioPlayer();
        }
        // Double check it
        if (hitWoodAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                hitWoodAudioPlayer?.volume = seVolumn;
                hitWoodAudioPlayer?.prepareToPlay();
                hitWoodAudioPlayer?.play();
            }
        }
    }
    
    // Hit metal sound
    static func setupHitMetalAudioPlayer() {
        hitMetalAudioPlayer = createAudioPlayer(GameConstants.HIT_METAL_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playHidMetalSound() {
        if (hitMetalAudioPlayer == nil) {
            setupHitMetalAudioPlayer();
        }
        // Double check it
        if (hitMetalAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                hitMetalAudioPlayer?.volume = seVolumn;
                hitMetalAudioPlayer?.prepareToPlay();
                hitMetalAudioPlayer?.play();
            }
        }
    }
    
    // No Ammo sound
    static func setupNoAmmoAudioPlayer() {
        noAmmoAudioPlayer = createAudioPlayer(GameConstants.NO_AMMO_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playNoAmmoSound() {
        if (noAmmoAudioPlayer == nil) {
            setupNoAmmoAudioPlayer();
        }
        // Double check it
        if (noAmmoAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                noAmmoAudioPlayer?.volume = seVolumn;
                noAmmoAudioPlayer?.prepareToPlay();
                noAmmoAudioPlayer?.play();
            }
        }
    }
    
    // Reload sound
    static func setupReloadAudioPlayer() {
        reloadAudioPlayer = createAudioPlayer(GameConstants.RELOAD_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playReloadSound() {
        if (reloadAudioPlayer == nil) {
            setupReloadAudioPlayer();
        }
        // Double check it
        if (reloadAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                reloadAudioPlayer?.volume = seVolumn;
                reloadAudioPlayer?.prepareToPlay();
                reloadAudioPlayer?.play();
            }
        }
    }
    
    // Stick break sound
    /*
    static func setupStickBreakAudioPlayer() {
        stickBreakAudioPlayer = createAudioPlayer(GameConstants.STICK_BREAK_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playStickBreakSound() {
        if (stickBreakAudioPlayer == nil) {
            setupStickBreakAudioPlayer();
        }
        // Double check it
        if (stickBreakAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                stickBreakAudioPlayer?.volume = seVolumn;
                stickBreakAudioPlayer?.prepareToPlay();
                stickBreakAudioPlayer?.play();
            }
        }
    }
   */
    
    // Button click sound
    static func setupButtonClickAudioPlayer() {
        buttonClickAudioPlayer = createAudioPlayer(GameConstants.BUTTON_CLICK_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playButtonClickSound() {
        if (buttonClickAudioPlayer == nil) {
            setupButtonClickAudioPlayer();
        }
        // Double check it
        if (buttonClickAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                buttonClickAudioPlayer?.volume = seVolumn;
                buttonClickAudioPlayer?.prepareToPlay();
                buttonClickAudioPlayer?.play();
            }
        }
    }
    
    // Ready sound
    static func setupReadyAudioPlayer() {
        readyAudioPlayer = createAudioPlayer(GameConstants.READY_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playReadySound() {
        if (readyAudioPlayer == nil) {
            setupReadyAudioPlayer();
        }
        // Double check it
        if (readyAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                readyAudioPlayer?.volume = seVolumn;
                readyAudioPlayer?.prepareToPlay();
                readyAudioPlayer?.play();
            }
        }
    }
    
    // Go sound
    static func setupGoAudioPlayer() {
        goAudioPlayer = createAudioPlayer(GameConstants.GO_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playGoSound() {
        if (goAudioPlayer == nil) {
            setupGoAudioPlayer();
        }
        // Double check it
        if (goAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                goAudioPlayer?.volume = seVolumn;
                goAudioPlayer?.prepareToPlay();
                goAudioPlayer?.play();
            }
        }
    }
    
    // Time Over sound
    static func setupTimeOverAudioPlayer() {
        timeOverAudioPlayer = createAudioPlayer(GameConstants.TIME_OVER_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playTimeOverSound() {
        if (timeOverAudioPlayer == nil) {
            setupTimeOverAudioPlayer();
        }
        // Double check it
        if (timeOverAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                timeOverAudioPlayer?.volume = seVolumn;
                timeOverAudioPlayer?.prepareToPlay();
                timeOverAudioPlayer?.play();
            }
        }
    }
    
    // Hurry up sound
    static func setupHurryUpAudioPlayer() {
        hurryUpAudioPlayer = createAudioPlayer(GameConstants.HURRY_UP_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playHurryUpSound() {
        if (hurryUpAudioPlayer == nil) {
            setupHurryUpAudioPlayer();
        }
        // Double check it
        if (hurryUpAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                hurryUpAudioPlayer?.volume = seVolumn;
                hurryUpAudioPlayer?.prepareToPlay();
                hurryUpAudioPlayer?.play();
            }
        }
    }
    
    // New High Score sound
    static func setupNewHightScoreAudioPlayer() {
        newHightScoreAudioPlayer = createAudioPlayer(GameConstants.NEW_HIGHT_SCORE_FILE_NAME, GameConstants.COMMON_AUDIO_FILE_TYPE);
    }
    
    static func playNewHighScoreSound() {
        if (newHightScoreAudioPlayer == nil) {
            setupNewHightScoreAudioPlayer();
        }
        // Double check it
        if (newHightScoreAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                newHightScoreAudioPlayer?.volume = seVolumn;
                newHightScoreAudioPlayer?.prepareToPlay();
                newHightScoreAudioPlayer?.play();
            }
        }
    }
    
    // Menu BGM
    static func setupMenuAudioPlayer() {
        menuBgmAudioPlayer = createAudioPlayer(GameConstants.MENU_BGM_NAME, GameConstants.BGM_TYPE);
    }
    
    static func playMenuBgm() {
        if (menuBgmAudioPlayer == nil) {
            setupMenuAudioPlayer();
        }
        // Double check it
        if (menuBgmAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                menuBgmAudioPlayer?.numberOfLoops = -1;
                menuBgmAudioPlayer?.volume = bgmVolumn;
                menuBgmAudioPlayer?.currentTime = 0;
                menuBgmAudioPlayer?.prepareToPlay();
                menuBgmAudioPlayer?.play();
            }
        }
    }
    
    static func stopMenuBgm() {
        if (menuBgmAudioPlayer == nil) {
            return;
        }
        menuBgmAudioPlayer?.stop();
    }
    
    
    // BGM 1
    static func setupBgmThemeOneAudioPlayer() {
        bgmThemeOneAudioPlayer = createAudioPlayer(GameConstants.THEME_ONE_NAME, GameConstants.BGM_TYPE);
    }
    
    static func playBgmThemeOne() {
        if (bgmThemeOneAudioPlayer == nil) {
            setupBgmThemeOneAudioPlayer();
        }
        // Double check it
        if (bgmThemeOneAudioPlayer != nil) {
            dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)) {
                bgmThemeOneAudioPlayer?.numberOfLoops = -1;
                bgmThemeOneAudioPlayer?.volume = bgmVolumn;
                bgmThemeOneAudioPlayer?.currentTime = 0;
                bgmThemeOneAudioPlayer?.prepareToPlay();
                bgmThemeOneAudioPlayer?.play();
            }
        }
    }
    
    static func stopBgmThemeOne() {
        if (bgmThemeOneAudioPlayer == nil) {
            return;
        }
        bgmThemeOneAudioPlayer?.stop();
    }
    
    
    // Helper function
    static func updateBgmVolumn() {
        let updatedBgmVolumn = 1 / Float(GameContext.gameSettingMaxValue - GameContext.gameSettingMinValue) * Float(GameConfigUtils.getBgmVolumn());
        AudioUtils.bgmVolumn = updatedBgmVolumn;
        
        // BGM are long running sound, thus update volumn here
        if (menuBgmAudioPlayer != nil) {
            menuBgmAudioPlayer?.volume = bgmVolumn;
        }
        if (bgmThemeOneAudioPlayer != nil) {
            bgmThemeOneAudioPlayer?.volume = bgmVolumn;
        }
        
        print("BGM volumn updated to \(updatedBgmVolumn)");
    }
    
    static func updateSeVolumn() {
        let updatedSeVolumn = 1 / Float(GameContext.gameSettingMaxValue - GameContext.gameSettingMinValue) * Float(GameConfigUtils.getSeVolumn());
        AudioUtils.seVolumn = updatedSeVolumn;
        print("SE volumn updated to \(updatedSeVolumn)");
    }
    
}
