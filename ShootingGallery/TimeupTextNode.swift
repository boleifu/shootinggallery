//
//  TimeupTextNode.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/11/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class TimeupTextNode : EnvironmentObject {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: GameConstants.TIMEUP_TEXT_IMAGE);
    }
    
    override func configProperties(){
        self.name = "TimeupText";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5); // Middle
    }
    
    override func getAction() -> SKAction {
        return SKAction.sequence([GameConstants.HILIGHT, SKAction.removeFromParent()]);
    }
    
    override func afterAction() {
        // Power off the game
        GameRuntimeContext.gamePoweredOn = false;
        
        // Switch the mode
        GameRuntimeContext.gameMode = GameMode.MENU;
        GameRuntimeContext.menuPage = MenuPage.GAME_OVER_PAGE;
        GameRuntimeContext.menuStatus = MenuLifyCycle.LEAVING_GAME;
    }
}