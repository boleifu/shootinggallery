//
//  FrontRowGrassSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/13/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class FrontRowGrassSpawner:BlockerSpawner {
    
    override func getTarget() -> SKSpriteNode {
        return EntityFactory.createGrass();
    }
    
    override func configSpawner() {
        self.objectLiftTime = self.objectLiftTime*2;
        super.configSpawner();
        //self.initPos.x -= self.objectWidth/2;
    }
}