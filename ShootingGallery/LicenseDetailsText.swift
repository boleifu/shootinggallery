//
//  LicenseDetailsText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/29/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class LicenseDetailsText : UpdatableText {
    
    var currentIndex:Int = 0;
    let texts:[String];
    
    // Constructor
    init(fontSize:CGFloat, fontColor:UIColor, texts:[String]) {
        self.texts = texts;
        super.init(fontSize:fontSize, fontColor:fontColor);
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented");
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "License Details Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
        self.currentIndex = 0;
        self.text = texts[0];
    }
    
    override func update() {
        // Update index
        currentIndex += 1;
        if (currentIndex >= texts.count) {
            currentIndex = 0;
        }
        
        self.text = texts[currentIndex];
    }
}