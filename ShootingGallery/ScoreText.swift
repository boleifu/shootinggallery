//
//  ScoreText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class ScoreText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("SCORE_TEXT", comment: "ScoreText");
        //return GameTextConstants.SCORE_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Score Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Center;
    }
}