//
//  SubTitleText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/19/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SubTitleText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("GAME_SUB_TITLE", comment: "Game Sub Title");
        //GameTextConstants.GAME_SUB_TITLE;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.SUB_TITLE_FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Sub Title Text";
        self.zPosition = LayerDepth.MENU_SUB_TITLE.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Bottom;
    }
}