//
//  SettingPage.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/22/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GameSettingPageManager : MenuPageManager {
    
    override func setupPage() {
        self.pageType = MenuPage.SETTING_PAGE;
        
        // Add Page Title
        let titleText = MenuObjectFactory.createGameSettingText();
        titleText.position = GameContext.pageTitlePos;
        menuTextNodes.append(titleText);
        pageLayer.addChild(titleText);
        
        // Add setting options
        let gameReportItemGapY = (GameContext.menuContentStartY - GameContext.menuContentEndY)/4;
        var currentY = GameContext.menuContentStartY;
        
        // Add sensitivity setting
        let sensitivityText = MenuObjectFactory.createSensitivityText();
        sensitivityText.position = CGPoint(x: GameContext.gameSettingCenterX, y: currentY);
        menuTextNodes.append(sensitivityText);
        pageLayer.addChild(sensitivityText);
        
        let sensitivityValueText = MenuObjectFactory.createSensitivityValueText();
        sensitivityValueText.position = CGPoint(x: GameContext.gameSettingValueX, y: currentY);
        selectableNodes.append(sensitivityValueText);
        pageLayer.addChild(sensitivityValueText);
        
        currentY -= gameReportItemGapY;
        
        // Add BGM volumn setting
        let bgmText = MenuObjectFactory.createBgmText();
        bgmText.position = CGPoint(x: GameContext.gameSettingCenterX, y: currentY);
        menuTextNodes.append(bgmText);
        pageLayer.addChild(bgmText);
        
        let bgmVolumValueText = MenuObjectFactory.createBgmVolumnValueText();
        bgmVolumValueText.position = CGPoint(x: GameContext.gameSettingValueX, y: currentY);
        selectableNodes.append(bgmVolumValueText);
        pageLayer.addChild(bgmVolumValueText);
        
        currentY -= gameReportItemGapY;
        
        // Add SE volumn setting
        let seText = MenuObjectFactory.createSeText();
        seText.position = CGPoint(x: GameContext.gameSettingCenterX, y: currentY);
        menuTextNodes.append(seText);
        pageLayer.addChild(seText);
        
        let seVolumValueText = MenuObjectFactory.createSeVolumnValueText();
        seVolumValueText.position = CGPoint(x: GameContext.gameSettingValueX, y: currentY);
        selectableNodes.append(seVolumValueText);
        pageLayer.addChild(seVolumValueText);
        
        currentY -= gameReportItemGapY;
        
        // Add arcade button position setting
        let arcadeButtonPosText = MenuObjectFactory.createArcadeButtonPosText();
        arcadeButtonPosText.position = CGPoint(x: GameContext.gameSettingCenterX, y: currentY);
        menuTextNodes.append(arcadeButtonPosText);
        pageLayer.addChild(arcadeButtonPosText);
        
        let buttonPosValueText = MenuObjectFactory.createButtonPosValueText();
        buttonPosValueText.position = CGPoint(x: GameContext.gameSettingValueX, y: currentY);
        selectableNodes.append(buttonPosValueText);
        pageLayer.addChild(buttonPosValueText);
        
        
        // Add Apply text
        let applyText = MenuObjectFactory.createApplyText();
        applyText.position = GameContext.actionOneTextPos;
        selectableNodes.append(applyText);
        pageLayer.addChild(applyText);
    }
}