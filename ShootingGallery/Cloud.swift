//
//  Cloud.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/13/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class Cloud : GameEntity, Damagable {
    
    let imageNames:[String] = ["cloud1", "cloud2"];
    
    override func generateTexture() -> SKTexture? {
        let imageIndex:Int = Int(Utils.getRandomInt(0, 2));
        return SKTexture(imageNamed: imageNames[imageIndex]);
    }
    
    override func configProperties(){
        self.name = "Cloud";
        self.zPosition = LayerDepth.ROW_SIX_BLOCKER.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5);  // Middle corner
    }
    
    func isHit(hitPosOnTarget:CGPoint) -> Bool {
        return false;
    }
    
    func hit(hitPosOnNode:CGPoint) -> Bool {
        return false;
    }
    
}