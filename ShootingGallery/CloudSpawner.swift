//
//  CloudSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/13/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class CloudSpawner:SingleObjectSpawner {
    
    var initPosDisplacementY:CGFloat = 0;
    
    let minLifeTime:UInt32 = 10;
    let maxLifeTime:UInt32 = 15;
    
    override func getTarget() -> SKNode {
        return EntityFactory.createCloud();
    }
    
    override func getTargetAction(startPos:CGPoint) -> SKAction {
        let targetLifeTime = NSTimeInterval(Utils.getRandomInt(minLifeTime, maxLifeTime));
        let moveAction = SKAction.moveBy(CGVector(dx: GameContext.gameContentSize.width+GameContext.cloudWidth, dy: 0), duration: targetLifeTime);
        return SKAction.sequence([moveAction, SKAction.removeFromParent()]);
    }
    
    override func configSpawner() {
        self.spawnCoolDownTime = 5;
        self.minInitObjectNum = UInt32(1);
        self.maxInitObjectNum = UInt32(3);
        self.initPosDisplacementY = GameContext.gameContentSize.height/10;
    }
    
    override func getTargetInitPos() -> CGPoint {
        let displacementRatio = (Utils.getRandomPercent() - 0.5) * 2;
        let yDisplacement:CGFloat = self.initPosDisplacementY * CGFloat(displacementRatio);
        return CGPoint(x: self.initPos.x - GameContext.cloudWidth/2, y:self.initPos.y + yDisplacement);
    }
}