//
//  AimingTarget.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/14/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit
class AimingTarget : TargetBody {
    let imageNames:[String] = ["target_colored_outline", "target_red1_outline", "target_red2_outline", "target_red3_outline"];
    
    override func generateTexture() -> SKTexture? {
        let imageIndex:Int = Int(Utils.getRandomInt(0, 4));
        return SKTexture(imageNamed: imageNames[imageIndex]);
    }
    
    override func configProperties(){
        self.name = "AimingTarget";
        self.zPosition = LayerDepth.ROW_TWO_TARGET_ADDON.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0.1); // Bottom-middle
        
        self.hitBoxX = 0;
        self.hitBoxY = 8/20;
        self.hitBoxRadius = 1/6;
    }
    
    override func getAction() -> SKAction {
        return SKAction.sequence([]);
    }
    
    override func getHitScoreValue() -> Int {
        return 100;
    }
    
    override func getMissScoreValue() -> Int {
        return 10;
    }
}
