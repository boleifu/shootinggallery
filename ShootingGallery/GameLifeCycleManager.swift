//
//  GameLifeCycleManager.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/12/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

/**
 * This class manage the lifecycle of the game
 */
class GameLifeCycleManager {
    
    weak var gameContentLayer:SKNode?;
    let hudManager:HudManager;
    
    private var envSpawners:[AutoObjectSpawner] = [];
    private var arcadeModeTargetSpawners:[AutoObjectSpawner] = [];
    private var speedModeTargetSpawners:[AutoObjectSpawner] = [];
    
    
    init(hudLayer:SKNode, gameContentLayer:SKNode) {
        self.hudManager = HudManager(hudLayer: hudLayer);
        self.gameContentLayer = gameContentLayer;
    }
    
    // ========== Game control ==========
    
    func setupGame() {
        // Init HUD
        hudManager.initHud();
        
        // Init Game Content
        initGameContent();
        
        // Setup object spawner
        setupEnvSpawner();
        setupArcadeModeTargetSpawner();
        setupSpeedModeTargetSpawner();
    }
    
    func resetGame(gameTime gameTime:Int, ammoNum:Int) {
        // Reset game state
        GameRuntimeContext.resetGameRuntimeContext(gameTime, ammoNum);
        
        // Reset HUD
        hudManager.resetHud();
        
        // Reset game content
        gameContentLayer!.removeAllChildren();
        initGameContent();
        
        // Reconfig env spawner
        if (GameRuntimeContext.gameMode == GameMode.SPEED_MODE) {
            // Blocker does not move in the speed mode
            reconfigEnvSpawner(true);
        } else {
            reconfigEnvSpawner(false);
        }
    }
    
    // ========== HUD Control ==========
    
    func refreshHud(currentTime: CFTimeInterval, crossHairPosOnHud:CGPoint) {
        hudManager.refresh(currentTime, crossHairPosOnHud: crossHairPosOnHud);
    }
    
    func checkTimeup(currentTime: CFTimeInterval) {
        let secRemains = Int(GameRuntimeContext.gameEndTime - currentTime);
        // Check timeup
        if (secRemains <= 0) {
            AudioUtils.stopBgmThemeOne();
            hudManager.showTimeupText();
            
            // End the game
            GameRuntimeContext.gameStarted = false;
        }
    }
    
    private func refillAmmo() {
        hudManager.ammoMeter!.refillAmmo();
    }
    
    private func removeAmmo() -> Bool {
        if (hasAmmo()) {
            hudManager.ammoMeter!.removeAmmo();
            return true;
        }
        return false;
    }
    
    private func hasAmmo() -> Bool {
        return hudManager.ammoMeter!.currentAmmoCount > 0;
    }
    
    // ========== Game Content control ==========
    
    func fillGameContents() {
        // Fill the screen
        for spawner in envSpawners {
            spawner.resetSpawnTimer();
            spawner.fillTheScreen();
        }
        
        if (GameRuntimeContext.gameMode == GameMode.ARCADE_MODE) {
            for spawner in arcadeModeTargetSpawners {
                spawner.resetSpawnTimer();
                spawner.fillTheScreen();
            }
        } else if (GameRuntimeContext.gameMode == GameMode.SPEED_MODE) {
            for spawner in speedModeTargetSpawners {
                spawner.resetSpawnTimer();
                spawner.fillTheScreen();
            }
        }
    }
    
    func spawnArcadeGameContents(currentTime:NSTimeInterval) {
        for spawner in envSpawners {
            spawner.spawnObjectByTime(currentTime);
        }
        
        for spawner in arcadeModeTargetSpawners {
            spawner.spawnObjectByTime(currentTime);
        }
    }
    
    func spawnSpeedGameContents(currentTime:NSTimeInterval) {
        for spawner in envSpawners {
            spawner.spawnObjectByTime(currentTime);
        }
        
        if (GameRuntimeContext.targetRemains != 0) {
            return;
        }
        
        if (GameRuntimeContext.targetLevel == 0) {
            //First time level change, start with 1
            GameRuntimeContext.targetLevel = 1;
        } else {
            // Clear targets from screen
            clearShootingTargets();
            
            // Add more game time
            GameRuntimeContext.gameEndTime += NSTimeInterval(max(1, GameRuntimeContext.targetLevel - 1));
            
            // No more targets, change level
            GameRuntimeContext.targetLevel = Int(Utils.getRandomInt(1, UInt32(GameContext.speedModeMaxLevel)));
            print("Speed mode level changed -> level \(GameRuntimeContext.targetLevel)");
            
            // Increase the multiplier
            GameRuntimeContext.currentScoreMultiplier += 1;
            
            // Update max multiplier
            GameRuntimeContext.maxScoreMultiplier = max(GameRuntimeContext.currentScoreMultiplier, GameRuntimeContext.maxScoreMultiplier);
        }
        
        // Spawn new targets
        let targetNeeded:Int = GameRuntimeContext.targetLevel;
        let minTargetNum:Int = max(0, targetNeeded - GameContext.speedModeTargetSlotNum);
        let maxTargetNum:Int = min(targetNeeded, GameContext.speedModeTargetSlotNum);
        let rowOneTargetNum:Int = Int(Utils.getRandomInt(UInt32(minTargetNum), UInt32(maxTargetNum)));
        let rowTwoTargetNum:Int = targetNeeded - rowOneTargetNum;
        
        var spawnedTargetsCount:Int = 0;
        
        if (speedModeTargetSpawners.count >= 2) {
            // Spawing row one target
            let result1:Int = speedModeTargetSpawners[0].spawnObjectByNumber(rowOneTargetNum);
            print("Row one target spawned. Desired = \(rowOneTargetNum), actual = \(result1)");
            spawnedTargetsCount += result1;
            
            // Spawing row two target
            let result2:Int = speedModeTargetSpawners[1].spawnObjectByNumber(rowTwoTargetNum);
            print("Row one target spawned. Desired = \(rowTwoTargetNum), actual = \(result2)");
            spawnedTargetsCount += result2;
        } else {
            print("Error: Too few speed mode target spawner [\(speedModeTargetSpawners.count)]");
        }
        
        print("\(spawnedTargetsCount) target spawned for level \(GameRuntimeContext.targetLevel)");
        GameRuntimeContext.targetRemains = spawnedTargetsCount;
    }
    
    func clearShootingTargets() {
        for node in gameContentLayer!.children {
            let shootingTarget:ShootingTarget? = node as? ShootingTarget;
            if (shootingTarget != nil) {
                shootingTarget?.removeAllChildren();
                shootingTarget?.removeFromParent();
            }
        }
    }
    
    func clearGameContents() {
        gameContentLayer!.removeAllChildren();
    }
    
    func processRifleFire(currentTime:NSTimeInterval, aimedPosOnGameContent:CGPoint) {
        // Remove bullet
        if (!removeAmmo()) {
            print("No Ammo!!!");
            AudioUtils.playNoAmmoSound();
        } else {
            // Reload the ammo if the last bullet shot
            if (!hasAmmo() && !GameRuntimeContext.isReloading) {
                refillAmmo();
                AudioUtils.playReloadSound();
            }
            
            // Process the bullet shoot
            print("Fire!!! \(currentTime)");
            
            // Check if any target is shot, only shot the most top one
            let aimedNodes = gameContentLayer!.nodesAtPoint(aimedPosOnGameContent);
            print("Hit \(aimedNodes.count) nodes.");
            let damagableNodeOnTop:SKNode? = getDamagableNodeOnTop(aimedNodes);
            if (damagableNodeOnTop != nil) {
                // Hit something
                let posOnNode = damagableNodeOnTop!.convertPoint(aimedPosOnGameContent, fromNode: gameContentLayer!);
                print("\(damagableNodeOnTop?.name) is shot.\nPos on GameContentLayer = \(aimedPosOnGameContent).\nPos on node = \(posOnNode)");
                
                // Add a bullet hole on that object
                let bulletHole = EntityFactory.createBulletHole();
                bulletHole.position = posOnNode;
                damagableNodeOnTop!.addChild(bulletHole);
                
                var hitTarget = false;
                let targetNode = damagableNodeOnTop as? TargetObject;
                if (targetNode != nil) {
                    // Process the bullet hit
                    hitTarget = targetNode!.hit(posOnNode);
                }
                
                // Play sound effect
                if (hitTarget) {
                    AudioUtils.playHidMetalSound();
                } else {
                    AudioUtils.playHitWoodSound();
                }
            } else {
                print("Does not hit any damagable object.")
            }
        }
    }
    
    // ========== Helper function ==========
    
    private func initGameContent() {
        // Init Game Content
        let background = BackBoard(position: CGPoint(x: GameContext.gameContentSize.width/2, y: GameContext.gameContentSize.height/2), face:0, targetWidth: GameContext.gameContentSize.width, targetHeight:nil);
        gameContentLayer!.addChild(background);
    }
    
    private func setupEnvSpawner() {
        let rowOneWaveSpawner = FrontRowWaveSpawner(gameContentLayer: gameContentLayer, initPos: GameContext.frontWaveSpawnerPos);
        envSpawners.append(rowOneWaveSpawner);
        let rowTwoWaveSpawner = BackRowWaveSpawner(gameContentLayer: gameContentLayer, initPos: GameContext.rowTwoWaveSpanerPos);
        envSpawners.append(rowTwoWaveSpawner);
        let rowOneGrassSpawner = FrontRowGrassSpawner(gameContentLayer: gameContentLayer, initPos: GameContext.frontGrassSpawnerPos);
        envSpawners.append(rowOneGrassSpawner);
        //let rowTwoGrassSpawner = BackRowGrassSpawner(gameContentLayer: gameContentLayer, initPos: GameContext.rowTwoGrassSpanerPos);
        let cloudSpawner = CloudSpawner(gameContentLayer: gameContentLayer, initPos: GameContext.cloudSpawnerPos);
        envSpawners.append(cloudSpawner);
    }
    
    private func setupArcadeModeTargetSpawner() {
        // Init all spawners
        let duckTargetSpawner = DuckTargetSpawner(gameContentLayer: gameContentLayer, initPos: GameContext.frontTargetSpawnerPos);
        arcadeModeTargetSpawners.append(duckTargetSpawner);
        //objectSpawners.append(rowTwoGrassSpawner);
        //let aimingTargetSpawner = AimingTargetSpawner(gameContentLayer: gameContentLayer, initPos: GameContext.rowTwoTargetSpawnerPos);
        //objectSpawners.append(aimingTargetSpawner);
        let rowTowduckTargetSpawner = RowTwoDuckTargetSpawner(gameContentLayer: gameContentLayer, initPos: GameContext.rowTwoTargetSpawnerPos);
        arcadeModeTargetSpawners.append(rowTowduckTargetSpawner);
    }
    
    private func setupSpeedModeTargetSpawner() {
        // Init all spawners
        let duckTargetSpawner = SpeedModeDuckTargetSpawner(gameContentLayer: gameContentLayer, initPos: GameContext.frontTargetSpawnerPos);
        speedModeTargetSpawners.append(duckTargetSpawner);
        let rowTowduckTargetSpawner = RowTwoSpeedModeDuckTargetSpawner(gameContentLayer: gameContentLayer, initPos: GameContext.rowTwoTargetSpawnerPos);
        speedModeTargetSpawners.append(rowTowduckTargetSpawner);
    }
    
    private func reconfigEnvSpawner(isBlockerSpawnerStatic:Bool) {
        for envSpawner in envSpawners {
            let blockerSpawner:BlockerSpawner? = envSpawner as? BlockerSpawner;
            if (blockerSpawner != nil) {
                blockerSpawner?.isStatic = isBlockerSpawnerStatic;
            }
        }
    }
    
    private func getDamagableNodeOnTop(nodes:[SKNode]) -> SKNode? {
        var topNode:SKNode? = nil;
        var currentMaxIndex:CGFloat = -1;
        for node in nodes {
            let damagableNode = node as? Damagable;
            if (damagableNode != nil) {
                var depth = node.zPosition;
                let parent:SKNode? = node.parent;
                if (parent != nil) {
                    depth += parent!.zPosition;
                }
                if (depth > currentMaxIndex) {
                    topNode = node;
                    currentMaxIndex = depth;
                }
            }
        }
        return topNode;
    }
}
