//
//  MenuManager.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/18/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

/**
 * This class manager the lifecycle of the menu pages
 */
class MenuManager {
    weak var menuLayer:SKNode?;
    
    let menuBackgroundLayer = SKNode();
    
    // Properties
    var menuEnabled:Bool = true;
    var currentMenuPage:MenuPage = MenuPage.UNKNOWN;
    
    // Menu objects
    var leftCurtain:Curtain? = nil;
    var rightCurtain:Curtain? = nil;
    var duckTarget:ShootingTarget? = nil;
    
    var menuPageManagers:[MenuPage:MenuPageManager] = [:];
    
    
    init(menuLayer:SKNode) {
        self.menuLayer = menuLayer;
        menuLayer.addChild(menuBackgroundLayer);
    }
    
    deinit {
        print("MenuManager released.")
    }
    
    func initMenu() {
        // Add left curtain
        leftCurtain = MenuObjectFactory.createCurtain();
        leftCurtain!.position = CGPoint(x: 0, y: GameContext.screenSize.height);
        menuBackgroundLayer.addChild(leftCurtain!);
        
        // Add right curtain
        rightCurtain = MenuObjectFactory.createCurtain();
        rightCurtain!.anchorPoint = CGPoint(x: 1, y: 1);
        rightCurtain!.position = CGPoint(x: GameContext.screenSize.width, y: GameContext.screenSize.height);
        menuBackgroundLayer.addChild(rightCurtain!);
        
        // Add a duck target
        resetDuckTarget();
        
        // Setup welcome page
        let welcomePageManager = WelcomePageManager(menuLayer:menuLayer!);
        menuPageManagers[welcomePageManager.pageType] = welcomePageManager;
        
        // Setup game prep page
        let gamePrepPageManager = GamePrepPageManager(menuLayer:menuLayer!);
        menuPageManagers[gamePrepPageManager.pageType] = gamePrepPageManager;
        
        // Setup game over page
        let gameOverPageManager = GameOverPageManager(menuLayer:menuLayer!);
        menuPageManagers[gameOverPageManager.pageType] = gameOverPageManager;
        
        // Setup setting page
        let settingPageManager = GameSettingPageManager(menuLayer:menuLayer!);
        menuPageManagers[settingPageManager.pageType] = settingPageManager;
        
        // Setup game credits page
        let creditPageManager = GameCreditsPageManager(menuLayer:menuLayer!);
        menuPageManagers[creditPageManager.pageType] = creditPageManager;
        
        // Play GBM
        AudioUtils.playMenuBgm();
        
        // Goto the welcome page
        currentMenuPage = MenuPage.WELCOME_PAGE;
        menuPageManagers[MenuPage.WELCOME_PAGE]?.enablePage();
    }
    
    func enableMenu() {
        if (menuEnabled) {
            return;
        }
        
        // Enable menu
        leftCurtain?.closeCurtain();
        rightCurtain?.closeCurtain();

        resetDuckTarget();
        
        // Enable page
        currentMenuPage = GameRuntimeContext.menuPage;
        menuPageManagers[currentMenuPage]?.enablePage();
        
        AudioUtils.playMenuBgm();
        menuEnabled = true;
    }
    
    func disableMenu() {
        if (!menuEnabled) {
            return;
        }
        
        // Disable menu
        leftCurtain?.openCurtain();
        rightCurtain?.openCurtain();
        
        duckTarget?.hidden = true;
        
        // Disable page
        menuPageManagers[currentMenuPage]?.disablePage();
        
        AudioUtils.stopMenuBgm();
        menuEnabled = false;
    }
    
    func updateMenu(aimingPosOnMenu:CGPoint, selected:Bool) {
        // Only process if menu is enabled
        if (!menuEnabled) {
            return;
        }
        
        // Go to next page if required
        if (currentMenuPage != GameRuntimeContext.menuPage) {
            menuPageManagers[currentMenuPage]?.disablePage();
            menuPageManagers[GameRuntimeContext.menuPage]?.enablePage();
            print("Menu page changed \(currentMenuPage) -> \(GameRuntimeContext.menuPage)");
            currentMenuPage = GameRuntimeContext.menuPage;
            
            resetDuckTarget();
            return;
        }
        
        let currentPageManager:MenuPageManager? = menuPageManagers[currentMenuPage];
        if (currentPageManager == nil) {
            print("Error: Cannot find the page manager for the page \(currentMenuPage)");
            return;
        }
        
        // Un-hilight all nodes
        for selectableNode in currentPageManager!.selectableNodes {
            selectableNode.unHilight();
        }
        
        // Get the top node
        let topNode = getPointedNodeOnTop(aimingPosOnMenu);
        
        // Hilight if selected node
        let selectedNode = topNode as? Selectable;
        if (selectedNode != nil) {
            selectedNode?.hilight();
        }
        
        // Fire button pressed
        if (selected) {
            // Process the selected node
            if (selectedNode != nil) {
                selectedNode?.select();
            }
            
            // Hit something
            if (topNode != nil) {
                let aimingPosInNode = topNode!.convertPoint(aimingPosOnMenu, fromNode: menuLayer!);
                let bulletHole = EntityFactory.createBulletHole();
                bulletHole.position = aimingPosInNode;
                topNode?.addChild(bulletHole);
                
                // Check if hit the duck target
                let targetObjectOnTop = topNode as? TargetObject;
                if (targetObjectOnTop != nil) {
                    // Play sound effect
                    if (targetObjectOnTop!.hit(aimingPosInNode)) {
                        AudioUtils.playHidMetalSound();
                    } else {
                        AudioUtils.playHitWoodSound();
                    }
                } else {
                    // Does not shoot the duck target
                    AudioUtils.playHitWoodSound();
                }
            }
        }
        
        // Run after update behavior
        currentPageManager?.afterUpdate();
    }
    
    
    // ======== Helper function ========
    private func resetDuckTarget() {
        // Remove the existing one
        if (duckTarget != nil) {
            duckTarget?.removeAllChildren();
            duckTarget?.removeFromParent();
            duckTarget = nil;
        }
        
        // Create new one
        duckTarget = EntityFactory.createDuckTarget();
        duckTarget?.position = CGPoint(x: -duckTarget!.base.size.width, y: GameContext.screenSize.height/4);
        duckTarget?.base.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        duckTarget?.zRotation = -CGFloat(M_PI_4);
        duckTarget?.runAction(SKAction.repeatActionForever(GameConstants.SMALL_SHAKE));
        menuBackgroundLayer.addChild(duckTarget!);
    }
    
    
    private func getPointedNodeOnTop(aimingPosOnMenu:CGPoint) -> SKNode? {
        let hilightedNodes = menuLayer!.nodesAtPoint(aimingPosOnMenu);
        var currentMaxIndex:CGFloat = -1;
        var topNode:SKNode? = nil;
        
        for hilightedNode in hilightedNodes {
            let selectableNode = hilightedNode as? Selectable;
            if (selectableNode != nil) {
                // Selectable node has highest priority
                topNode = hilightedNode;
                break;
            } else {
                // For non-selectable node, get the top one
                var depth = hilightedNode.zPosition;
                let parent:SKNode? = hilightedNode.parent;
                if (parent != nil) {
                    depth += parent!.zPosition;
                }
                if (depth > currentMaxIndex) {
                    topNode = hilightedNode;
                    currentMaxIndex = depth;
                }
            }
        }
        
        return topNode;
    }
    
}