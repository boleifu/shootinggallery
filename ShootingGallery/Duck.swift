//
//  DuckTarget.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/6/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit
class Duck : TargetBody {
    
    let duckImageNames:[String] = ["duck_outline_target_white", "duck_outline_target_yellow", "duck_outline_target_brown"];
    
    override func generateTexture() -> SKTexture? {
        let imageIndex:Int = Int(Utils.getRandomInt(0, 3));
        return SKTexture(imageNamed: duckImageNames[imageIndex]);
    }
    
    override func configProperties(){
        self.name = "Duck";
        self.zPosition = LayerDepth.ROW_ONE_TARGET_ADDON.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0.1); // Bottom-middle
        
        self.hitBoxX = -1/20;
        self.hitBoxY = 4/20;
        self.hitBoxRadius = 1/6;
    }
    
    override func getAction() -> SKAction {
        return SKAction.sequence([]);
    }
    
    override func getHitScoreValue() -> Int {
        return 100;
    }
    
    override func getMissScoreValue() -> Int {
        return 10;
    }
    
}
