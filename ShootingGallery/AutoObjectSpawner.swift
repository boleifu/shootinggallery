//
//  TargetSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/6/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class AutoObjectSpawner {
    
    weak var gameContentLayer:SKNode?;  // Where the target add to
    var initPos:CGPoint = CGPoint.zero;
    
    // Internal properties
    var spawnCoolDownTime:CFTimeInterval = 2;
    var lastSpawnTime:CFTimeInterval = 0;
    
    init(gameContentLayer:SKNode?, initPos:CGPoint) {
        self.gameContentLayer = gameContentLayer;
        self.initPos = initPos;
        
        configSpawner();
    }
    
    deinit {
        print("AutoObjectSpawner released.")
    }
    
    func resetSpawnTimer() {
        lastSpawnTime = 0;
    }
    
    func configSpawner() {
        // No default behavior
    }

    /**
     * Genrate a object to spawn
     */
    func getTarget() -> SKNode {
        // Not implemented
        fatalError("getTarget() has not been implemented");
    }
    
    /**
     * Get the embadded action of the spawned object
     */
    func getTargetAction(startPos:CGPoint) -> SKAction {
        // Not implemented
        fatalError("getTargetAction() has not been implemented");
    }
    
    /**
     * Get the initial pos of spawned object
     * Currently only used by the default spawnObjectByTime method
     */
    func getTargetInitPos() -> CGPoint {
        return initPos;
    }
    
    // ==== Spawn objects ====
    
    /**
     * Spawn objects to fill the screen
     */
    func fillTheScreen() {
        // No default behavior
    }
    
    /**
     * Spawn object if the cool down finished
     * @param currentTime The current game time
     */
    func spawnObjectByTime(currentTime: CFTimeInterval) {
        if (currentTime - lastSpawnTime > spawnCoolDownTime) {
            if (gameContentLayer == nil) {
                print("Error: gameContentLayer is nil.");
                return;
            }
            let target = getTarget();
            target.position = getTargetInitPos();
            gameContentLayer!.addChild(target);
            target.runAction(getTargetAction(self.initPos));
            
            lastSpawnTime = currentTime;
        }
    }
    
    /**
     * Spawn the given amount of objects immediately
     * @param targetNum The number of objects needed
     */
    func spawnObjectByNumber(targetNum: Int) -> Int {
        // No default behavior
        return 0;
    }
}
