//
//  DuckTargetSign.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/26/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class DuckTargetSign : GameEntity {
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "duck_target_sign");
    }
    
    override func configProperties(){
        self.name = "Duck Target Sign";
        self.zPosition = LayerDepth.MENU_IMAGE_ADD_ON.getDepth();
        self.anchorPoint = CGPoint(x: 1, y: 0.5); // Left-middle
        self.alpha = 0.8;
    }
}