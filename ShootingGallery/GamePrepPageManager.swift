//
//  GamePrepPageManager.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/20/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GamePrepPageManager : MenuPageManager {
    
    let arcadeModeLayer = SKNode();
    let speedModeLayer = SKNode();
    let onlineBattleLayer = SKNode();
    var duckTargetSign:DuckTargetSign? = nil;
    
    var arcadeModeText:ArcadeModeText? = nil;
    var speedModeText:SpeedModeText? = nil;
    var onlineBattleText:OnlineBattleText? = nil;
    
    override func setupPage() {
        self.pageType = MenuPage.GAME_PREP_PAGE;
        
        // Add Page Title
        let titleText = MenuObjectFactory.createGamePrepPageTitle();
        titleText.position = GameContext.pageTitlePos;
        menuTextNodes.append(titleText);
        pageLayer.addChild(titleText);
        
        // Add panel
        let panel = MenuObjectFactory.createPanel();
        panel.position = GameContext.panelPos;
        pageLayer.addChild(panel);
        
        // Add duck target sign
        duckTargetSign = MenuObjectFactory.createDuckTargetSign();
        duckTargetSign!.position = GameContext.duckTargetSignPos;
        panel.addChild(duckTargetSign!);
        duckTargetSign!.hidden = true;
        
        // Add desc layers
        panel.addChild(arcadeModeLayer);
        panel.addChild(speedModeLayer);
        panel.addChild(onlineBattleLayer);
        
        let lineGap:CGFloat = GameContext.gameModeDescTextSize;
        
        // Add ArcadeMode desc
        let arcadeModeDesc1Text:String = NSLocalizedString("ARCADE_MODE_DESC_1", comment: "arcadeModeDesc1Text");
        let arcadeModeDesc2Text:String = NSLocalizedString("ARCADE_MODE_DESC_2", comment: "arcadeModeDesc2Text");
        
        let arcadeModeDesc1 = MenuObjectFactory.createGameModeDescText(arcadeModeDesc1Text);
        arcadeModeDesc1.position = CGPoint(x: GameContext.gameModeDescPosX, y: GameContext.gameModeDescStartY);
        arcadeModeLayer.addChild(arcadeModeDesc1);
        
        let arcadeModeDesc2 = MenuObjectFactory.createGameModeDescText(arcadeModeDesc2Text);
        arcadeModeDesc2.position = CGPoint(x: GameContext.gameModeDescPosX, y: GameContext.gameModeDescStartY-lineGap);
        arcadeModeLayer.addChild(arcadeModeDesc2);
        
        
        // Add ArcadeMode details
        let arcadeModeDetail1Text:String = NSLocalizedString("ARCADE_MODE_DETAIL_1", comment: "arcadeModeDetail1Text");
        let arcadeModeDetail2Text:String = NSLocalizedString("ARCADE_MODE_DETAIL_2", comment: "arcadeModeDetail2Text");
        let arcadeModeDetail3Text:String = NSLocalizedString("ARCADE_MODE_DETAIL_3", comment: "arcadeModeDetail3Text");
        let arcadeModeDetail4Text:String = NSLocalizedString("ARCADE_MODE_DETAIL_4", comment: "arcadeModeDetail4Text");
        
        let arcadeModeDetail1 = MenuObjectFactory.createGameModeDescText(arcadeModeDetail1Text);
        arcadeModeDetail1.position = CGPoint(x: GameContext.gameModeDescPosX, y: GameContext.gameModeDetailStartY);
        arcadeModeLayer.addChild(arcadeModeDetail1);
        
        let arcadeModeDetail2 = MenuObjectFactory.createGameModeDescText(arcadeModeDetail2Text);
        arcadeModeDetail2.position = CGPoint(x: GameContext.gameModeDescPosX, y: arcadeModeDetail1.position.y-lineGap);
        arcadeModeLayer.addChild(arcadeModeDetail2);
        
        let arcadeModeDetail3 = MenuObjectFactory.createGameModeDescText(arcadeModeDetail3Text);
        arcadeModeDetail3.position = CGPoint(x: GameContext.gameModeDescPosX, y: arcadeModeDetail2.position.y-lineGap);
        arcadeModeLayer.addChild(arcadeModeDetail3);
        
        let arcadeModeDetail4 = MenuObjectFactory.createGameModeDescText(arcadeModeDetail4Text);
        arcadeModeDetail4.position = CGPoint(x: GameContext.gameModeDescPosX, y: arcadeModeDetail3.position.y-lineGap);
        arcadeModeLayer.addChild(arcadeModeDetail4);
        
        // Add speed mode desc
        let speedModeDesc1Text:String = NSLocalizedString("SPEED_MODE_DESC_1", comment: "speedModeDesc1Text");
        let speedModeDesc2Text:String = NSLocalizedString("SPEED_MODE_DESC_2", comment: "speedModeDesc2Text");
        
        let speedModeDesc1 = MenuObjectFactory.createGameModeDescText(speedModeDesc1Text);
        speedModeDesc1.position = CGPoint(x: GameContext.gameModeDescPosX, y: GameContext.gameModeDescStartY);
        speedModeLayer.addChild(speedModeDesc1);
        
        let speedModeDesc2 = MenuObjectFactory.createGameModeDescText(speedModeDesc2Text);
        speedModeDesc2.position = CGPoint(x: GameContext.gameModeDescPosX, y: GameContext.gameModeDescStartY-lineGap);
        speedModeLayer.addChild(speedModeDesc2);
        
        // Add speed mode details
        let speedModeDetail1Text:String = NSLocalizedString("SPEED_MODE_DETAIL_1", comment: "speedModeDetail1Text");
        let speedModeDetail2Text:String = NSLocalizedString("SPEED_MODE_DETAIL_2", comment: "speedModeDetail2Text");
        let speedModeDetail3Text:String = NSLocalizedString("SPEED_MODE_DETAIL_3", comment: "speedModeDetail3Text");
        let speedModeDetail4Text:String = NSLocalizedString("SPEED_MODE_DETAIL_4", comment: "speedModeDetail4Text");
        
        let speedModeDetail1 = MenuObjectFactory.createGameModeDescText(speedModeDetail1Text);
        speedModeDetail1.position = CGPoint(x: GameContext.gameModeDescPosX, y: GameContext.gameModeDetailStartY);
        speedModeLayer.addChild(speedModeDetail1);
        
        let speedModeDetail2 = MenuObjectFactory.createGameModeDescText(speedModeDetail2Text);
        speedModeDetail2.position = CGPoint(x: GameContext.gameModeDescPosX, y: speedModeDetail1.position.y-lineGap);
        speedModeLayer.addChild(speedModeDetail2);
        
        let speedModeDetail3 = MenuObjectFactory.createGameModeDescText(speedModeDetail3Text);
        speedModeDetail3.position = CGPoint(x: GameContext.gameModeDescPosX, y: speedModeDetail2.position.y-lineGap);
        speedModeLayer.addChild(speedModeDetail3);
        
        let speedModeDetail4 = MenuObjectFactory.createGameModeDescText(speedModeDetail4Text);
        speedModeDetail4.position = CGPoint(x: GameContext.gameModeDescPosX, y: speedModeDetail3.position.y-lineGap);
        speedModeLayer.addChild(speedModeDetail4);
        
        // Add online battle desc
        let commingSoonText = MenuObjectFactory.createCommingSoonText();
        commingSoonText.position = CGPoint(x: panel.size.width/2, y: 0);
        commingSoonText.fontSize = GameContext.commingSoonTextSize;
        onlineBattleLayer.addChild(commingSoonText);
        
        // Hide all desc
        arcadeModeLayer.hidden = true;
        speedModeLayer.hidden = true;
        onlineBattleLayer.hidden = true;
        
        // Add game mode options
        let gameModeOptionPosY = (GameContext.menuContentStartY - GameContext.menuContentEndY)/3;
        var currentY = GameContext.menuContentStartY;
        
        // Add Arcade mode option
        arcadeModeText = MenuObjectFactory.createArcadeModeText();
        arcadeModeText!.position = CGPoint(x: GameContext.gameModePosX, y: currentY);
        selectableNodes.append(arcadeModeText!);
        pageLayer.addChild(arcadeModeText!);
        
        currentY -= gameModeOptionPosY;
        
        // Add Speed mode option
        speedModeText = MenuObjectFactory.createSpeedModeText();
        speedModeText!.position = CGPoint(x: GameContext.gameModePosX, y: currentY);
        selectableNodes.append(speedModeText!);
        pageLayer.addChild(speedModeText!);
        
        currentY -= gameModeOptionPosY;
        
        // Add Arcade mode option
        onlineBattleText = MenuObjectFactory.createOnlineBattleText();
        onlineBattleText!.position = CGPoint(x: GameContext.gameModePosX, y: currentY);
        selectableNodes.append(onlineBattleText!);
        pageLayer.addChild(onlineBattleText!);
        
        currentY -= gameModeOptionPosY;
        
        // Add setting text
        let settingText = MenuObjectFactory.createSettingText();
        settingText.position = GameContext.settingTextPos;
        selectableNodes.append(settingText);
        pageLayer.addChild(settingText);
        
        // Add Back text
        let backText = MenuObjectFactory.createBackText(MenuPage.WELCOME_PAGE);
        backText.position = GameContext.actionOneTextPos;
        selectableNodes.append(backText);
        pageLayer.addChild(backText);
    }
    
    override func afterUpdate() {
        arcadeModeLayer.hidden = !arcadeModeText!.isHilighted;
        speedModeLayer.hidden = !speedModeText!.isHilighted;
        onlineBattleLayer.hidden = !onlineBattleText!.isHilighted;
        duckTargetSign!.hidden = arcadeModeLayer.hidden && speedModeLayer.hidden;
    }
}