//
//  AimingTargetSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/14/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class AimingTargetSpawner:SingleObjectSpawner {
    
    let minAppearTime:UInt32 = 3;
    let maxAppearTime:UInt32 = 6;
    let minHideTime:UInt32 = 1;
    let maxHideTime:UInt32 = 3;
    
    
    override func getTarget() -> ShootingTarget {
        return EntityFactory.createAimingTarget();
    }
    
    override func getTargetAction(startPos:CGPoint) -> SKAction {
        let targetLifeTime:NSTimeInterval = 20;
        let appearTime = NSTimeInterval(Utils.getRandomInt(minAppearTime, maxAppearTime));
        let hideTime = NSTimeInterval(Utils.getRandomInt(minHideTime, maxHideTime));
        
        //let rotateAngle:CGFloat = CGFloat(M_PI_4);
        //let rotateAction = SKAction.sequence([SKAction.rotateByAngle(rotateAngle, duration: 1), SKAction.rotateByAngle(-rotateAngle*2, duration: 2),SKAction.rotateByAngle(rotateAngle, duration: 1)]);
        let sneakAction = ActionFactory.getSneakAction(appearTime: appearTime, hideTime: hideTime);
        let moveAction = SKAction.moveBy(CGVector(dx: GameContext.gameContentSize.width, dy: 0), duration: targetLifeTime);
        //let finalAction = SKAction.group([SKAction.sequence([moveAction, SKAction.removeFromParent()]),
        //    SKAction.repeatActionForever(rotateAction)]);
        return SKAction.group([SKAction.sequence([moveAction, SKAction.removeFromParent()]), sneakAction]);
    }
    
    override func configSpawner() {
        self.minInitObjectNum = 1;
        self.maxInitObjectNum = 3;
        self.spawnCoolDownTime = 5;
    }
}