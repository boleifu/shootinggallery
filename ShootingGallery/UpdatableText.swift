//
//  UpdatableText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class UpdatableText : MenuText, Updatable {
    
    override func getText() -> String? {
        return GameTextConstants.UNKNON_TEXT;
    }
    
    func update() {
        fatalError("update() has not been implemented");
    }
}
