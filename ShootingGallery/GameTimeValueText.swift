//
//  GameTimeValueText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class GameTimeValueText : UpdatableText {
    
    override func getFontName() -> String? {
        return GameTextConstants.SUB_TITLE_FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Game Time Value Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Left;
        self.verticalAlignmentMode = .Center;
    }
    
    override func update() {
        let highestText:String = NSLocalizedString("HIGHEST_TEXT", comment: "highestText");
        let newLongestText:String = NSLocalizedString("NEW_LONGEST_GAME_TIME", comment: "GameTimeValueText");
        let secondText:String = NSLocalizedString("SECOND_TEXT", comment: "secondText");
        
        var longestGameTime:Int = 0;
        if (GameRuntimeContext.prevGameMode == GameMode.ARCADE_MODE) {
            longestGameTime = UserDataUtils.getArcadeModeLongestGameTime();
        } else if (GameRuntimeContext.prevGameMode == GameMode.SPEED_MODE) {
            longestGameTime = UserDataUtils.getSpeedModeLongestGameTime();
        }

        let currentGameTime = Int(GameRuntimeContext.gameEndTime - GameRuntimeContext.gameStartTime);
        
        if (currentGameTime > longestGameTime) {
            GameRuntimeContext.breakTheRecord = true;
            
            if (GameRuntimeContext.prevGameMode == GameMode.ARCADE_MODE) {
                UserDataUtils.setArcadeModeLongestGameTime(currentGameTime);
            } else if (GameRuntimeContext.prevGameMode == GameMode.SPEED_MODE) {
                UserDataUtils.setSpeedModeLongestGameTime(currentGameTime);
            }
            
            self.text = String("\(currentGameTime)\(secondText) (\(newLongestText))");
            //self.text = String("\(currentGameTime)\(GameTextConstants.SECOND_TEXT) (\(GameTextConstants.NEW_LONGEST_GAME_TIME))");
        } else {
            self.text = String("\(currentGameTime)\(secondText) (\(highestText) \(longestGameTime)\(secondText))");
            //self.text = String("\(currentGameTime)\(GameTextConstants.SECOND_TEXT) (\(GameTextConstants.HIGHEST_TEXT) \(longestGameTime)\(GameTextConstants.SECOND_TEXT))");
        }
    }
}