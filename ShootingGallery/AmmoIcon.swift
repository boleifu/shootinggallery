//
//  AmmoIcon.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/12/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class AmmoIcon : GameEntity {
    
    var isEmtpy:Bool = false;
    
    override func generateTexture() -> SKTexture? {
        return getFilledTexture();
    }
    
    override func configProperties(){
        self.name = "AmmoIcon";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: 1, y: 1);  // Top-right
    }
    
    func empty() {
        self.texture = getEmptyTexture();
        isEmtpy = true;
    }
    
    func refill() {
        self.texture = getFilledTexture();
        isEmtpy = false;
    }
    
    func getFilledTexture() -> SKTexture {
        fatalError("getFilledTexture() has not been implemented");
    }
    
    func getEmptyTexture() -> SKTexture {
        fatalError("getEmptyTexture() has not been implemented");
    }
}