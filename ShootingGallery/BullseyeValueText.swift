//
//  BullseyeValueText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class BullseyeValueText : UpdatableText {
    
    override func getFontName() -> String? {
        return GameTextConstants.SUB_TITLE_FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Bullseye Value Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Left;
        self.verticalAlignmentMode = .Center;
    }
    
    override func update() {
        var bullseyePercentage:Float = 0;
        if (GameRuntimeContext.totalShotNumber != 0) {
            bullseyePercentage = (Float(GameRuntimeContext.bullseyeHit)/Float(GameRuntimeContext.totalShotNumber))*100;
        }
        let outputString = String(format: "%d/%d (%.2f%%)",
                                  GameRuntimeContext.bullseyeHit,
                                  GameRuntimeContext.totalShotNumber,
                                  bullseyePercentage);
        self.text = outputString;
    }
}