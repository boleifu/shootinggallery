//
//  Tree.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/16/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class Tree : GameEntity, Damagable {
    
    let imageNames:[String] = ["tree_oak", "tree_pine"];
    
    override func generateTexture() -> SKTexture? {
        let imageIndex:Int = Int(Utils.getRandomInt(0, 2));
        return SKTexture(imageNamed: imageNames[imageIndex]);
    }
    
    override func configProperties(){
        self.name = "Tree";
        self.zPosition = LayerDepth.ROW_FIVE_BLOCKER.getDepth();
        self.anchorPoint = CGPoint(x: 0.5, y: 0);  // Middle bottom
    }
    
    func isHit(hitPosOnTarget:CGPoint) -> Bool {
        return false;
    }
    
    func hit(hitPosOnNode:CGPoint) -> Bool {
        return false;
    }
    
}
