//
//  DevelopedByText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/29/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class DevelopedByText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("DEVELOPED_BY_TEXT", comment: "DevelopedByText");
        //return GameTextConstants.DEVELOPED_BY_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Developed By Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
    }
}