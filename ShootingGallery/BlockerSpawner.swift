//
//  WaveSpawner.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/6/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class BlockerSpawner:AutoObjectSpawner {
    
    var isStatic:Bool;  // Whether the block will move
    var objectLiftTime:NSTimeInterval = 30;  // 30 sec
    
    var objectWidth:CGFloat = 0;
    var overlapWidth:CGFloat = 0;
    var objectGapWidth:CGFloat = 0;
    var movingLength:CGFloat = 0;
    
    init(gameContentLayer:SKNode?, initPos:CGPoint, isStatis:Bool = false) {
        self.isStatic = isStatis;
        super.init(gameContentLayer: gameContentLayer, initPos: initPos);
    }
    
    override func getTarget() -> SKSpriteNode {
        fatalError("getTarget() has not been implemented");
    }
    
    override func getTargetAction(startPos:CGPoint) -> SKAction {
        if (!isStatic) {
            let actualMovingLength = movingLength-(startPos.x-initPos.x);
            let actualLifeTime = NSTimeInterval(CGFloat(objectLiftTime) / movingLength * actualMovingLength);
            let moveAction = SKAction.moveBy(CGVector(dx: actualMovingLength, dy: 0), duration: actualLifeTime);
            return SKAction.sequence([moveAction, SKAction.removeFromParent()]);
        } else {
            // No action if static
            return SKAction();
        }
    }
    
    override func fillTheScreen() {
        let initWaveNum = Int(movingLength / objectGapWidth);
        for index in 1...initWaveNum {
            let wavePosX:CGFloat = objectGapWidth * CGFloat(index);
            let waveObject = getTarget();
            waveObject.position = CGPoint(x: wavePosX, y: self.initPos.y);
            self.gameContentLayer?.addChild(waveObject);
            waveObject.runAction(getTargetAction(waveObject.position));
        }
    }
    
    override func configSpawner() {
        let sampleObject = getTarget();
        objectWidth = sampleObject.size.width;
        overlapWidth = objectWidth/200;
        objectGapWidth = objectWidth - overlapWidth;
        movingLength = GameContext.gameContentSize.width + objectWidth - initPos.x;
        self.spawnCoolDownTime = Double(objectGapWidth * CGFloat(objectLiftTime) / (movingLength));
    }
    
    override func spawnObjectByTime(currentTime: CFTimeInterval) {
        if (!isStatic) {
            super.spawnObjectByTime(currentTime);
        } else {
            // No need to spawn new if static
            return;
        }
    }
    
}