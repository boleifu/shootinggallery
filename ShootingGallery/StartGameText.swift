//
//  StartGameText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/20/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class StartGameText : SelectableNode {

    override func getText() -> String? {
        return NSLocalizedString("START_TEXT", comment: "StartGameText");
        //return GameTextConstants.START_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Start Text";
        self.zPosition = LayerDepth.MENU_OBJECT.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
    }
    
    override func select() {
        // Move to the game prep page
        GameRuntimeContext.menuPage = MenuPage.GAME_PREP_PAGE;
        unHilight();
    }
}