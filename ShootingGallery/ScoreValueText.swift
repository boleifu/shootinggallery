//
//  ScoreValueText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/21/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class ScoreValueText : UpdatableText {
        
    override func getFontName() -> String? {
        return GameTextConstants.SUB_TITLE_FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Score Value Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Left;
        self.verticalAlignmentMode = .Center;
    }
    
    override func update() {
        let highestText:String = NSLocalizedString("HIGHEST_TEXT", comment: "highestText");
        let highestScoreText:String = NSLocalizedString("NEW_HIGHTEST_SCORE", comment: "ScoreValueText");
        
        var higestScore:Int = 0;
        if (GameRuntimeContext.prevGameMode == GameMode.ARCADE_MODE) {
            higestScore = UserDataUtils.getArcadeModeHighestScore();
        } else if (GameRuntimeContext.prevGameMode == GameMode.SPEED_MODE) {
            higestScore = UserDataUtils.getSpeedModeHighestScore();
        }
        
        let currentGameScore = GameRuntimeContext.currentScore;
        
        if (currentGameScore > higestScore) {
            GameRuntimeContext.breakTheRecord = true;
            
            if (GameRuntimeContext.prevGameMode == GameMode.ARCADE_MODE) {
                UserDataUtils.setArcadeModeHighestScore(currentGameScore);
            } else if (GameRuntimeContext.prevGameMode == GameMode.SPEED_MODE) {
                UserDataUtils.setSpeedModeHighestScore(currentGameScore);
            }

            self.text = String("\(currentGameScore) (\(highestScoreText))");
            //self.text = String("\(currentGameScore) (\(GameTextConstants.NEW_HIGHTEST_SCORE))");
        } else {
            self.text = String("\(currentGameScore) (\(highestText) \(higestScore))");
            //self.text = String("\(currentGameScore) (\(GameTextConstants.HIGHEST_TEXT) \(higestScore))");
        }
    }
}