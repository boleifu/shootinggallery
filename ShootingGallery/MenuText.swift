//
//  MenuText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/18/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

protocol Updatable {
    func update();
}

protocol Selectable {
    var isHilighted:Bool {get set};
    
    func hilight();
    func unHilight();
    func select();
}

class MenuText : SKLabelNode {
    
    // Constructor
    init(fontSize:CGFloat, fontColor:UIColor) {
        super.init();
        
        self.text = getText();
        self.fontName = getFontName();
        self.fontSize = fontSize;
        self.fontColor = fontColor;
        
        // Set the position
        self.position = position;
        
        configProperties();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented");
    }
    
    deinit {
        //print("Game Entity \(self.name) deinit.");
    }
    
    func getText() -> String? {
        // Not implemented
        fatalError("getText() has not been implemented");
    }
    
    func getFontName() -> String? {
        // Not implemented
        fatalError("getFontName() has not been implemented");
    }
    
    func configProperties(){
        // Not implemented
    }
}