//
//  ProgressBar.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/14/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class ProgressBar : GameEntity {

    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "XP_bar_meter");
    }
    
    override func configProperties(){
        self.name = "ProgressBar";
        self.zPosition = LayerDepth.HUD_CONTENT.getDepth();
        self.anchorPoint = CGPoint(x: 1, y: 1);  // Top-right
    }
}