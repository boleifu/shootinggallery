//
//  SelectGameModeText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/20/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class SelectGameModeText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("SELECT_GAME_MODE", comment: "Game prep page title");
        //GameTextConstants.SELECT_GAME_MODE;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Select Game Mode Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Center;
        self.verticalAlignmentMode = .Center;
    }
}