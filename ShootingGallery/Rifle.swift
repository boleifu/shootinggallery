//
//  Rifle.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/4/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class Rifle : GameEntity {
    
    let aimingAngle = M_PI/6;
    let tanAimingAngle = tan(CGFloat(M_PI/6));
    
    // Runtime state
    var lastFireTime:CFTimeInterval = 0;
    var lastCommandId:Int = -1;
    var lastUpdatedCrossHairPos = CGPoint.zero;
    
    var riflePosY:CGFloat = 0;
    
    // ======== Config ========
    override func configProperties() {
        self.name = "Rifle";
        self.zPosition = LayerDepth.RIFLE.getDepth();
        self.anchorPoint = CGPoint(x: 0, y: 1);
        self.alpha = 0.8;
        
        riflePosY = GameContext.screenSize.height/4;
    }
    
    override func generateTexture() -> SKTexture? {
        return SKTexture(imageNamed: "rifle");
    }
    
    // ======== Liftcycle management ========
    /**
     * Update the rifle position based on the current crosshair position
     */
    func update(crossHairPos: CGPoint) {
        if (crossHairPos == lastUpdatedCrossHairPos) {
            return;
        }
        
        let dx:CGFloat = (crossHairPos.y - self.position.y) * tanAimingAngle;
        let desiredPos = CGPoint(x: (crossHairPos.x + dx), y: riflePosY);
        //let posInBoundary = checkBoundary(desiredPos);
        self.position = desiredPos;
        
        lastUpdatedCrossHairPos = crossHairPos;
    }
    
    // ======== Helper function ========
    /**
     * Process rifle fire
     * @param currentTime The current game time
     * @param commandId The unique id of the fire button command, at most fire for each command
     * @return Whether the rifle fired
     */
    func fire(currentTime: CFTimeInterval, commandId:Int) -> Bool {
        if (commandId != lastCommandId && currentTime - lastFireTime > GameRuntimeContext.fireCoolDownTime) {
            //print("Processing fire. Cooldown time = \(currentTime - lastFireTime), command id = \(commandId)");
            lastFireTime = currentTime;
            lastCommandId = commandId;
            // Fired
            
            // Add total shot number
            GameRuntimeContext.totalShotNumber += 1;
            return true;
        } else {
            // Not ready to fire
            return false;
        }
    }
}
