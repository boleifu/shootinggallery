//
//  ArcadeButtonPosText.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/23/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

class ArcadeButtonPosText : MenuText {
    
    override func getText() -> String? {
        return NSLocalizedString("ARCADE_BUTTON_POS_TEXT", comment: "ArcadeButtonPosText");
        //return GameTextConstants.ARCADE_BUTTON_POS_TEXT;
    }
    
    override func getFontName() -> String? {
        return GameTextConstants.FONT_NAME;
    }
    
    override func configProperties(){
        self.name = "Arcade Button Pos Text";
        self.zPosition = LayerDepth.MENU_TITLE.getDepth();
        self.horizontalAlignmentMode = .Right;
        self.verticalAlignmentMode = .Center;
    }
}