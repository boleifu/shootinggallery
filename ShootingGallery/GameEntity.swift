//
//  GameEntity.swift
//  ShootingGallery
//
//  Created by Bolei Fu on 8/4/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import SpriteKit

protocol Damagable {
    func isHit(hitPosOnTarget:CGPoint) -> Bool;
    func hit(hitPosOnNode:CGPoint) -> Bool;
}

class GameEntity : SKSpriteNode {
    
    // Constructor
    init(position:CGPoint, face:CGFloat, targetWidth:CGFloat?, targetHeight:CGFloat?) {
        super.init(texture: nil, color: SKColor.whiteColor(), size: CGSize.zero);
        
        // Get the texture of this object
        self.texture = generateTexture();
        
        // Resize the sprite based on user's input
        resize(targetWidth, targetHeight);
        
        // Set the position and rotation
        self.position = position;
        self.zRotation = face;
        
        configProperties();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented");
    }
    
    deinit {
        //print("Game Entity \(self.name) deinit.");
    }
    
    /**
     * Resize the object based on the input
     * If either width or height is nil, keep the original width/height ratio
     */
    func resize(targetWidth:CGFloat?, _ targetHeight:CGFloat?) {
        // Resize the sprite based on user's input
        if (targetWidth == nil && targetHeight == nil) {
            // Size not specified
            self.size = self.texture!.size();
        } else if (targetWidth == nil) {
            // Only height specified
            let ratio:CGFloat = targetHeight!/self.texture!.size().height;
            self.size = CGSizeMake(self.texture!.size().width * ratio, self.texture!.size().height * ratio);
        } else if (targetHeight == nil) {
            // Only width specified
            let ratio:CGFloat = targetWidth!/self.texture!.size().width;
            self.size = CGSizeMake(self.texture!.size().width * ratio, self.texture!.size().height * ratio);
        } else {
            // Size specified
            self.size = CGSizeMake(targetWidth!, targetHeight!);
        }
    }
    
    /**
     * This function is used to generate the sprite texture
     * No default behavior, must be override
     */
    func generateTexture() -> SKTexture? {
        // Not implemented
        fatalError("generateTexture() has not been implemented");
    }
    
    /**
     * This function is used to set some customized configuration
     * Executed after object initialization
     * No default behavior
     */
    func configProperties(){
        // Not implemented
    }
}
